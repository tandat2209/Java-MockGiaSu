<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<!DOCTYPE html>
<html>
<head>
<title>Nhân viên đăng nhập</title>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="bootstrap-theme.min.css">
<link rel="stylesheet" href="css/myStyleSheet.css">
<script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/myJavaScript.js"></script>
</head>
<body>
<jsp:include page="headerND.jsp"></jsp:include>
<div class="container content">

		<div class="row">

			<div class="col-md-4 col-md-offset-4">
				<div class="panel panel-warning">
					<div class="panel-heading text-center">
						<h4>Đăng nhập hệ thống</h4>
						<h6>Dành cho <strong>nhân viên</strong></h6>
					</div>
					<div class="panel-body">
					<html:form action="nvDangNhap">
					  <div class="form-group">
					  	<br>
					    <label>Tên đăng nhập</label>
					    <html:text property="tenDangNhap" styleClass="form-control" />
						<p style="color: red;">
						<html:errors property="tenDangNhapError" />
						</p>
					  </div>
					  <div class="form-group">
					    <label>Mật khẩu</label>
					    <html:password property="matKhau" styleClass="form-control" />
						<p style="color: red;">
						<html:errors property="matKhauError" />
						</p>
					  </div>
					  
					  <hr>
					  <p style="color: red;"><bean:write name="nvDangNhapForm" property="error" /></p>
					  <html:submit styleClass="btn btn-warning" property="submit" value="Đăng nhập" />
					  <html:link action="/searchCourseAction" styleClass="btn btn-default">Hủy</html:link>
					</html:form>
					</div>
				</div>
			</div>
			</div>
		</div>
<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>