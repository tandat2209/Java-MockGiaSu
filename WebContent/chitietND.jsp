<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html>
<html:html lang="en">
<head>
<title>Chi tiết người dùng</title>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="bootstrap-theme.min.css">
<link rel="stylesheet" href="css/myStyleSheet.css">
<script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/myJavaScript.js"></script>
</head>
<body>
	<jsp:include page="headerND.jsp"></jsp:include>
	<div class="container content">
   <div class="thumbnail">
   <div class="row">
     <div class="col-md-2">
         <p><html:link action="/searchTutorAction.do" styleClass="btn btn-default center-block btn-type" >Tìm gia sư</html:link></p>
         <p><html:link action="/searchCourseAction.do" styleClass="btn btn-warning center-block btn-type" >Tìm suất dạy</html:link></p>
     </div>
     <div class="col-md-8">
     	<div class="panel panel-warning">
     		<div class="panel-heading">
				<h4 class="panel-title text-center">Thông tin người dùng</h4>
			</div>
			<div class="panel-body">
				<bean:define id="nguoidung" name="nvChiTietNguoidung" property="nguoiDung"/>
				<div class="col-md-4">
							<logic:empty name="nguoidung" property="anhDaiDien">
							<img src="img/avatar.png" alt="avatar"
								class="img-circle img-responsive">
							</logic:empty>
							<logic:present name="nguoidung" property="anhDaiDien">
							<div class="avatar">
							<bean:define id="path" name="nguoidung" property="anhDaiDien"></bean:define>
								<img src="${path}" alt="avatar"
								class="img-responsive"></div>
							</logic:present>
						</div>
				<div class="col-md-8">
					
					<table class="table table-default table-information">
								<tbody>
									<tr>
										<td>Tên đăng nhập:</td>
										<td><bean:write name="nguoidung" property="tenDangNhap" /></td>
									</tr>
									<tr>
										<td>Họ tên:</td>
										<td><bean:write name="nguoidung" property="hoTen" /></td>
									</tr>
									<tr>
										<td>Giới tính:</td>
										<td><bean:write name="nguoidung" property="gioiTinh" /></td>
									</tr>
									<tr>
										<td>Ngày sinh:</td>
										<td><bean:write name="nguoidung" property="ngaySinh" /></td>
									</tr>
									<tr>
										<td>Số điện thoại:</td>
										<td><bean:write name="nguoidung" property="soDienThoai" /></td>
									</tr>
									<tr>
										<td>Email:</td>
										<td><bean:write name="nguoidung" property="email" /></td>
									</tr>
									<tr>
										<td>Số lần cảnh báo:</td>
										<td><bean:write name="nguoidung" property="soLanCanhBao" /></td>
									</tr>
									<tr>
										<td>Số bài đăng:</td>
										<td><bean:write name="nguoidung" property="soBaiDang" /></td>
									</tr>
									<tr>
										<td>Trạng thái:</td>
										<td><bean:write name="nguoidung" property="trangThai" /></td>
									</tr>
								</tbody>
							</table>
				</div>
			</div>
			<div class="panel-footer text-right">
				<logic:present name="ndDangNhapForm" property="tenDangNhap">
					<bean:define id="nd" name="ndDangNhapForm" property="tenDangNhap" />
					<logic:equal value="${nd}" name="nguoidung" property="tenDangNhap">
						<html:link action="/ndSuaThongTin" styleClass="btn btn-warning">Cập nhật thông tin</html:link>
					</logic:equal>
				</logic:present>
						<button class="btn btn-default" onclick="window.history.go(-1)">Trở về</button>
					</div>
     	</div>
     </div>
     <div class="col-md-2"></div>
    </div>
   </div>
   
  </div>
  <jsp:include page="footer.jsp"></jsp:include>
 </body>
 </html:html>