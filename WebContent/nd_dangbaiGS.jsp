<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Đăng bài gia sư</title>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="bootstrap-theme.min.css">
<link rel="stylesheet" href="css/myStyleSheet.css">
<script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/myJavaScript.js"></script>
</head>
<body>
	<jsp:include page="headerND.jsp"></jsp:include>
	<div class="container content">
		<div class="thumbnail">
		<div class="row">
			<div class="col-md-3 col-xs-12">

					<p>
						<html:link styleClass="btn btn-warning center-block btn-type"
							action="/dangBaiGiaSu">Đăng bài gia sư</html:link>
					</p>
					<p>
						<html:link styleClass="btn btn-default center-block btn-type"
							action="/dangBaiSuatDay">Đăng bài suất dạy</html:link>
					</p>
					<hr>
					<p>
						<html:link styleClass="btn btn-default center-block btn-type"
							action="/ndThongtinCaNhan">Thông tin cá nhân</html:link>
					</p>
					<p>
						<html:link styleClass="btn btn-default center-block btn-type"
							action="/lichSuDangBai">Lịch sử đăng bài</html:link>
					</p>
					<p>
						<html:link styleClass="btn btn-default center-block btn-type"
							action="/ndThongBao">Danh sách thông báo</html:link>
					</p>

			</div>
			<div class="col-md-7">
				<div class="panel panel-warning">
					<div class="panel-heading">
						<h4 class="text-center">Đăng bài gia sư</h4>
					</div>
					<html:form styleClass="form-horizontal" action="/dangBaiGiaSu">
						<div class="panel-body">
							<h4>Thông tin gia sư</h4><hr>
							<div class="row">
								<div class="col-md-11 col-md-offset-1">
									<div class="form-group">
										<label class="col-md-4 condivol-label">Họ tên Gia sư:</label>
										<div class="col-md-7">
											<html:text styleClass="form-control hotenInput" property="tenGiaSu" />
										</div>
									</div>
									<div class="col-xs-8 col-xs-offset-4">
										<p style="color: red;">
											<html:errors property="errHoTen" />
										</p>
									</div>
									<div class="form-group">
										<label class="col-md-4 condivol-label">Hiện đang là:</label>
										<div class="col-md-7">
											<html:select styleClass="form-control" property="ngheNghiep">
												<html:options name="ndChiTietBaiDangForm" property="nghe" />
											</html:select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-4 condivol-label">Nơi công tác:</label>
										<div class="col-md-7">
											<html:text styleClass="form-control noicongtacInput" property="noiCongTac" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-4 condivol-label">Giới tính:</label>
										<div class="col-md-7 input-group">
											<div class="col-xs-4">
												<html:radio styleClass="checkbox-inline" property="gioiTinh"
													value="Nam"> Nam</html:radio>
											</div>
											<div class="col-xs-4">
												<html:radio styleClass="checkbox-inline" property="gioiTinh"
													value="Nữ"> Nữ</html:radio>
											</div>
											<div class="col-xs-4">
												<html:radio styleClass="checkbox-inline" property="gioiTinh"
													value="Khác"> Khác</html:radio>
											</div>
										</div>
									</div>
									<div class="col-xs-8 col-xs-offset-4">
										<p style="color: red;">
											<html:errors property="errGioiTinh" />
										</p>
									</div>
									<div class="form-group">
										<label class="col-md-4 condivol-label">Ngày sinh:</label>
										<div class="col-md-7">
											<html:text styleClass="form-control ngaysinhInput" property="ngaySinh" />
										</div>
									</div>
									<div class="col-xs-8 col-xs-offset-4">
										<p style="color: red;">
											<html:errors property="errNgaySinh" />
										</p>
									</div>
									<div class="form-group">
										<label class="col-md-4 condivol-label">Quê quán:</label>
										<div class="col-md-7">
											<html:select styleClass="form-control" property="queQuan">
												<html:options name="ndChiTietBaiDangForm" property="tinh" />
											</html:select>
										</div>

									</div>
									<div class="form-group">
										<label class="col-md-4 condivol-label">Số điện thoại:</label>
										<div class="col-md-7">
											<html:text styleClass="form-control sodienthoaiInput" property="soDienThoai" />
										</div>
									</div>
									<div class="col-xs-8 col-xs-offset-4">
										<p style="color: red;">
											<html:errors property="errPhone" />
										</p>
									</div>
								</div>
							</div>
							<h4>Thông tin lớp dạy</h4><hr>
							<div class="row">
							
								<div class="col-md-11 col-md-offset-1">
									
									<div class="form-group">
										<label class="col-md-4 condivol-label">Tiêu đề:</label>
										<div class="col-md-7">
											<html:text styleClass="form-control tieudeInput" property="tieuDe" ></html:text>
										</div>
									</div>
									<div class="col-xs-8 col-xs-offset-4">
										<p style="color: red;">
											<html:errors property="errTieuDe" />
										</p>
									</div>	
									
									<div class="form-group">
										<label class="col-md-4 condivol-label">Lớp dạy:</label>
										<div class="col-md-7">
											<html:text styleClass="form-control" property="lop" />
										</div>
									</div>
									<div class="col-xs-8 col-xs-offset-4">
										<p style="color: red;">
											<html:errors property="errLop" />
										</p>
									</div>
									<div class="form-group">
										<label class="col-md-4 condivol-label">Môn dạy:</label>
										<div class="col-md-7">
											<html:text styleClass="form-control" property="mon" />
										</div>
									</div>
									<div class="col-xs-8 col-xs-offset-4">
										<p style="color: red;">
											<html:errors property="errMon" />
										</p>
									</div>
									<div class="form-group">
										<label class="col-md-4 condivol-label">Khu vực:</label>
										<div class="col-md-7">
											<logic:iterate id="kv" name="ndChiTietBaiDangForm"
												property="kvList">
												<div class="col-md-6">
													<html:multibox property="maKvArr">
														<bean:write name="kv" property="maKV" />
													</html:multibox>
													<bean:write name="kv" property="tenKV" />
												</div>
											</logic:iterate>
										</div>
									</div>
									<div class="col-xs-8 col-xs-offset-4">
										<p style="color: red;">
											<html:errors property="errKhuVuc" />
										</p>
									</div>
									<div class="form-group">
										<label class="col-md-4 condivol-label">Thời gian rảnh:</label>
										<div class="col-md-7">
											<html:text styleClass="form-control thoigiandayInput" property="thoiGianDay" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-4 condivol-label">Lương (VNĐ/buổi):</label>
										<div class="col-md-7">
											<html:text styleClass="form-control" property="luongGS" />
										</div>
	
									</div>
									<div class="col-xs-8 col-xs-offset-4">
										<p style="color: red;">
											<html:errors property="errLuong" />
										</p>
									</div>
									<div class="form-group">
										<label class="col-md-4 condivol-label">Mô tả thêm:</label>
										<div class="col-md-7">
											<html:textarea styleClass="form-control" property="moTaThem"
												cols="25" rows="5" />
										</div>
									</div>
									<div class="col-xs-8 col-xs-offset-4">
										<p style="color: red;">
											<html:errors property="errorChung" />
										</p>
									</div>

								</div>
							</div>
							<div class="panel-footer text-right">
								<html:submit styleClass="btn btn-warning" property="submit"
									value="Đăng bài"></html:submit>
								<button class="btn btn-default" onclick="window.history.go(-1)">Trở
									về</button>
							</div>

						</div>
					</html:form>
				</div>
			</div>

			<div class="col-md-2 col-xs-12">
				<div class="thumbnail">
					<img src="img/dat-quang-cao.gif" alt="" style="border-radius: 3px"><br>
					<img src="img/FPT1.gif" alt="" style="border-radius: 3px">
				</div>
			</div>
		</div>
		</div>
	</div>
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>
