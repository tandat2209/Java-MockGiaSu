<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html>
<html:html lang="en">
<head>
<title>Đăng ký</title>
<meta charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="bootstrap-theme.min.css">
<link rel="stylesheet" href="css/myStyleSheet.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</head>
<body>

	
	<jsp:include page="headerND.jsp"></jsp:include>
	
	<div class="container content">
  
   <div class="row">
    
     <div class="col-md-6 col-md-offset-3">
				<div class="panel panel-warning">
					<div class="panel-heading">
						<h4 class="text-center">Đăng ký</h4>
					</div>
					<html:form styleClass="form-horizontal" action="/dangKyAction">
						<div class="panel-body">
							<div class="row">
								<div class="col-md-11 col-md-offset-1">
									<div class="form-group">
										<label class="col-md-4 condivol-label">Tên đăng nhập:</label>
										<div class="col-md-7">
											<html:text styleClass="form-control" property="tenDangNhap" />
										</div>
									</div>
									<div class="col-xs-8 col-xs-offset-4">
										<p style="color: red;">
											<html:errors property="errTenDangNhap" />
										</p>
									</div>
									<div class="form-group">
										<label class="col-md-4 condivol-label">Mật khẩu:</label>
										<div class="col-md-7">
											<html:password styleClass="form-control" property="matKhau" />
										</div>
									</div>
									<div class="col-xs-8 col-xs-offset-4">
										<p style="color: red;">
											<html:errors property="errMatKhau" />
										</p>
									</div>
									<div class="form-group">
										<label class="col-md-4 condivol-label">Xác nhận mật khẩu:</label>
										<div class="col-md-7">
											<html:password styleClass="form-control" property="pwd1" />
										</div>
									</div>
									<div class="col-xs-8 col-xs-offset-4">
										<p style="color: red;">
											<html:errors property="errMatKhauXN" />
										</p>
									</div>
									<div class="form-group">
										<label class="col-md-4 condivol-label">Họ tên:</label>
										<div class="col-md-7">
											<html:text styleClass="form-control" property="tenND" />
										</div>
									</div>
									<div class="col-xs-8 col-xs-offset-4">
										<p style="color: red;">
											<html:errors property="errHoTen" />
										</p>
									</div>
									<div class="form-group">
										<label class="col-md-4 condivol-label">Giới tính:</label>
										<div class="col-md-7 input-group">
											<div class="col-xs-4">
												<html:radio styleClass="checkbox-inline" property="gioiTinh"
													value="Nam">Nam</html:radio>
											</div>
											<div class="col-xs-4">
												<html:radio styleClass="checkbox-inline" property="gioiTinh"
													value="Nữ">Nữ</html:radio>
											</div>
											<div class="col-xs-4">
												<html:radio styleClass="checkbox-inline" property="gioiTinh"
													value="Khác">Khác</html:radio>
											</div>
										</div>
									</div>
									<div class="col-xs-8 col-xs-offset-4">
										<p style="color: red;">
											<html:errors property="errGioiTinh" />
										</p>
									</div>
									<div class="form-group">
										<label class="col-md-4 condivol-label">Ngày sinh:</label>
										<div class="col-md-7">
											<html:text styleClass="form-control" property="ngaySinh" />
										</div>
									</div>
									<div class="col-xs-8 col-xs-offset-4">
										<p style="color: red;">
											<html:errors property="errNgaySinh" />
										</p>
									</div>
									<div class="form-group">
										<label class="col-md-4 condivol-label">Số điện thoại:</label>
										<div class="col-md-7">
											<html:text styleClass="form-control" property="soDienThoai" />
										</div>
									</div>
									<div class="col-xs-8 col-xs-offset-4">
										<p style="color: red;">
											<html:errors property="errPhone" />
										</p>
									</div>
									<div class="form-group">
										<label class="col-md-4 condivol-label">Email:</label>
										<div class="col-md-7">
											<html:text styleClass="form-control" property="email" />
										</div>
									</div>
									<div class="col-xs-8 col-xs-offset-4">
										<p style="color: red;">
											<html:errors property="errEmail" />
										</p>
									</div>
								</div>
							</div>
							<div class="panel-footer text-right">
								<html:submit styleClass="btn btn-warning" property="submit"
									value="Đăng ký"></html:submit>
								<button class="btn btn-default" onclick="window.history.go(-1)">Trở
									về</button>
							</div>

						</div>
					</html:form>
				</div>
			</div>
     
     


   </div>
  </div>
	<jsp:include page="footer.jsp"></jsp:include>
	<script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
</body>
</html:html>