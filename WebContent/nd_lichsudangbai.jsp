<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Lịch sử bài đăng</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/myStyleSheet.css" rel="stylesheet">
<script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/myJavaScript.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/PaginationJS.js"></script>
<script type="text/javascript">
$(document).ready(function() {
$('#showtable').pageMe({
pagerSelector : '#myPager',
showPrevNext : true,
hidePageNumbers : false,
perPage : 10
});
});
</script>
<script type="text/javascript">
	function xacNhanXoa(a) {
		if (confirm("Bạn có chắc muốn xóa bài đăng có tiêu đề là " + a
				+ " không?"))
			return true;
		return false;
	}
</script>
<title>Lịch sử đăng bài</title>
</head>
<body>
	<jsp:include page="headerND.jsp"></jsp:include>
	<div class="container content">
		<div class="thumbnail">
		<div class="bs-example">
			<div class="row">
			<div class="col-md-3 col-xs-12">
		         <p><html:link styleClass="btn btn-default center-block btn-type" action="/dangBaiGiaSu">Đăng bài gia sư</html:link></p>
		         <p><html:link styleClass="btn btn-default center-block btn-type" action="/dangBaiSuatDay">Đăng bài suất dạy</html:link></p>
		  		<hr>
		         <p><html:link styleClass="btn btn-default center-block btn-type" action="/ndThongtinCaNhan">Thông tin cá nhân</html:link></p>
		         <p><html:link styleClass="btn btn-warning center-block btn-type" action="/lichSuDangBai">Lịch sử đăng bài</html:link></p>
		         <p><html:link styleClass="btn btn-default center-block btn-type" action="/ndThongBao">Danh sách thông báo</html:link></p>
			</div>
			<div class="col-md-8 col-xs-12">
				
				<div class="panel panel-danger">
                	<h3 class="text-center">Danh sách bài đăng</h3>
                <html:form action="/lichSuDangBai" styleClass="form-inline">
                
                  <div class="form-group">
                    <label>Loại bài đăng :</label>
                    <html:select name="ndLichSuDangBaiForm" property="loai" styleClass="form-control">
                      <html:option value="Tất cả">Tất cả</html:option>
                      <html:option value="Gia sư">Gia sư</html:option>
                      <html:option value="Suất dạy">Suất dạy</html:option>
                    </html:select>
                  </div>
                  <div class="form-group">
                    <label>Trạng thái :</label>
                    <html:select name="ndLichSuDangBaiForm" property="trangThai" styleClass="form-control">
                      <html:option value="Tất cả">Tất cả</html:option>
                      <html:option value="Chờ duyệt">Chờ duyệt</html:option>
                      <html:option value="Đã duyệt">Đã duyệt</html:option>
                      <html:option value="Từ chối">Bị từ chối</html:option>
                      <html:option value="Ẩn">Bị ẩn</html:option>
                    </html:select>
                  </div>
                  <html:submit styleClass="btn btn-default">Tìm kiếm</html:submit>
                </html:form>
          		</div>
				<div class=" panel panel-default">
					<div class="bs-example">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Tiêu đề</th>
                <th>Loại</th>
                <th>Thời gian đăng</th>
                <th>Trạng thái</th>
                <th>Chi tiết</th>
            </tr>
            </thead>        
            <tbody id="showtable">
								<logic:iterate name="ndLichSuDangBaiForm" property="listbd"
									id="baidang">
									<tr>
										<bean:define id="maBD" name="baidang" property="maBD" />
										<bean:define id="tieuDe" name="baidang" property="tieuDe" />
										<td><html:link action="/ndChiTietBaiDang?maBD=${maBD}"><bean:write name="baidang" property="tieuDe" /></html:link></td>
										<td><bean:write name="baidang" property="loaiBD" /></td>
										<td><bean:write name="baidang" property="thoiGianDang" /></td>
										<td><bean:write name="baidang" property="trangThai" /></td>
										<td><logic:equal value="Gia sư" name="baidang" property="loaiBD">
												<html:link action="/suaGiaSu?maBD=${maBD}"><span class="glyphicon glyphicon-edit"></span></html:link>
											</logic:equal> <logic:equal value="Suất dạy" name="baidang"
												property="loaiBD">
												<html:link action="/suaSuatDay?maBD=${maBD}"><span class="glyphicon glyphicon-edit"></span></html:link>
											</logic:equal>
											<html:link action="/ndxoaBaiDang?maBD=${maBD}" onclick="return xacNhanXoa('${tieuDe}')" ><span class="glyphicon glyphicon-trash"></span></html:link>
											</td>
											
									</tr>
								</logic:iterate>
			</tbody>
      
        </table>
         <div class="row" style="text-align: center;">
				<div class="col-lg-4"></div>
				<div class="col-lg-4">
					<ul style="margin: 0px" class="pagination pagination-xs"
						id="myPager"></ul>
				</div>
		</div>
    </div>		
				</div>
			</div>
		</div>
		</div>
		</div>
		</div>
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>