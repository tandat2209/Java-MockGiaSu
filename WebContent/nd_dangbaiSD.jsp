<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Đăng bài suất dạy</title>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="bootstrap-theme.min.css">
<link rel="stylesheet" href="css/myStyleSheet.css">
<script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/myJavaScript.js"></script>
</head>
<body>
	<jsp:include page="headerND.jsp"></jsp:include>
	<div class="container content">
	<div class="thumbnail">
		<div class="row">
			<div class="col-md-3 col-xs-12">
		
					<p><html:link styleClass="btn btn-default center-block btn-type"
							action="/dangBaiGiaSu">Đăng bài gia sư</html:link></p>
					<p><html:link styleClass="btn btn-warning center-block btn-type"
							action="/dangBaiSuatDay">Đăng bài suất dạy</html:link></p>
					<hr>
					<p><html:link styleClass="btn btn-default center-block btn-type"
							action="/ndThongtinCaNhan">Thông tin cá nhân</html:link></p>
					<p><html:link styleClass="btn btn-default center-block btn-type"
							action="/lichSuDangBai">Lịch sử đăng bài</html:link></p>
					<p><html:link styleClass="btn btn-default center-block btn-type"
							action="/ndThongBao">Danh sách thông báo</html:link></p>

			</div>
			<div class="col-md-7">
				<div class="panel panel-warning">
				<div class="panel-heading">
						<h4 class="text-center">Đăng bài suất dạy</h4>
					</div>
				<html:form styleClass="form-horizontal" action="/dangBaiSuatDay">
					<div class="panel-body">
						<h4>Thông tin phụ huynh</h4><hr>
						<div class="row">
							<div class="col-md-11 col-md-offset-1">
									<div class="form-group">
										<label class="col-md-4 condivol-label">Tên phụ huynh:</label>
										<div class="col-md-7">
											<html:text styleClass="form-control hotenInput" property="tenPhuHuynh" />
										</div>
									</div>
									<div class="col-xs-8 col-xs-offset-4">
										<p style="color: red;">
											<html:errors property="errHoTen" />
										</p>
									</div>
									<div class="form-group">
										<label class="col-md-4 condivol-label">Địa chỉ:</label>
										<div class="col-md-7">
											<html:text styleClass="form-control diachiInput" property="diaChi" />
										</div>
									</div>
									<div class="col-xs-8 col-xs-offset-4">
										<p style="color: red;">
											<html:errors property="errDiaChi" />
										</p>
									</div>
									<div class="form-group">
										<label class="col-md-4 condivol-label">Khu vực:</label>
										<div class="col-md-7">
											<html:select styleClass="form-control" property="khuVucUpdateSD">
												<html:optionsCollection name="ndChiTietBaiDangForm"
													property="kvList" label="tenKV" value="maKV" />
											</html:select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-4 condivol-label">Số điện thoại:</label>
										<div class="col-md-7">
											<html:text styleClass="form-control sodienthoaiInput" property="soDienThoai" />
										</div>
									</div>
									<div class="col-xs-8 col-xs-offset-4">
										<p style="color: red;">
											<html:errors property="errPhone" />
										</p>
									</div>
								</div>
								</div>
							<h4>Thông tin lớp</h4><hr>
							<div class="row">
							<div class="col-md-11 col-md-offset-1">
							
									<div class="form-group">
										<label class="col-md-4 condivol-label">Tiêu đề:</label>
										<div class="col-md-7">
											<html:text styleClass="form-control tieudeInput" property="tieuDe"></html:text>
										</div>
									</div>
									<div class="col-xs-8 col-xs-offset-4">
										<p style="color: red;">
											<html:errors property="errTieuDe" />
										</p>
									</div>
									
									
									<div class="form-group">
										<label class="col-md-4 condivol-label">Lớp dạy:</label>
										<div class="col-md-7">
											<html:text styleClass="form-control" property="lop" />
										</div>
									</div>
									<div class="col-xs-8 col-xs-offset-4">
										<p style="color: red;">
											<html:errors property="errLop" />
										</p>
									</div>
									<div class="form-group">
										<label class="col-md-4 condivol-label">Môn dạy:</label>
										<div class="col-md-7">
											<html:text styleClass="form-control" property="mon" />
										</div>
									</div>
									<div class="col-xs-8 col-xs-offset-4">
										<p style="color: red;">
											<html:errors property="errMon" />
										</p>
									</div>
									
									
									<div class="form-group">
										<label class="col-md-4 condivol-label">Số buổi trên
											tuần:</label>
										<div class="col-md-7">
											<input type="number" class="form-control" min=1 value="1" name="soBuoiTrenTuan" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-4 condivol-label">Số lượng học
											sinh:</label>
										<div class="col-md-7">
											<input type="number" class="form-control" min=1 value="1" name="soLuongHocSinh" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-4 condivol-label">Thời gian:</label>
										<div class="col-md-7">
											<html:text styleClass="form-control thoigiandayInput" property="thoiGianDay" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-4 condivol-label">Lương (VNĐ/tháng):</label>
										<div class="col-md-7">
											<html:text styleClass="form-control" property="luongSD" />
										</div>
									</div>
									<div class="col-xs-8 col-xs-offset-4">
										<p style="color: red;">
											<html:errors property="errLuong" />
										</p>
									</div>
									<div class="form-group">
										<label class="col-md-4 condivol-label">Mô tả thêm:</label>
										<div class="col-md-7">
											<html:textarea styleClass="form-control" property="moTaThem"
												cols="25" rows="5" />
										</div>
									</div>
									
							</div>
						</div>
					</div>
					<div class="panel-footer text-right">
										<html:submit styleClass="btn btn-warning" property="submit"
											value="Đăng bài"></html:submit>
										<button class="btn btn-default"
											onclick="window.history.go(-1)">Trở về</button>
									</div>
					</html:form>
				</div>
			</div>

			<div class="col-md-2 col-xs-12">
				<div class="thumbnail">
					<img src="img/dat-quang-cao.gif" alt="" style="border-radius: 3px"><br>
					<img src="img/FPT1.gif" alt="" style="border-radius: 3px">
				</div>
			</div>
		</div>
		</div>
	</div>
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>
