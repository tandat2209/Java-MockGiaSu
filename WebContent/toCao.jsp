<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Trang To Cao</title>
</head>
<body>
	<html:form action="/toCaoAction">
			Email:<html:text property="email"></html:text>
			<bean:write name="toCaoForm" property="emailError"/>
		<br />
			Số ĐT:<html:text property="phone"></html:text>
			<html:errors property="errorPhone"/>
		<br />
			Nội dung:<html:textarea property="content"></html:textarea>
			<html:errors property="errorEmpty"/>
		<br />
		<html:submit property="submit" value="confirm">Gửi</html:submit>
		<html:submit property="submit" value="cancel">Hủy</html:submit>
	</html:form>
</body>
</html:html>