<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Thêm nhân viên</title>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="bootstrap-theme.min.css">
<link rel="stylesheet" href="css/myStyleSheet.css">
<script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/myJavaScript.js"></script>
</head>
<body>
	<jsp:include page="headerAdmin.jsp"></jsp:include>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3 col-xs-12">
				<div id="left-panel">
					<div class="list-group">
						<html:link action="admin_ThemNhanVienAction"
							styleClass="list-group-item">Thêm nhân viên</html:link>
						<html:link action="admin_dsnhanvien" styleClass="list-group-item">Danh sách nhân viên</html:link>
					</div>
				</div>
			</div>

			<div class="col-md-6 col-xs-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<h4 class="text-center">Thêm nhân viên</h4>
						<hr>
						<html:form action="admin_ThemValidateNhanVienAction">
							<table class="table table-default table-information">
								<tbody>
									<tr>
										<td><label>Tên đăng nhập</label></td>
										<td><html:text property="tenDangNhap"
												styleClass="form-control"></html:text>
										<html:errors property="tenDangNhapError"/>	
										<bean:write name="admin_NhanVienValidateForm" property="errTenDangNhap"/>
										</td>
										
									</tr>
									<tr>
										<td><label>Mật khẩu</label></td>
										<td><html:password property="matKhau"
												styleId="matKhau_01" styleClass="form-control"></html:password>
											<html:errors property="matKhauError"/>		
												</td>
									</tr>
									<tr>
										<td><label>Nhập lại mật khẩu</label></td>
										<td><html:password property="matKhau"
												styleId="matKhau_01" styleClass="form-control"></html:password></td>
									</tr>
									<tr>
										<td><label>Họ tên nhân viên</label></td>
										<td><html:text property="tenNV" styleClass="form-control"></html:text>
											<html:errors property="tenNVError"/>	
										</td>
									</tr>
									<tr>
										<td><label>Ngày sinh</label></td>
										<td><html:text property="ngaySinh"
												styleClass="form-control"></html:text>
											<html:errors property="ngaySinhError"/>	
											</td>
									</tr>
									<tr>
										<td><label>Giới tính</label></td>
										
										<td><html:radio property="gioiTinh" value="Nam"
												name="admin_NhanVienForm" styleClass="checkbox-inline"> Nam</html:radio>
											<html:radio property="gioiTinh" value="Nữ"
												name="admin_NhanVienForm" styleClass="checkbox-inline"> Nữ</html:radio>
											<html:radio property="gioiTinh" value="Khác"
												name="admin_NhanVienForm" styleClass="checkbox-inline"> Khác</html:radio>
										</td>
										
									</tr>
									<tr>
										<td><label>Địa chỉ</label></td>
										<td><html:text property="diaChi"
												styleClass="form-control"></html:text></td>
									</tr>
									<tr>
										<td><label>Email</label></td>
										<td><html:text property="email" styleClass="form-control"></html:text>
										<html:errors property="emailError"/>
										</td>
									</tr>
									<tr>
										<td><label>Số điện thoại</label></td>
										<td><html:text property="soDienThoai"
												styleClass="form-control"></html:text>
											<html:errors property="soDienThoaiError"/>
										</td>
									</tr>
								</tbody>
							</table>
							<hr>
							<div class="text-right">
								<html:submit property="submitThem" styleClass="btn btn-info">Thêm</html:submit>
								<button class="btn btn-default" onclick="window.history.go(-1)">Hủy</button>
							</div>
						</html:form>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</body>
</html>