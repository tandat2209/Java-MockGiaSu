<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Thông tin cá nhân</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/myStyleSheet.css" rel="stylesheet">
<script src="js/jquery-1.11.3.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3 col-xs-12">
				<!-- left panel -->
				<div id="left-panel">
					<div class="list-group">
						<html:link action="/nvThongTinCaNhan" styleClass="list-group-item">Thông tin cá nhân</html:link>
						<html:link action="/dsduyetbaidang" styleClass="list-group-item">Danh sách bài đăng</html:link>
						<html:link action="/dsNguoiDung" styleClass="list-group-item">Danh sách người dùng</html:link>
						<html:link action="/nvDSThongBao" styleClass="list-group-item">Danh sách thông báo</html:link>
					</div>
				</div>
			</div>
			<div class="col-md-6">
			<html:form action="/nvDoiMatKhau">
				<div class="panel panel-warning">
					<div class="panel-heading">
						<h4 class="panel-title text-center">Đổi mật khẩu</h4>
					</div>
					<div class="panel-body">
						<div class="col-md-4">
							<img src="img/avatar.png" alt="avatar"
								class="img-circle img-responsive">
						</div>
						<div class="col-md-8">
							<table class="table table-default table-information">
								<tbody>
									<tbody>
											<tr>
												<td>Tên đăng nhập:</td>
												<html:hidden property="tenDangNhap"
													name="nhanVienForm" />
												<td><strong><bean:write
															name="nhanVienForm" property="tenDangNhap" /></strong></td>
											</tr>
											<tr>
												<td>Mật khẩu cũ:</td>
												<td><html:password styleClass="form-control"
														name="nhanVienForm" property="matKhauCu" />
													<p style="color: red;">
														<html:errors property="errMatKhauCu" />
													</p></td>
											</tr>
											<tr>
												<td>Mật khẩu mới:</td>
												<td><html:password styleClass="form-control"
														name="nhanVienForm" property="matKhauMoi" />
													<p style="color: red;">
														<html:errors property="errMatKhau" />
													</p></td>
											</tr>
											<tr>
												<td>Xác nhận mật khẩu</td>
												<td><html:password styleClass="form-control"
														name="nhanVienForm" property="matKhauXN" />
													<p style="color: red;">
														<html:errors property="errMatKhauXN" />
													</p></td>
											</tr>
										</tbody>
								</tbody>
							</table>
						</div>
					</div>
					<div class="panel-footer text-right">
						<html:submit styleClass="btn btn-warning" property="submit" value="Lưu"></html:submit>
						<html:link action="/nvThongTinCaNhan" styleClass="btn btn-default">Trở
							về</html:link>
					</div>
				</div>
				</html:form>
			</div>
		</div>
	</div>	
</body>
</html>
