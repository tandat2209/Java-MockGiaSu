<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

 <!-- header -->
    <nav class="navbar navbar-default" id="headerNV">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <html:link styleClass="navbar-brand" action="/dsduyetbaidang"><html:img styleId="logo" src="img/logo.png" alt="GiaSuDN" /></html:link>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a href="#" data-toggle="dropdown" role="" aria-haspopup="true" aria-expanded="false">
              <span class="glyphicon glyphicon-user"></span>
              <bean:write name="nvDangNhapForm" property="tenDangNhap"/><span class="caret"></span></a>
              <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                <li><html:link action="/nvThongTinCaNhan">Thông tin cá nhân</html:link></li>
                <li><html:link action="/dsduyetbaidang">Danh sách bài đăng</html:link></li>
                <li><html:link action="/dsNguoiDung">Danh sách người dùng</html:link></li>
                <li><html:link action="/nvDSThongBao">Danh sách thông báo</html:link></li>
                <li role="separator" class="divider"></li>
                <li><html:link action="/nvDangXuat">Đăng xuất</html:link>
              </ul>
            </li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>
     <!-- end header -->