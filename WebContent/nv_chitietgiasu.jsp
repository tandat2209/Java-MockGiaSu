<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Chi tiết bài đăng</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/myStyleSheet.css" rel="stylesheet">
<script src="js/jquery-1.11.3.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</head>
<body>

	<jsp:include page="header.jsp"></jsp:include>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3 col-xs-12">
				<!-- left panel -->
				<div id="left-panel">
					<div class="list-group">
						<html:link action="/nvThongTinCaNhan" styleClass="list-group-item">Thông tin cá nhân</html:link>
						<html:link action="/dsduyetbaidang" styleClass="list-group-item">Danh sách bài đăng</html:link>
						<html:link action="/dsNguoiDung" styleClass="list-group-item">Danh sách người dùng</html:link>
						<html:link action="/nvDSThongBao" styleClass="list-group-item">Danh sách thông báo</html:link>
					</div>
				</div>
			</div>
			<bean:define id="baidang" name="baiDangForm" property="baiDang" />
			<div class="col-md-8 col-xs-12">
				<div class="panel panel-warning">
					<div class="panel-heading">
						<div class="row">
							<div class="col-md-6 col-xs-12 title-left">
								<h4><bean:write name="baidang" property="tieuDe" /></h4>
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="row">
									<div class="title-right">
										Đăng bởi:
										<html:link action="/nvChiTietND?tenDangNhap=${maNguoiDang}">
											<bean:write name="baidang" property="maNguoiDang" />
										</html:link>
									</div>
								</div>
								<div class="row">
									<div class="title-right">
										Vào lúc:
										<bean:write name="baidang" property="thoiGianDang" />
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">Thông tin gia sư</h4>
									</div>
									<div class="panel-body">
										<table class="table table-default table-information">
											<tr>
												<td>Họ tên gia sư: <strong><bean:write
															name="baidang" property="tenGiaSu" /></strong></td>
											</tr>
											<tr>
												<td>Hiện đang là: <strong><bean:write
															name="baidang" property="ngheNghiep" /></td>
											</tr>
											<tr>
												<td>Nơi công tác: <strong><bean:write
															name="baidang" property="noiCongTac" /></strong></td>
											</tr>
											<tr>
												<td>Giới tính: <strong><bean:write
															name="baidang" property="gioiTinh" /></strong></td>
											</tr>
											<tr>
												<td>Ngày sinh: <strong><bean:write
															name="baidang" property="ngaySinh" /></strong></td>
											</tr>
											<tr>
												<td>Quê quán: <strong><bean:write
															name="baidang" property="queQuan" /></strong></td>
											</tr>
											<tr>
												<td>Thời gian rảnh: <strong><bean:write
															name="baidang" property="thoiGianDay" /></strong></td>
											</tr>
											<tr>
												<td>Số điện thoại: <strong><bean:write
															name="baidang" property="soDienThoai" /></strong></td>
											</tr>
										</table>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">Thông tin dạy</h4>
									</div>
									<div class="panel-body">
										<table class="table table-default table-information">
											<tr>
												<td>Lớp dạy: <strong><bean:write
															name="baidang" property="lop" /></strong></td>
											</tr>
											<tr>
												<td>Môn dạy: <strong><bean:write
															name="baidang" property="mon" /></strong></td>
											</tr>
											<tr>
												<td>Khu vực dạy: <strong> <logic:iterate
															id="kv" name="baiDangForm" property="khuVucList">
															<bean:write name="kv" property="tenKV" />
														</logic:iterate>
												</strong></td>
											</tr>
											
											<tr>
												<td>Lương: <strong><bean:write
															name="baidang" property="luongGS" /></strong> VNĐ/ buổi</td>
											</tr>
											
										
										</table>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading">Mô tả thêm</div>
									<div class="panel-body">
										<bean:write name="baidang" property="moTaThem" />
									</div>
								</div>
							</div>
						</div>
						
						<div class="panel-footer text-right">
							
							<logic:equal value="Chờ duyệt" name="baidang"
								property="trangThai">
								
								<html:form action="/nvXuLyBD" styleClass="form-button">
									<html:hidden name="baidang" property="maBD"/>
									<html:hidden name="baidang" property="xuly" value="duyet"/>
								<html:submit styleClass="btn btn-warning">Duyệt</html:submit>
								</html:form>
								
								
								<html:form action="/nvXuLyBD" styleClass="form-button">
									<html:hidden name="baidang" property="maBD"/>
									<html:hidden name="baidang" property="xuly" value="tuchoi"/>
								<html:submit styleClass="btn btn-danger">Từ chối</html:submit>
								</html:form>
								
							</logic:equal>
							<div class="form-button">
								<button class="btn btn-default" onclick="window.history.go(-1)">Trở về</button>
							</div>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>