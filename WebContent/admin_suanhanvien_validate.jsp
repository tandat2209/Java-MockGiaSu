<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sửa NV</title>
<script type="text/javascript">
	function xemMK(mk){
		alert(mk);
	}
</script>
</head>
<body>
	<h2>Sửa thông tin nhân viên</h2>
	<html:form action="admin_SuaValidateNhanVienAction">
		<label>Tên đăng nhập</label>
		<html:hidden name="admin_NhanVienValidateForm" property="tenDangNhap"/>
		<bean:write name="admin_NhanVienValidateForm" property="tenDangNhap"/>
		<br>
		<!-- Mật khẩu -->
		<label>Mật khẩu</label>
		<html:hidden property="matKhau" name="admin_NhanVienValidateForm"/>
		<bean:define id="mk" property="matKhau" name="admin_NhanVienValidateForm"></bean:define>
		<button onclick="xemMK('Mật khẩu là : ${mk}')">Nhấn để xem</button>
		<!--  -->
		<br>
		<label>Họ và tên</label>
		<html:text property="tenNV" name="admin_NhanVienValidateForm"></html:text>
		<html:errors property="tenNVError"/>
		<br>
		<label>Ngày sinh(*)</label>
		<html:text property="ngaySinh" name="admin_NhanVienValidateForm"></html:text>
		<html:errors property="ngaySinhError"/>
		<br>
		<label>Giới tính</label>
		<html:radio property="gioiTinh" value="Nam">Nam</html:radio>
		<html:radio property="gioiTinh" value="Nữ">Nữ</html:radio>
		<html:radio property="gioiTinh" value="Khác">Khác</html:radio>
		<br>
		<label>Địa chỉ</label>
		<html:text property="diaChi" name="admin_NhanVienValidateForm"></html:text>
		<br>
		<label>Email(*)</label>
		<html:text property="email" name="admin_NhanVienValidateForm"></html:text>
		<html:errors property="emailError"/>
		<br>
		<label>Số điện thoại(*)</label>
		<html:text property="soDienThoai" name="admin_NhanVienValidateForm"></html:text>
		<html:errors property="soDienThoaiError"/>
		<br>
		<html:submit property="submitSua" value="Lưu"></html:submit>
	</html:form>
	<html:form action="admin_dsnhanvien">
		<html:submit value="Hủy"></html:submit>
	</html:form>
</body>
</html>