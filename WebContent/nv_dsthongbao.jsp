<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Danh sách thông báo</title>
  <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/myStyleSheet.css" rel="stylesheet">
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>

	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3 col-xs-12">
				<!-- left panel -->
				<div id="left-panel">
					<div class="list-group">
						<html:link action="/nvThongTinCaNhan" styleClass="list-group-item">Thông tin cá nhân</html:link>
						<html:link action="/dsduyetbaidang" styleClass="list-group-item">Danh sách bài đăng</html:link>
						<html:link action="/dsNguoiDung" styleClass="list-group-item">Danh sách người dùng</html:link>
						<html:link action="/nvDSThongBao" styleClass="list-group-item">Danh sách thông báo</html:link>
					</div>
				</div>
			</div>
			<div class="col-md-8 col-xs-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Danh sách thông báo</h4>
					</div>
					<div class="panel-body">
						<table class="table table-striped">
							<thead>
								<tr>
									<th>Tiêu đề</th>
									<th>Người đăng</th>
									<th>Ngày báo</th>
									<th>Trạng thái</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<logic:iterate name="nvDSThongBaoForm" property="thongBaoList"
									id="thongbao">
									<tr>
										<bean:define id="maBD" name="thongbao" property="maBaiDang" />
										<td><html:link action="/nvChiTietBD?maBD=${maBD}">
												<bean:write name="thongbao" property="tieuDe" />
											</html:link></td>
										<td><bean:define id="maND" name="thongbao" property="maNguoiDang" />
											<html:link action="/nvChiTietND?tenDangNhap=${maND}">
												<bean:write name="thongbao" property="maNguoiDang" />
											</html:link></td>
										<td><bean:write name="thongbao" property="ngayBao" /></td>
										<td><bean:write name="thongbao" property="trangThai" /></td>
										<bean:define id="maTB" name="thongbao" property="maTB" />
										<td><html:link action="/nvChiTietTB?maTB=${maTB}">Xem</html:link></td>
									</tr>
								</logic:iterate>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>