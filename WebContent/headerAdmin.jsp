<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

 <!-- header -->
    <nav class="navbar navbar-default" id="headerNV">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <html:link styleClass="navbar-brand" action="/admin_dsnhanvien"><html:img styleId="logo" src="img/logo.png" alt="GiaSuDN" /></html:link>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav navbar-right">
            <li><html:link action="admin_dsnhanvien">Admin</html:link></li>
            <li><html:link action="nvDangXuat">Đăng xuất</html:link></li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>
     <!-- end header -->