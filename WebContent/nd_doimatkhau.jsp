<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Thông tin cá nhân</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/myStyleSheet.css" rel="stylesheet">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</head>
<body>

	<jsp:include page="headerND.jsp"></jsp:include>
	<div class="container content">
		<div class="thumbnail">
			<div class="row">
				<div class="col-md-3 col-xs-12">

					<p>
						<html:link styleClass="btn btn-default center-block btn-type"
							action="/dangBaiGiaSu">Đăng bài gia sư</html:link>
					</p>
					<p>
						<html:link styleClass="btn btn-default center-block btn-type"
							action="/dangBaiSuatDay">Đăng bài suất dạy</html:link>
					</p>
					<hr>
					<p>
						<html:link styleClass="btn btn-warning center-block btn-type"
							action="/ndThongtinCaNhan">Thông tin cá nhân</html:link>
					</p>
					<p>
						<html:link styleClass="btn btn-default center-block btn-type"
							action="/lichSuDangBai">Lịch sử đăng bài</html:link>
					</p>
					<p>
						<html:link styleClass="btn btn-default center-block btn-type"
							action="/ndThongBao">Danh sách thông báo</html:link>
					</p>

				</div>
				<div class="col-md-7">
					<html:form action="/ndDoiMatKhau">
						<div class="panel panel-warning">
							<div class="panel-heading">
								<h4 class="panel-title text-center">Đổi mật khẩu</h4>
							</div>
							<div class="panel-body">
								<div class="col-md-8 col-md-offset-2 ">
									<table class="table table-default table-information">
										<tbody>
											<tr>
												<td>Tên đăng nhập:</td>
												<td><strong><bean:write
															name="ndThongTinCaNhanForm" property="tenDangNhap" /></strong></td>
											</tr>
											<tr>
												<html:hidden property="tenDangNhap"
													name="ndThongTinCaNhanForm" />

												<td>Mật khẩu cũ:</td>
												<td><html:password styleClass="form-control"
														name="ndThongTinCaNhanForm" property="matKhauCu" />
													<p style="color: red;">
														<html:errors property="errMatKhauCu" />
													</p></td>
											</tr>
											<tr>
												<td>Mật khẩu mới:</td>
												<td><html:password styleClass="form-control"
														name="ndThongTinCaNhanForm" property="matKhauMoi" />
													<p style="color: red;">
														<html:errors property="errMatKhau" />
													</p></td>
											</tr>
											<tr>
												<td>Xác nhận mật khẩu</td>
												<td><html:password styleClass="form-control"
														name="ndThongTinCaNhanForm" property="matKhauXN" />
													<p style="color: red;">
														<html:errors property="errMatKhauXN" />
													</p></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<bean:define id="tenDangNhap" name="ndThongTinCaNhanForm"
								property="tenDangNhap"></bean:define>
							<div class="panel-footer text-right">
								<html:submit property="submit" styleClass="btn btn-warning"
									value="Lưu">Lưu</html:submit>
								<html:link action="/ndThongtinCaNhan"
									styleClass="btn btn-default">Hủy</html:link>
							</div>
						</div>
					</html:form>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>