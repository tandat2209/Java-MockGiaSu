<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Thêm nhân viên</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/myStyleSheet.css" rel="stylesheet">

<link rel="stylesheet" href="css/bootstrap.min.css" />
<link rel="stylesheet" href="css/style.css" />
<script src="js/jquery-1.11.2.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/PaginationJS.js"></script>
</head>
<body>
	<jsp:include page="headerAdmin.jsp"></jsp:include>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3 col-xs-12">
				<div id="left-panel">
					<div class="list-group">
						<html:link action="admin_ThemNhanVienAction"
							styleClass="list-group-item">Thêm nhân viên</html:link>
						<html:link action="admin_dsnhanvien" styleClass="list-group-item">Danh sách nhân viên</html:link>
					</div>
				</div>
			</div>
	
			<div class="col-md-6">
				<div class="panel panel-warning">
					<div class="panel-heading">
						<h4 class="panel-title text-center">Thông tin nhân viên <em> <bean:write property="tenDangNhap" name="admin_NhanVienForm"/></em></h4>
					</div>
					<div class="panel-body">
						<div class="col-md-4">
							<img src="img/avatar.png" alt="avatar"
								class="img-circle img-responsive">
						</div>
						<div class="col-md-8">
							<table class="table table-default table-information">
								<tbody>
									<tr>
										<td>Tên đăng nhập</td>
										<td><bean:write name="admin_NhanVienForm" property="tenDangNhap"/></td>
									</tr>
									<tr>
										<td>Họ tên nhân viên</td>
										<td><bean:write name="admin_NhanVienForm" property="tenNV" /></td>
									</tr>
									<tr>
										<td>Ngày sinh</td>
										<td><bean:write property="ngaySinh" name="admin_NhanVienForm"/></td>
									</tr>
									<tr>
										<td>Giới tính</td>
										<td><bean:write property="gioiTinh" name="admin_NhanVienForm"/></td>
									</tr>
									<tr>
										<td>Địa chỉ</td>
										<td><bean:write property="diaChi" name="admin_NhanVienForm"/></td>
									</tr>
									<tr>
										<td>Điện thoại</td>
										<td><bean:write property="soDienThoai" name="admin_NhanVienForm"/></td>
									</tr>
									<tr>
										<td>Email</td>
										<td><bean:write property="email" name="admin_NhanVienForm"/></td>
									</tr>
								</tbody>
							</table>
							
						</div>
					</div>
					<div class="panel-footer text-right">
						<bean:define id="tenDangNhap" name="admin_NhanVienForm" property="tenDangNhap"/>
						<html:link action="admin_SuaNhanVienAction?tenDangNhap=${tenDangNhap}" styleClass="btn btn-default">Sửa</html:link>
						<html:link action="admin_dsnhanvien" styleClass="btn btn-default">Trở về</html:link>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</body>
</html>