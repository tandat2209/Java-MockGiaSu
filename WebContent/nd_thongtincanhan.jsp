<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Thông tin cá nhân</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/myStyleSheet.css" rel="stylesheet">
<script src="js/jquery-1.11.3.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</head>
<body >

	<jsp:include page="headerND.jsp"></jsp:include>
	<div class="container content">
		<div class="thumbnail">
		<div class="row">
			<div class="col-md-3 col-xs-12">

		         <p><html:link styleClass="btn btn-default center-block btn-type" action="/dangBaiGiaSu">Đăng bài gia sư</html:link></p>
		         <p><html:link styleClass="btn btn-default center-block btn-type" action="/dangBaiSuatDay">Đăng bài suất dạy</html:link></p>
		   		<hr>	
		         <p><html:link styleClass="btn btn-warning center-block btn-type" action="/ndThongtinCaNhan">Thông tin cá nhân</html:link></p>
		         <p><html:link styleClass="btn btn-default center-block btn-type" action="/lichSuDangBai">Lịch sử đăng bài</html:link></p>
		         <p><html:link styleClass="btn btn-default center-block btn-type" action="/ndThongBao">Danh sách thông báo</html:link></p>
		 
			</div>
			<div class="col-md-7">
				<div class="panel panel-warning">
					<div class="panel-heading">
						<h4 class="panel-title text-center">Thông tin cá nhân</h4>
					</div>
					<div class="panel-body">
						<div class="col-md-4">
							<logic:empty name="ndThongTinCaNhanForm" property="anhDaiDien">
							<img src="img/avatar.png" alt="avatar"
								class="img-circle img-responsive">
							</logic:empty>
							<logic:present name="ndThongTinCaNhanForm" property="anhDaiDien">
							<div class="avatar"><bean:define id="path" name="ndThongTinCaNhanForm" property="anhDaiDien"></bean:define>
								<img src="${path}" alt="avatar"
								class="img-responsive"></div>
							</logic:present>
							<bean:define id="tenDangNhap" name="ndThongTinCaNhanForm" property="tenDangNhap"/>
							<h6><html:link action="ndDoiAnhDaiDien?tenDangNhap=${tenDangNhap}"
								styleClass="btn btn-link btn-sm">Đổi ảnh đại diện</html:link> </h6>
						</div>
						<div class="col-md-8">
							<table class="table table-default table-information">
								<tbody>
									<tr>
										<td>Tên đăng nhập</td>
										<td><bean:write name="ndThongTinCaNhanForm"
												property="tenDangNhap" /></td>
									</tr>
									<tr>
										<td>Mật khẩu</td>
										<bean:define id="matKhau" property="matKhau" name="ndThongTinCaNhanForm"/>
										<bean:define id="tenDangNhap" name="ndThongTinCaNhanForm" property="tenDangNhap"/>
										<td><html:link action="/ndDoiMatKhau?tenDangNhap=${tenDangNhap}" >Đổi mật khẩu</html:link></td>  
									</tr>
									<tr>
										<td>Họ và tên</td>
										<td><bean:write name="ndThongTinCaNhanForm"
												property="tenND" /></td>
									</tr>
									<tr>
										<td>Ngày sinh</td>
										<td><bean:write name="ndThongTinCaNhanForm"
												property="ngaySinh" /></td>
									</tr>
									<tr>
										<td>Giới tính</td>
										<td><bean:write name="ndThongTinCaNhanForm"
												property="gioiTinh" /></td>
									</tr>
									<tr>
										<td>Điện thoại</td>
										<td><bean:write name="ndThongTinCaNhanForm"
												property="soDienThoai" /></td>
									</tr>
									<tr>
										<td>Email</td>
										<td><bean:write name="ndThongTinCaNhanForm"
												property="email" /></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="panel-footer text-right">
						<html:link action="/ndSuaThongTin" styleClass="btn btn-warning">Cập nhật thông tin</html:link>
					</div>
				</div>
			</div>
			</div>
		</div>
	</div>
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>