<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Thông tin cá nhân</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/myStyleSheet.css" rel="stylesheet">
<script src="js/jquery-1.11.3.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3 col-xs-12">
				<!-- left panel -->
				<div id="left-panel">
					<div class="list-group">
						<html:link action="/nvThongTinCaNhan" styleClass="list-group-item">Thông tin cá nhân</html:link>
						<html:link action="/dsduyetbaidang" styleClass="list-group-item">Danh sách bài đăng</html:link>
						<html:link action="/dsNguoiDung" styleClass="list-group-item">Danh sách người dùng</html:link>
						<html:link action="/nvDSThongBao" styleClass="list-group-item">Danh sách thông báo</html:link>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="panel panel-warning">
					<div class="panel-heading">
						<h4 class="panel-title text-center">Thông tin cá nhân</h4>
					</div>
					<div class="panel-body">
						<div class="col-md-4">
							<img src="img/avatar.png" alt="avatar"
								class="img-circle img-responsive">
						</div>
						<div class="col-md-8">
							<table class="table table-default table-information">
								<tbody>
									<tr>
										<td>Tên đăng nhập</td>
										<td><bean:write name="nhanVienForm"
												property="tenDangNhap" /></td>
									</tr>
									<tr>
										<td>Mật khẩu</td>
										<bean:define id="tenDangNhap" name="nhanVienForm" property="tenDangNhap"/>
										<td><html:link styleClass="btn btn-link btn-xs" action="/nvDoiMatKhau?tenDangNhap=${tenDangNhap }" >Đổi mật khẩu</html:link></td>
									</tr>
									<tr>
										<td>Họ và tên</td>
										<td><bean:write name="nhanVienForm" property="tenNV" /></td>
									</tr>
									<tr>
										<td>Ngày sinh</td>
										<td><bean:write name="nhanVienForm" property="ngaySinh" /></td>
									</tr>
									<tr>
										<td>Giới tính</td>
										<td><bean:write name="nhanVienForm" property="gioiTinh" /></td>
									</tr>
									<tr>
										<td>Địa chỉ</td>
										<td><bean:write name="nhanVienForm" property="diaChi" /></td>
									</tr>
									<tr>
										<td>Điện thoại</td>
										<td><bean:write name="nhanVienForm"
												property="soDienThoai" /></td>
									</tr>
									<tr>
										<td>Email</td>
										<td><bean:write name="nhanVienForm" property="email" /></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="panel-footer text-right">
						<html:link action="/nvCapNhatThongTin?tenDangNhap=${tenDangNhap}"
							styleClass="btn btn-warning">Cập nhật thông tin</html:link>
						<html:link action="/dsduyetbaidang" styleClass="btn btn-default">Trở
							về</html:link>
					</div>
				</div>
			</div>
		</div>
	</div>	
</body>
</html>
