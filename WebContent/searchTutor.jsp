<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html>
<html:html lang="en">
	<head>
		<title>Tìm kiếm gia sư</title>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="bootstrap-theme.min.css">
		<link rel="stylesheet" href="css/myStyleSheet.css">
		<script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		<script type="text/javascript" src="js/myJavaScript.js"></script>
		<script type="text/javascript" src="js/humanized_time_span.js"></script>
		<script src="js/PaginationJS.js"></script>	    
		<script type="text/javascript">
			$(document).ready(function() {
	
				$('#showtable').pageMe({
					pagerSelector : '#myPager',
					showPrevNext : true,
					hidePageNumbers : false,
					perPage : 5
				});
			});
		</script>
	</head>
	<body>
	<jsp:include page="headerND.jsp"></jsp:include>
	
	<div class="container content">
	   <div class="thumbnail">
	   <div class="row">
	     <div class="col-md-2">
	         <p><html:link action="/searchTutorAction.do" styleClass="btn btn-warning center-block btn-type" >Tìm gia sư</html:link></p>
         	<p><html:link action="/searchCourseAction.do" styleClass="btn btn-default center-block btn-type" >Tìm suất dạy</html:link></p>
	     </div>
	     <div class="col-md-8"> <!-- main search -->
	       <div class="row">  <!-- search form row -->
	         <div class="col-md-12">
	           <div class="panel panel-danger">
	             <div class="panel-body"> <!-- panel body -->
	               <html:form action="/searchTutorAction"> <!-- search form -->
	                 <div class="row">
	                   <div class="col-md-10">
	                      <div class="row">
	                        <div class="col-md-12 form-group">
	                          <html:text property="input" styleClass="form-control search-input"></html:text>
	                        </div>
	                      </div>
	                      <div id="search-indicator" class="btn btn-link btn-xs">Tìm kiếm nâng cao</div>
	                      <div class="row form-group advanced-search">
	                      <div class="col-md-4">
	                        <html:select property="career" styleId="" styleClass="form-control">
								<html:option value="0"> Hiện đang là </html:option>
								<html:option value="Giáo viên"> Giáo viên </html:option>
								<html:option value="Sinh viên"> Sinh viên </html:option>
								<html:option value="Đã tốt nghiệp"> Đã tốt nghiệp </html:option>
								<html:option value="Khác"> Khác </html:option>
							</html:select>
						   </div>
	                        <div class="col-md-4">
	                          <html:select property="hometown" styleId="" styleClass="form-control">
								<html:option value="0"> Quê quán </html:option>
								<html:option value="Hà Tĩnh"> Hà Tĩnh </html:option>
								<html:option value="Quảng Bình"> Quảng Bình </html:option>
								<html:option value="Quảng Trị"> Quảng Trị </html:option>
								<html:option value="Huế"> Thừa Thiên Huế </html:option>
								<html:option value="Đà Nẵng"> Đà Nẵng </html:option>
								<html:option value="Quảng Nam"> Quảng Nam </html:option>
								<html:option value="Quảng Ngãi"> Quảng Ngãi </html:option>
							</html:select>
	                        </div>
	                        <div class="col-md-4">
	                          <html:select property="gender" styleId="" styleClass="form-control">
								<html:option value="0"> Giới Tính </html:option>
								<html:option value="Nam"> Nam </html:option>
								<html:option value="Nữ"> Nữ </html:option>
								<html:option value="Khác"> Khác </html:option>
							</html:select>
	                        </div>
	                      </div>
	                      
	                      
	                      <div class="row form-group advanced-search">
	                  	<div class="col-md-3">
	                  		<label for="">Khu vực</label>
	                  	</div>
	                  	<div class="col-md-9">
	                  		<div class="row">
	                  			<div class="col-md-4">
	                  				<html:multibox property="area" value="Cẩm Lệ" /> Cẩm lệ
	                  			</div>
	                  			<div class="col-md-4">
	                  				<html:multibox property="area" value="Hải Châu" /> Hải châu
	                  			</div>
	                  			<div class="col-md-4">
	                  				<html:multibox property="area" value="Liên Chiểu" /> Liên chiểu
	                  			</div>
	                  			<div class="col-md-4">
	                  				<html:multibox property="area" value="Ngũ Hành Sơn" /> Ngũ hành sơn
	                  			</div>
	                  			<div class="col-md-4">
	                  				<html:multibox property="area" value="Thanh Khê" /> Thanh khê
	                  			</div>
	                  			<div class="col-md-4">
	                  				<html:multibox property="area" value="Sơn Trà" /> Sơn trà
	                  			</div>
	                  		</div>
	                  	</div>
	                  </div>
	                      
	                      
	                      
	                      
	                      <div class="row form-group advanced-search">
	                        <div class="col-md-4">
	                          <label for="">Lương/ buổi:</label>
	                        </div>
	                        <div class="col-md-4">
	                          <html:select property="minOfWage" styleId="" styleClass="form-control">
									<html:option value="0">Lương thấp nhất</html:option>
									<html:option value="50000">50.000</html:option>
									<html:option value="80000">80.000</html:option>
									<html:option value="12000">120.000</html:option>
									<html:option value="150000">150.000</html:option>
									<html:option value="180000">180.000</html:option>
									<html:option value="200000">200.000</html:option>
								</html:select>
	                        </div>
	                        <div class="col-md-4">
	                          <html:select property="maxOfWage" styleId="" styleClass="form-control">
									<html:option value="0">Lương cao nhất</html:option>
									<html:option value="50000">50.000</html:option>
									<html:option value="80000">80.000</html:option>
									<html:option value="12000">120.000</html:option>
									<html:option value="150000">150.000</html:option>
									<html:option value="180000">180.000</html:option>
									<html:option value="200000">200.000</html:option>
								</html:select>
	                        </div>
	                      </div>
	                   </div>
	                   <div class="col-md-2">
	                     <html:submit styleClass="btn btn-warning">Tìm kiếm</html:submit>
	                   </div>
	                 </div>
	               </html:form>  <!-- end search form -->
	                
	             </div> <!-- end panel body -->
	           </div>
	         </div>
	       </div> <!-- end search form row -->
	       <div class="bs-example">
					<div id ="showtable">
				    	<logic:iterate id="x" name="searchTutorForm" property="tutorList">
					       <div class="row"> 
					        <div class="col-md-12">
					          <div class="panel panel-success"> <!-- result panel -->
					            <div class="panel-body">
					               <div class="row"> <!-- title row -->
					                <div class="col-md-7">
					                	<bean:define id="y" name="x" property="maBD"></bean:define>
					                  <h4><html:link
											action="/chiTietBaiDangAction?maBD=${y}">
											<div class="indent-title">Gia sư</div><bean:write name="x" property="tieuDe" />
										</html:link></h4>
					                </div>
					                <div class="col-md-5">
					                	<h6>Đăng lúc: 
					                	<span class="date">
					                	<bean:write name="x" property="thoiGianDang"/>
					                	</span></h6>
					                </div>
					              </div> <!-- end title row -->
					              <div class="row"><!--  content row -->
					                <div class="col-md-6">
					                  <bean:write name="x" property="ngheNghiep" />,<bean:write name="x" property="gioiTinh" />
					                </div>
					                <div class="col-md-6">
					                  Quê quán: <bean:write name="x" property="queQuan" />
					                </div>
					                <div class="col-md-6">
					                  Nơi công tác: <bean:write name="x" property="noiCongTac" />
					                </div>
					                <div class="col-md-6">
					                  Lương: <bean:write name="x" property="luongGS" /> VND/ buổi
					                </div>
					              </div> <!-- end content row -->
					            </div>
					          </div>  <!-- end result panel -->
					        </div>
					       </div>
					      </logic:iterate>
				     </div>
				    	<div class="row" style="text-align: center;">
							<div class="col-lg-4"></div>
							<div class="col-lg-4">
								<ul style="margin: 0px" class="pagination pagination-sm"
										id="myPager"></ul>
							</div>
						</div>
				    </div> 
	     </div> <!-- end main-search -->
	     <div class="col-md-2">
	        <div class="thumbnail">
	          <img src="img/dat-quang-cao.gif" alt="" style="border-radius: 3px"><br>
	          <img src="img/FPT1.gif" alt="" style="border-radius: 3px">
	        </div>
	     </div>
	   </div>
	   </div>
	  </div>
	  <jsp:include page="footer.jsp"></jsp:include>
	</body>
</html:html>