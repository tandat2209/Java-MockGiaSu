<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!DOCTYPE html>
<html>
<head>
<title>Quên mật khẩu</title>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="bootstrap-theme.min.css">
<link rel="stylesheet" href="css/myStyleSheet.css">
<script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/myJavaScript.js"></script>
</head>
<body>
<jsp:include page="headerND.jsp"></jsp:include>
<div class="container content">
	<div class="col-md-4 col-md-offset-4">
	<div class="panel panel-default">
		<div class="panel-body">
			<logic:present name="ndThongTinCaNhanForm" property="successMS">
				<h5><bean:write name="ndThongTinCaNhanForm" property="successMS"/> </h5>
			</logic:present>
			<logic:notPresent name="ndThongTinCaNhanForm" property="successMS">
			<h4>Nhập thông tin để reset mật khẩu</h4><hr>
			<html:form action="/ndQuenMatKhau">
				<label>Nhập Tên đăng nhập</label>
				<html:text property="tenDangNhap" styleClass="form-control"/>
				<br>
				<label>Nhập Email</label>
				<html:text property="email" styleClass="form-control" />
				<br>
				<html:errors property="errEmail" />
				<bean:write name="ndThongTinCaNhanForm" property="errorMS" />
				<hr>
				
				<html:submit styleClass="btn btn-default">Xác nhận</html:submit>
			</html:form>
			</logic:notPresent>
		</div>
	</div>
	</div>
</div>
<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>