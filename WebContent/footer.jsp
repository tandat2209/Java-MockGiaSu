<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

  <div class="row" id="footer">
  	<div class="col-md-3 col-md-offset-1">
		<div class="footer-left">
			<label>Liên kết</label> <hr>
			<a href="https://www.facebook.com/tandat2209" target="blank"><img src="img/facebook-logo.png" alt=""> Facebook</a><br>
			<a href="https://www.facebook.com/tandat2209" target="blank"><img src="img/googleplus-logo.png" alt=""> Google</a><br>
			<a href="https://www.facebook.com/tandat2209" target="blank"><img src="img/twitter-logo.png" alt=""> Twitter</a><br>
		</div>
  	</div>
  	<div class="col-md-3">
  		<div class="footer-center">
  			<label>Về trang web</label><hr>
			Trang web được xây dựng với mục đích tạo một cầu nối giữa gia sư và phụ huynh, giúp sự tương tác giữa gia sư và phụ huynh trở nên dễ dàng hơn bao giờ hết!
  		</div>
  	</div>
  	<div class="col-md-3">
  		<div class="footer-right">
  			<label>Team Bất Lực quá cực vì Mock</label><hr>
  			<ol>
  				<li>Nguyễn Văn Dũng</li>
  				<li>Nguyễn Văn Tấn Đạt</li>
  				<li>Nguyễn Viết Huy</li>
  				<li>Hoàng Thị Mai Liên</li>
  			</ol>
  		</div>
  	</div>
  </div>
