<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Danh sách người dùng</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/myStyleSheet.css" rel="stylesheet">
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </head>
  <body>
     <jsp:include page="header.jsp"></jsp:include>

     <div class="container-fluid">
      <div class="row"> 
         <div class="col-md-3 col-xs-12"> <!-- left panel -->
          <div id="left-panel">
              <div class="list-group">
                <html:link action="/nvThongTinCaNhan" styleClass="list-group-item">Thông tin cá nhân</html:link>
                <html:link action="/dsduyetbaidang" styleClass="list-group-item">Danh sách bài đăng</html:link>
                <html:link action="/dsNguoiDung" styleClass="list-group-item">Danh sách người dùng</html:link>
                <html:link action="/nvDSThongBao" styleClass="list-group-item">Danh sách thông báo</html:link>
              </div>
          </div>
         </div>
         <div class="col-md-8 col-xs-12">
          <div class="panel panel-default">
            <div class="panel-heading">
            <div class="row">
              <div class="col-md-4 col-sm-4 col-xs-12">
                <h3 class="panel-title">Danh sách người dùng</h3>
              </div>
              <div class="col-md-8 col-sm-8 col-xs-12">
              <html:form action="/dsNguoiDung" styleClass="form-inline text-right">
                <html:text styleClass="form-control"  property="searchString"/>
                <html:submit styleClass="btn btn-default">Tìm kiếm</html:submit>
              </html:form>
              </div>
             </div>
            </div>
            <div class="panel-body">
              <table class="table table-striped">
                <thead>
                <tr>
                  <th>Tên đăng nhập</th>
                  <th>Họ tên</th>
                  <th>Số bài đăng</th>
                  <th>Trạng thái</th>
                </tr>
                </thead>
                <logic:iterate name="nvDSNguoiDungForm" property="nguoiDungArray" id="nguoidung">
                  <tr>
                    <bean:define id="tenDangNhap" name="nguoidung" property="tenDangNhap"></bean:define>
                    <td><html:link action="/nvChiTietND?tenDangNhap=${tenDangNhap}"><bean:write name="nguoidung" property="tenDangNhap"/></html:link></td>
                    <td><bean:write name="nguoidung" property="hoTen"/></td>
                    <td><bean:write name="nguoidung" property="soBaiDang"/></td>
                    <td><bean:write name="nguoidung" property="trangThai"/></td>
                  </tr>
                </logic:iterate>
              </table>
            </div>
          </div>
        </div>
      </div>
	</div>
    
  </body>
</html>         