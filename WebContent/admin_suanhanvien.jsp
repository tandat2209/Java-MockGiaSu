<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Sửa thông tin nhân viên</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/myStyleSheet.css" rel="stylesheet">

<link rel="stylesheet" href="css/bootstrap.min.css" />
<link rel="stylesheet" href="css/myStyleSheet.css" />
<script src="js/jquery-1.11.2.min.js"></script>
<script src="js/bootstrap.min.js"></script>

<script type="text/javascript">
	var r = "";
	function xemMK(mk) {
		alert(mk);
	}
</script>
</head>
<body>
	<jsp:include page="headerAdmin.jsp"></jsp:include>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3 col-xs-12">
				<div id="left-panel">
					<div class="list-group">
						<html:link action="admin_ThemNhanVienAction"
							styleClass="list-group-item">Thêm nhân viên</html:link>
						<html:link action="admin_dsnhanvien" styleClass="list-group-item">Danh sách nhân viên</html:link>
					</div>
				</div>
			</div>

			<div class="col-md-6 col-xs-12">
				<div class="panel panel-warning">
					<div class="panel-body">
						<h4 class="text-center">Sửa nhân viên</h4>
						<hr>
						<html:form action="/admin_SuaNhanVienAction">
									<table class="table table-default table-information">
										<tbody>
											<tr>
												<td><label>Tên đăng nhập</label></td>
												<td><html:hidden name="admin_NhanVienForm"
														property="tenDangNhap" /> <bean:write
														name="admin_NhanVienForm" property="tenDangNhap" /></td>
											</tr>
											<tr>
												<td><label>Mật khẩu</label></td>
												<td><html:hidden property="matKhau"
														name="admin_NhanVienForm" /> <bean:define id="mk"
														property="matKhau" name="admin_NhanVienForm"></bean:define>
													<button onclick="xemMK('Mật khẩu là : ${mk}')">Nhấn
														để xem</button></td>
											</tr>
											<tr>
												<td><label>Họ tên nhân viên</label></td>
												<td><html:text property="tenNV"
														styleClass="form-control"></html:text>
													<p style="color: red;">
														<html:errors property="tenNVError" />
													</p></td>
											</tr>
											<tr>
												<td><label>Ngày sinh</label></td>
												<td><html:text property="ngaySinh"
														styleClass="form-control"></html:text>
													<p style="color: red;">
														<html:errors property="ngaySinhError" />
													</p></td>
											</tr>
											<tr>
												<td><label>Giới tính</label></td>
												<td><html:radio property="gioiTinh" value="Nam"
														name="admin_NhanVienForm" styleClass="checkbox-inline"> Nam</html:radio>
													<html:radio property="gioiTinh" value="Nữ"
														name="admin_NhanVienForm" styleClass="checkbox-inline"> Nữ</html:radio>
													<html:radio property="gioiTinh" value="Khác"
														name="admin_NhanVienForm" styleClass="checkbox-inline"> Khác</html:radio>
												</td>
											</tr>
											<tr>
												<td><label>Địa chỉ</label></td>
												<td><html:text property="diaChi"
														styleClass="form-control"></html:text></td>
											</tr>
											<tr>
												<td><label>Email</label></td>
												<td><html:text property="email"
														styleClass="form-control"></html:text>
													<p style="color: red;">
														<html:errors property="emailError" />
													</p></td>
											</tr>
											<tr>
												<td><label>Số điện thoại</label></td>
												<td><html:text property="soDienThoai"
														styleClass="form-control"></html:text>
													<p style="color: red;">
														<html:errors property="soDienThoaiError" />
													</p></td>
											</tr>
										</tbody>
									</table>
									<hr>
									<div class="text-right">
										<html:submit property="submitSua" styleClass="btn btn-warning"
											value="Lưu">Lưu</html:submit>
										<html:link styleClass="btn btn-default"
											action="admin_dsnhanvien">Hủy</html:link>
									</div>
						</html:form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>