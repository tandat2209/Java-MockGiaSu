<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Chi tiết thông báo</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/myStyleSheet.css" rel="stylesheet">
<script src="js/jquery-1.11.3.min.js"></script>
<script src="js/bootstrap.min.js"></script>>
</head>
<body>

	<jsp:include page="header.jsp"></jsp:include>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3 col-xs-12">
				<!-- left panel -->
				<div id="left-panel">
					<div class="list-group">
						<html:link action="/nvThongTinCaNhan" styleClass="list-group-item">Thông tin cá nhân</html:link>
						<html:link action="/dsduyetbaidang" styleClass="list-group-item">Danh sách bài đăng</html:link>
						<html:link action="/dsNguoiDung" styleClass="list-group-item">Danh sách người dùng</html:link>
						<html:link action="/nvDSThongBao" styleClass="list-group-item">Danh sách thông báo</html:link>
					</div>
				</div>
			</div>
			<bean:define id="thongbao" name="thongBaoForm" property="thongBao" />
			<div class="col-md-6 col-xs-12">
				<div class="panel panel-warning">
					<div class="panel-heading">
						<h4 class="panel-title">Chi tiết thông báo</h4>
					</div>
					<div class="panel-body">
						<table class="table table-default table-information">
							<tr>
								<td>Email:</td>
								<td><bean:write name="thongbao" property="email" /></td>
							</tr>
							<tr>
								<td>Số điện thoại:</td>
								<td><bean:write name="thongbao" property="soDienThoai" /></td>
							</tr>
							<tr>
								<td>Nội dung:</td>
								<td><bean:write name="thongbao" property="noiDung" /></td>
							</tr>
							<bean:define id="maBD" name="thongbao" property="maBaiDang" />
							<tr>
								<td>Bài đăng:</td>
								<td><html:link action="/nvChiTietBD?maBD=${maBD}">
										<bean:write name="thongbao" property="tieuDe" />
									</html:link></td>
							</tr>
						</table>
					</div>
					<div class="panel-footer text-right">
						<logic:equal value="Chờ xử lý" name="thongbao"
							property="trangThai">
							<bean:define id="maTB" name="thongbao" property="maTB"></bean:define>
							<html:form action="/nvXuLyTB" styleClass="form-button">
								<html:hidden property="maTB" value="${maTB}"/>
								<html:hidden property="xuly" value="an"/>
								<html:submit styleClass="btn btn-danger">Ẩn và Cảnh báo người đăng</html:submit>
							</html:form>
							<html:form action="/nvXuLyTB" styleClass="form-button">
								<html:hidden property="maTB" value="${maTB}"/>
								<html:hidden property="xuly" value="boqua"/>
								<html:submit styleClass="btn btn-default"> Bỏ qua</html:submit>
							</html:form>
						</logic:equal>
						<button class="btn btn-default" onclick="window.history.go(-1)">Trở
							về</button>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>