<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<nav>
  <div class="row" id="header">
  	<div class="col-md-3 col-md-offset-1">
  		<html:link action="/searchTutorAction"><img id="logo" src="img/logo.png" alt=""></html:link>
  	</div>
  	<div class="col-md-5 col-md-offset-3 header-menu">
  		<logic:empty name="ndDangNhapForm">
	  		<ul>
		  		<li><html:link action="/searchCourseAction"><span class="glyphicon glyphicon-search"></span> Tìm kiếm</html:link></li>
		  		<li><html:link action="/ndDangNhap"> Đăng nhập</html:link></li>
		  		<li><html:link action="/dangKyAction"> Đăng ký</html:link></li>
	  		</ul>
  		</logic:empty>
  		<logic:notEmpty name="ndDangNhapForm">
  		<logic:equal value="true" name="ndDangNhapForm" property="logined">
  			<ul>
  				<li><html:link action="/searchCourseAction"><span class="glyphicon glyphicon-search"></span> Tìm kiếm</html:link></li>
  				<li>
  					<span class="glyphicon glyphicon-pencil"></span> Đăng bài
	  				<ul>
	  					<li><html:link action="/dangBaiGiaSu">Gia sư</html:link></li>
	  					<li><html:link action="/dangBaiSuatDay">Suất dạy</html:link></li>
	  				</ul>
  				</li>
	  			<li><html:link action="ndThongtinCaNhan"><span class="glyphicon glyphicon-user"></span>
	  				<bean:write name="ndDangNhapForm" property="tenDangNhap"/>
	  			</html:link>
						<ul>
							<li><html:link action="/ndThongtinCaNhan">Thông tin cá nhân</html:link></li>
							<li><html:link action="/lichSuDangBai">Lịch sử đăng bài</html:link></li>
							<li><html:link action="/ndDangXuat">Đăng xuất</html:link></li>
						</ul>
	  			</li>
  			</ul>
  		</logic:equal>
  		<logic:equal value="false" name="ndDangNhapForm" property="logined">
  			<ul>
		  		<li><html:link action="/searchCourseAction"><span class="glyphicon glyphicon-search"></span> Tìm kiếm</html:link></li>
		  		<li><html:link action="/ndDangNhap"> Đăng nhập</html:link></li>
		  		<li><html:link action="/dangKyAction"> Đăng ký</html:link></li>
	  		</ul>
  		</logic:equal>
  		</logic:notEmpty>
  	</div>
  </div>
</nav>