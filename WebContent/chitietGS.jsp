<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Chi tiết gia sư</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/myStyleSheet.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript">
function load(){
	document.getElementById("smsLoi").innerHTML = "";
}
function myFunction() {
    var phone = document.getElementById("phone").value;
    var email = document.getElementById("email").value;
    var content = document.getElementById("content").value;
    var regexPhone = /^\d+$/;
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    

	if( phone=="" || email=="" || content==""){
		document.getElementById("smsLoi").innerHTML = "Vui lòng nhập đẩy đủ thông tin!!!";
		return false;
	} else
	if(!regexPhone.test(phone)){
		document.getElementById("smsLoi").innerHTML = "Số điện thoại không chính xác!!!";
		return false;
	}
    if (phone.length<10||phone.length>11) {
    	document.getElementById("smsLoi").innerHTML = "Số điện thoại không chính xác!!!";
    	return false;
    } else
    if(!re.test(email)){
    	document.getElementById("smsLoi").innerHTML = "Email sai định dạng!!!";
    	return false;
    }
    else {
        alert("Gửi báo cáo thành công!!");
        return true;
    }
}


</script>
</head>
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.5&appId=1509955279314575";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<script src="https://apis.google.com/js/platform.js" async defer>
  {lang: 'vi'}
</script>
<script src="https://apis.google.com/js/platform.js" async defer></script>
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
		aria-labelledby="exampleModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" onclick="load()" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="exampleModalLabel">Báo cáo sai phạm</h4>
				</div>
				<html:form action="/toCaoAction">
				<div class="modal-body">
						<html:hidden property="maBD"/>
						<div class="form-group">
							<label class="control-label">Số điện thoại</label>
							<html:text styleClass="form-control" styleId="phone" property="phone"></html:text>
						</div>
						<div class="form-group">
							<label class="control-label">Email</label>
							<html:text styleClass="form-control" styleId="email" property="email"></html:text>
						</div>
						<div class="form-group">
							<label class="control-label">Nội dung</label>
							<html:textarea styleClass="form-control" styleId="content" property="content"></html:textarea>
						</div>
						<p>
						<label id="smsLoi"></label>
						</p>
				</div>
				<div class="modal-footer">
					<button type="button" onclick="load()"  class="btn btn-default" data-dismiss="modal">Close</button>
					<html:submit onclick="return myFunction()" styleClass="btn btn-danger">Gửi</html:submit>
				</div>
				</html:form>
			</div>
		</div>
	</div>
	<jsp:include page="headerND.jsp"></jsp:include>
	<div class="container content">
	<div class="thumbnail">
		<div class="row">
			<div class="col-md-2 col-xs-12">
				<p><html:link styleClass="btn btn-warning center-block btn-type" action="/searchTutorAction">Tìm gia sư</html:link></p>
		         <p><html:link styleClass="btn btn-default center-block btn-type" action="/searchCourseAction">Tìm suất dạy</html:link></p>

			</div>
			<bean:define id="baidang" name="ndChiTietBaiDangForm" property="baiDang" />
			<div class="col-md-7 col-xs-12">
				<div class="panel panel-warning">
					<div class="panel-heading">
						<div class="row">
							<div class="col-md-6 col-xs-12 title-left">
								<span style="font-size: 13px;">Gia sư:</span>
								<h4> <bean:write name="baidang" property="tieuDe" /></h4>
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="row">
									<div class="title-right">
										Đăng bởi:
										<bean:define id="maNguoiDang" name="baidang" property="maNguoiDang" />
										<html:link action="/nvChiTietND?tenDangNhap=${maNguoiDang}">
											<bean:write name="baidang" property="maNguoiDang" />
										</html:link>
									</div>
								</div>
								<div class="row">
									<div class="title-right">
										Vào lúc:
										<bean:write name="baidang" property="thoiGianDang" />
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">Thông tin gia sư</h4>
									</div>
									<div class="panel-body">
										<table class="table table-default table-information">
											<tr>
												<td>Họ tên gia sư: <strong><bean:write
															name="baidang" property="tenGiaSu" /></strong></td>
											</tr>
											<tr>
												<td>Hiện đang là: <strong><bean:write
															name="baidang" property="ngheNghiep" /></strong></td>
											</tr>
											<tr>
												<td>Nơi công tác: <strong><bean:write
															name="baidang" property="noiCongTac" /></strong></td>
											</tr>
											<tr>
												<td>Giới tính: <strong><bean:write
															name="baidang" property="gioiTinh" /></strong></td>
											</tr>
											<tr>
												<td>Ngày sinh: <strong><bean:write
															name="baidang" property="ngaySinh" /></strong></td>
											</tr>
											<tr>
												<td>Quê quán: <strong><bean:write
															name="baidang" property="queQuan" /></strong></td>
											</tr>
											<tr>
												<td>Thời gian rảnh: <strong><bean:write
															name="baidang" property="thoiGianDay" /></strong></td>
											</tr>
											<tr>
												<td>Số điện thoại: 
															<bean:define id="sdt" name="baidang" property="soDienThoai"></bean:define>
													<strong><div class="phone" onclick="$(this).text('${sdt}');">Bấm để xem</div>		</strong></td>
											</tr>
										</table>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">Thông tin dạy</h4>
									</div>
									<div class="panel-body">
										<table class="table table-default table-information">
											<tr>
												<td>Lớp dạy: <strong><bean:write
															name="baidang" property="lop" /></strong></td>
											</tr>
											<tr>
												<td>Môn dạy: <strong><bean:write
															name="baidang" property="mon" /></strong></td>
											</tr>
											<tr>
												<td>Khu vực dạy: <strong> <bean:write name="ndChiTietBaiDangForm" property="khuVuc"/>
												</strong></td>
											</tr>
											
											<tr>
												<td>Lương: <strong><bean:write
															name="baidang" property="luongGS" /></strong> VNĐ/ buổi</td>
											</tr>
											
											
										</table>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading">Mô tả thêm</div>
									<div class="panel-body">
										<bean:write name="baidang" property="moTaThem" />
									</div>
								</div>
							</div>
							
							<div class="col-md-12 text-right">
								<logic:present name="ndDangNhapForm" property="tenDangNhap">
								
								<logic:equal value="${nd}" name="baidang" property="maNguoiDang">
									<bean:define id="maBD" name="baidang" property="maBD"></bean:define>
									<html:link styleClass="btn btn-default" action="/suaGiaSu?maBD=${maBD}"><span class="glyphicon glyphicon-edit"></span> Sửa</html:link>
								</logic:equal>
							</logic:present>
							<button class="btn btn-default" onclick="window.history.go(-1)">Trở
								về</button>
							</div>
							<div class="col-md-12 share">CHIA SẺ TIN NÀY <br>
								
								<a href="#"><img alt="" src="img/facebook-logo.png">Facebook</a>
								<a href="#"><img alt="" src="img/twitter-logo.png">Twitter</a>
								<a href="#"><img alt="" src="img/googleplus-logo.png">Google</a>
								<logic:notPresent name="ndDangNhapForm" property="tenDangNhap">
								<button type="button" class="btn btn-link btn-sm" data-toggle="modal"
							data-target="#exampleModal" data-whatever="@getbootstrap"><img alt="" src="img/forbidden2.png">Báo cáo sai phạm</button>
								</logic:notPresent>
								<logic:present name="ndDangNhapForm" property="tenDangNhap">
									<bean:define id="nd" name="ndDangNhapForm" property="tenDangNhap" />
									<logic:notEqual value="${nd}" name="baidang" property="maNguoiDang">
										<button type="button" class="btn btn-link btn-sm" data-toggle="modal"
							data-target="#exampleModal" data-whatever="@getbootstrap"><img alt="" src="img/forbidden2.png">Báo cáo sai phạm</button>
									</logic:notEqual>
								</logic:present>
							</div>
							<div class="col-md-12">
								<div class="fb-like" data-share="true" data-show-faces="true" data-layout="standard"></div>
								<div class="g-plus" data-action="share" data-annotation="none" data-href="http://localhost:8080/MockGiaSu/chiTietBaiDangAction.do?maBD=${maBD}"></div>
							</div>
							<div class="col-md-12">
								<div class="fb-comments" data-href="http://localhost:8080/MockGiaSu/chiTietBaiDangAction.do?maBD=${maBD}" data-numposts="5"></div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
			<div class="col-md-3 col-xs-12">
				<div class="thumbnail">
				<h5><strong>Bạn muốn tìm gia sư</strong></h5>
					<logic:iterate id="bd" name="ndChiTietBaiDangForm" property="lienquanList">
							<bean:define id="maBD" name="bd" property="maBD" />
							<p><html:link action="/chiTietBaiDangAction?maBD=${maBD}">[Gia sư] <bean:write name="bd" property="tieuDe"/></html:link></p>
					</logic:iterate>
				</div><br>
				<div class="fb-page" data-href="https://www.facebook.com/DUTpage" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/DUTpage"><a href="https://www.facebook.com/DUTpage">Trường Đại Học Bách Khoa Đà Nẵng</a></blockquote></div></div>
			</div>
		</div>
	</div>
	</div>
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>