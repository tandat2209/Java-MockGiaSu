<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Cập nhật thông tin</title>
  <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/myStyleSheet.css" rel="stylesheet">
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>

	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3 col-xs-12">
				<!-- left panel -->
				<div id="left-panel">
					<div class="list-group">
						<html:link action="/nvThongTinCaNhan" styleClass="list-group-item">Thông tin cá nhân</html:link>
						<html:link action="/dsduyetbaidang" styleClass="list-group-item">Dabg sách bài đăng</html:link>
						<html:link action="/dsNguoiDung" styleClass="list-group-item">Danh sách người dùng</html:link>
						<html:link action="/nvDSThongBao" styleClass="list-group-item">Danh sách thông báo</html:link>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="panel panel-warning">
					<html:form action="/nvCapNhatThongTin">
						<div class="panel-heading">
							<h4 class="panel-title text-center">Thông tin cá nhân</h4>
						</div>
						<div class="panel-body">
							<div class="col-md-4">
								<img src="img/avatar.png" alt="avatar"
									class="img-circle img-responsive">
							</div>
							<div class="col-md-8">

								<table class="table table-default table-information">
									<tbody>
										<tr>
											<td><label>Tên đăng nhập</label></td>
											<td><html:text property="tenDangNhap" readonly="true" styleClass="form-control"/></td>
										</tr>
										<tr>
											<td>Mật khẩu</td>
											<td>*********</td>
										</tr>
										<tr>
											<td>Họ và tên</td>
											<td><html:text property="tenNV" styleClass="form-control" /></td>
										</tr>
										<tr>
											<td>Ngày sinh</td>
											<td><html:text property="ngaySinh" styleClass="form-control"/></td>
										</tr>
										<tr>
											<td>Giới tính</td>
											<td>
											<div class="input-group">
												<html:radio property="gioiTinh" value="Nam" styleClass="checkbox-inline"> Nam</html:radio>
												<html:radio property="gioiTinh" value="Nữ" styleClass="checkbox-inline"> Nữ</html:radio> 
												<html:radio property="gioiTinh" value="Khác" styleClass="checkbox-inline"> Khác</html:radio>
											</div>
											</td>
										</tr>
										<tr>
											<td>Địa chỉ</td>
											<td><html:text property="diaChi" styleClass="form-control"/></td>
										</tr>
										<tr>
											<td>Điện thoại</td>
											<td><html:text property="soDienThoai" styleClass="form-control"/></td>
										</tr>
										<tr>
											<td>Email</td>
											<td><html:text property="email" styleClass="form-control"/></td>
										</tr>
									</tbody>
								</table>

							</div>
						</div>
						<div class="panel-footer text-right">
							<html:submit property="submit" value="Xong"
								styleClass="btn btn-warning"></html:submit>
							<button class="btn btn-default" onclick="window.history.go(-1)">Trở về</button>
						</div>
					</html:form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
