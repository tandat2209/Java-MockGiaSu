<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Danh sách nhân viên</title>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="bootstrap-theme.min.css">
<link rel="stylesheet" href="css/myStyleSheet.css">
<script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/myJavaScript.js"></script>
<script type="text/javascript">
	function xacNhanXoa(tenNV) {
		if (confirm("Bạn có chắc chắn muốn xóa nhân viên " + tenNV + "?"))
			return true;
		else
			return false;
	}
</script>
</head>
<body>
	<jsp:include page="headerAdmin.jsp"></jsp:include>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3 col-xs-12">
				<!-- left panel -->
				<div id="left-panel">
					<div class="list-group">
						<html:link action="admin_ThemNhanVienAction"
							styleClass="list-group-item">Thêm nhân viên</html:link>
						<html:link action="admin_dsnhanvien" styleClass="list-group-item">Danh sách nhân viên</html:link>
					</div>
				</div>
			</div>
			<div class="col-md-8 col-xs-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="row">
							<div class="col-md-4 col-sm-4 col-xs-12">
								<h4>Danh sách nhân viên</h4>
							</div>
							<div class="col-md-8 col-sm-8 col-xs-12">
								<html:form action="/admin_TimKiemNhanVienAction"
									styleClass="form-inline text-right">
									<html:text styleClass="form-control" property="searchName" />
									<html:submit styleClass="btn btn-default">Tìm kiếm</html:submit>
								</html:form>
							</div>
						</div>
					</div>
					<div class="panel-body">
						<table class="table table-striped">
							<thead>
								<tr>
									<th>Họ tên</th>
									<th>Ngày sinh</th>
									<th>Giới tính</th>
									<th>Email</th>
									<th>Số điện thoại</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<logic:iterate id="x" name="admin_DSNhanVienForm"
									property="listNV">
									<tr>
										<td><bean:define id="ten" name="x" property="tenDangNhap"></bean:define>
											<html:link
												action="admin_XemNhanVienAction.do?tenDangNhap=${ten}">
												<bean:write name="x" property="tenNV" />
											</html:link></td>
										<td><bean:write name="x" property="ngaySinh" /></td>
										<td><bean:write name="x" property="gioiTinh" /></td>
										<td><bean:write name="x" property="email" /></td>
										<td><bean:write name="x" property="soDienThoai" /></td>
										<td><bean:define id="tenDangNhap" name="x"
												property="tenDangNhap">
											</bean:define> <html:link
												action="/admin_SuaNhanVienAction?tenDangNhap=${tenDangNhap}">
												<span class="glyphicon glyphicon-edit"></span>
											</html:link></td>
										<td><bean:define id="tenNV" property="tenNV" name="x"></bean:define>
											<html:link action="admin_XoaNhanVienAction"
												onclick="return xacNhanXoa('${tenNV}')">
												<span class="glyphicon glyphicon-trash"></span>
											</html:link></td>
									</tr>
								</logic:iterate>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>