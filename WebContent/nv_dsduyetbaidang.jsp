<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Danh sách duyệt bài đăng</title>
  <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/myStyleSheet.css" rel="stylesheet">
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
<script src="js/PaginationJS.js"></script>
<script type="text/javascript">
	$(document).ready(function() {

		$('#showtable').pageMe({
			pagerSelector : '#myPager',
			showPrevNext : true,
			hidePageNumbers : false,
			perPage : 10
		});
	});
</script>
</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>

	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3 col-xs-12">
				<!-- left panel -->
				<div id="left-panel">
					<div class="list-group">
						<html:link action="/nvThongTinCaNhan" styleClass="list-group-item">Thông tin cá nhân</html:link>
						<html:link action="/dsduyetbaidang" styleClass="list-group-item">Danh sách bài đăng</html:link>
						<html:link action="/dsNguoiDung" styleClass="list-group-item">Danh sách người dùng</html:link>
						<html:link action="/nvDSThongBao" styleClass="list-group-item">Danh sách thông báo</html:link>
					</div>
				</div>
			</div>
			<div class="col-md-8 col-xs-12 ">
				<div class="row">
					<div class="col-md-12">
						<h3 class="text-center">Danh sách bài đăng</h3>
						<html:form action="/dsduyetbaidang" styleClass="form-inline form-search">
							<div class="form-group">
								<label>Loại bài đăng :</label>
								<html:select name="nvDSDuyetBaiDangForm" property="loai"
									styleClass="form-control">
									<html:option value="Tất cả">Tất cả</html:option>
									<html:option value="Gia sư">Gia sư</html:option>
									<html:option value="Suất dạy">Suất dạy</html:option>
								</html:select>
							</div>
							<div class="form-group">
								<label>Trạng thái :</label>
								<html:select name="nvDSDuyetBaiDangForm" property="trangThai"
									styleClass="form-control">
									<html:option value="Tất cả">Tất cả</html:option>
									<html:option value="Chờ duyệt">Chờ duyệt</html:option>
									<html:option value="Đã duyệt">Đã duyệt</html:option>
									<html:option value="Từ chối">Bị từ chối</html:option>
									<html:option value="Ẩn">Bị ẩn</html:option>
								</html:select>
							</div>
							<html:submit styleClass="btn btn-default">Tìm kiếm</html:submit>
						</html:form>
						<hr>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table">
								<thead>
									<tr>
										<th>Tiêu đề</th>
										<th>Loại</th>
										<th>Người đăng</th>
										<th>Thời gian đăng</th>
										<th>Trạng thái</th>
									</tr>
								</thead>
								<tbody id="showtable">
									<logic:iterate name="nvDSDuyetBaiDangForm"
										property="baiDangList" id="baidang">
										<tr>
											<bean:define id="maBD" name="baidang" property="maBD" />
											<bean:define id="maNguoiDang" name="baidang"
												property="maNguoiDang" />
											<td><html:link action="/nvChiTietBD?maBD=${maBD}">
													<bean:write name="baidang" property="tieuDe" />
												</html:link></td>
											<td><bean:write name="baidang" property="loaiBD" /></td>
											<td><html:link
													action="/nvChiTietND?tenDangNhap=${maNguoiDang}">
													<bean:write name="baidang" property="maNguoiDang" />
												</html:link></td>
											<td><bean:write name="baidang" property="thoiGianDang" /></td>
											<td><bean:write name="baidang" property="trangThai" /></td>
										</tr>
									</logic:iterate>
								</tbody>
							</table>
						</div>
						<div class="row" style="text-align: center;">
							<div class="col-lg-4"></div>
							<div class="col-lg-4">
								<ul style="margin: 0px" class="pagination pagination-lg pager"
									id="myPager"></ul>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</body>
</html>
