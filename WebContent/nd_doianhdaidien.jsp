<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html>
<html:html lang="en">
<head>
<title>Đổi ảnh đại diện</title>
<meta charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/myStyleSheet.css" rel="stylesheet">
<script src="js/jquery-1.11.3.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript">
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#blah').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

$("#imgInp").change(function(){
	console.log('something');
    readURL(this);
});
</script>
</head>
<body>
	<jsp:include page="headerND.jsp"></jsp:include>
	<div class="container content">
	<div class="row">
	<div class="col-md-4 col-md-offset-4">
	<div class="panel panel-default">
	<div class="panel-body">
	<html:form action="/ndDoiAnhDaiDien" enctype="multipart/form-data">
		<html:hidden property="tenDangNhap"/>
		<div class="form-group">
		
		<span class="btn btn-default btn-file">
		Chọn tệp<html:file property="image" styleId="imgInp" accept="image/gif, image/jpeg, image/png" onchange="readURL(this);"></html:file>
		</span>
		<br>
		<img width="100%" class="img-responsive" id="blah" style="border: 1px solid #eee;" src="img/avatar.png" alt="your image" />
		</div>
		<html:submit property="submit" value="Thay đổi" styleClass="btn btn-default"></html:submit>
	</html:form>
	</div>
	</div>
	</div>
	</div>
	</div>
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html:html>