<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Chi tiết người dùng</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/myStyleSheet.css" rel="stylesheet">
<script src="js/jquery-1.11.3.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3 col-xs-12">
				<!-- left panel -->
				<div id="left-panel">
					<div class="list-group">
						<html:link action="/nvThongTinCaNhan" styleClass="list-group-item">Thông tin cá nhân</html:link>
						<html:link action="/dsduyetbaidang" styleClass="list-group-item">Danh sách bài đăng</html:link>
						<html:link action="/dsNguoiDung" styleClass="list-group-item">Danh sách người dùng</html:link>
						<html:link action="/nvDSThongBao" styleClass="list-group-item">Danh sách thông báo</html:link>
					</div>
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title text-center">Thông tin người dùng</h4>
					</div>
					<div class="panel-body">
						<div class="col-md-4">
							<img src="img/avatar.png" alt="avatar"
								class="img-circle img-responsive">
						</div>
						<div class="col-md-8">
							<bean:define id="nguoidung" name="nvChiTietNguoidung" property="nguoiDung"/>
							<table class="table table-default table-information">
								<tbody>
									<tr>
										<td>Tên đăng nhập:</td>
										<td><bean:write name="nguoidung" property="tenDangNhap" /></td>
									</tr>
									<tr>
										<td>Họ tên:</td>
										<td><bean:write name="nguoidung" property="hoTen" /></td>
									</tr>
									<tr>
										<td>Giới tính:</td>
										<td><bean:write name="nguoidung" property="gioiTinh" /></td>
									</tr>
									<tr>
										<td>Ngày sinh:</td>
										<td><bean:write name="nguoidung" property="ngaySinh" /></td>
									</tr>
									<tr>
										<td>Số điện thoại:</td>
										<td><bean:write name="nguoidung" property="soDienThoai" /></td>
									</tr>
									<tr>
										<td>Email:</td>
										<td><bean:write name="nguoidung" property="email" /></td>
									</tr>
									<tr>
										<td>Số lần cảnh báo:</td>
										<td><bean:write name="nguoidung" property="soLanCanhBao" /></td>
									</tr>
									<tr>
										<td>Số bài đăng:</td>
										<td><bean:write name="nguoidung" property="soBaiDang" /></td>
									</tr>
									<tr>
										<td>Trạng thái:</td>
										<td><bean:write name="nguoidung" property="trangThai" /></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="panel-footer text-right">
						<bean:define id="tenDangNhap" name="nvChiTietNguoidung" property="tenDangNhap"></bean:define>
						<logic:equal value="Bình thường" name="nguoidung" property="trangThai">
							<html:link action="/nvChanND?tenDangNhap=${tenDangNhap}" onclick="return confirm('Chặn người dùng này?')" styleClass="btn btn-danger">Chặn người dùng này</html:link>
						</logic:equal>
						<logic:notEqual value="Bình thường" name="nguoidung" property="trangThai">
							<html:link action="/nvBoChanND?tenDangNhap=${tenDangNhap}" onclick="return confirm('Bỏ chặn người dùng này?')" styleClass="btn btn-warning">Bỏ chặn người dùng này</html:link>
						</logic:notEqual>
						<html:link action="/dsNguoiDung" styleClass="btn btn-default">Trở về</html:link>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>