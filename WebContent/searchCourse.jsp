<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html>
<html:html lang="en">
<head>
<title>Tìm kiếm suất dạy</title>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="bootstrap-theme.min.css">
<link rel="stylesheet" href="css/myStyleSheet.css">
<script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/humanized_time_span.js"></script>
<script type="text/javascript" src="js/myJavaScript.js"></script>
<script src="js/PaginationJS.js"></script>
<script type="text/javascript">
$(document).ready(function() {
$('#showtable').pageMe({
pagerSelector : '#myPager',
showPrevNext : true,
hidePageNumbers : false,
perPage : 5
});
});
</script>
</head>
<body>
	<jsp:include page="headerND.jsp"></jsp:include>
	<div class="container content">
		<div class="thumbnail">
			<div class="row">
				<div class="col-md-2">
					<p>
						<html:link action="/searchTutorAction.do"
							styleClass="btn btn-default center-block btn-type">Tìm gia sư</html:link>
					</p>
					<p>
						<html:link action="/searchCourseAction.do"
							styleClass="btn btn-warning center-block btn-type">Tìm suất dạy</html:link>
					</p>
				</div>
				<div class="col-md-8">
					<!-- main search -->
					<div class="row">
						<!-- search form row -->
						<div class="col-md-12">
							<div class="panel panel-danger">
								<div class="panel-body">
									<!-- panel body -->
									<html:form action="/searchCourseAction">
										<!-- search form -->
										<div class="row">
											<div class="col-md-10">
												<div class="row">
													<div class="col-md-12 form-group">
														<html:text property="input"
															styleClass="form-control search-input"></html:text>
													</div>
												</div>
												<div id="search-indicator" class="btn btn-link btn-xs">Tìm
													kiếm nâng cao</div>
												<div class="row form-group advanced-search">
													<div class="col-md-4">
														<html:select property="area" styleId=""
															styleClass="form-control">
															<html:option value="0">Khu vực</html:option>
															<html:option value="Cẩm Lệ">Cẩm Lệ</html:option>
															<html:option value="Hải Châu">Hải Châu</html:option>
															<html:option value="Liên Chiểu">Liên Chiểu</html:option>
															<html:option value="Ngũ Hành Sơn">Ngũ Hành Sơn</html:option>
															<html:option value="Thanh Khê">Thanh Khê</html:option>
															<html:option value="Sơn Trà">Sơn Trà</html:option>
														</html:select>
													</div>
													<div class="col-md-4">
														<html:select property="numberOfPeriod" styleId=""
															styleClass="form-control">
															<html:option value="0">Số buổi/tuần</html:option>
															<html:option value="1">1</html:option>
															<html:option value="2">2</html:option>
															<html:option value="3">3</html:option>
															<html:option value="4">4</html:option>
															<html:option value="5">5</html:option>
															<html:option value="6">6</html:option>
															<html:option value="7">Cả tuần</html:option>
														</html:select>
													</div>
													<div class="col-md-4">
														<html:select property="numberOfStudent" styleId=""
															styleClass="form-control">
															<html:option value="0">Số lượng học viên</html:option>
															<html:option value="1">1</html:option>
															<html:option value="2">2</html:option>
															<html:option value="3">3</html:option>
															<html:option value="4">4</html:option>
															<html:option value="5">5</html:option>
															<html:option value="6">6</html:option>
															<html:option value="7">7</html:option>
														</html:select>
													</div>
												</div>
												<div class="row form-group advanced-search">
													<div class="col-md-4">
														<label for="">Lương/ tháng:</label>
													</div>
													<div class="col-md-4">
														<html:select property="minOfSalary" styleId=""
															styleClass="form-control">
															<html:option value="0">Lương thấp nhất</html:option>
															<html:option value="100000">100.000</html:option>
															<html:option value="200000">200.000</html:option>
															<html:option value="300000">300.000</html:option>
															<html:option value="400000">400.000</html:option>
															<html:option value="500000">500.000</html:option>
															<html:option value="600000">600.000</html:option>
															<html:option value="700000">700.000</html:option>
															<html:option value="800000">800.000</html:option>
															<html:option value="900000">900.000</html:option>
															<html:option value="1000000">1000.000</html:option>
														</html:select>
													</div>
													<div class="col-md-4">
														<html:select property="maxOfSalary" styleId=""
															styleClass="form-control">
															<html:option value="0">Lương cao nhất</html:option>
															<html:option value="100">100.000</html:option>
															<html:option value="200">200.000</html:option>
															<html:option value="300">300.000</html:option>
															<html:option value="400">400.000</html:option>
															<html:option value="500">500.000</html:option>
															<html:option value="1000">1000.000</html:option>
															<html:option value="1200">1200.000</html:option>
															<html:option value="1400">1400.000</html:option>
															<html:option value="1600">1600.000</html:option>
															<html:option value="1800">1800.000</html:option>
															<html:option value="2000">2000.000</html:option>
														</html:select>
													</div>
												</div>
											</div>
											<div class="col-md-2">
												<html:submit styleClass="btn btn-warning">Tìm kiếm</html:submit>
											</div>
										</div>
									</html:form>
									<!-- end search form -->

								</div>
								<!-- end panel body -->
							</div>
						</div>
					</div>
					<!-- end search form row -->
					<div class="bs-example">
						<div id="showtable">
							<logic:iterate id="x" name="searchCourseForm"
								property="courseList">
								<div class="row">
									<div class="col-md-12">
										<div class="panel panel-success">
											<!-- result panel -->
											<div class="panel-body">
												<div class="row">
													<!-- title row -->
													<div class="col-md-7">
														<bean:define id="y" name="x" property="maBD"></bean:define>
														<h4>
															<html:link action="/chiTietBaiDangAction?maBD=${y}">
																<div class="indent-title">Suất dạy</div> <bean:write name="x" property="tieuDe" />
															</html:link>
														</h4>
													</div>
													<div class="col-md-5">
														<h6>Đăng lúc:
														<span class="date"><bean:write name="x" property="thoiGianDang"/>
														</span>
														</h6>
													</div>
												</div>
												<!-- end title row -->
												<div class="row">
													<!--  content row -->
													<div class="col-md-6">
														Số buổi/ tuần:
														<bean:write name="x" property="soBuoiTrenTuan" />
													</div>
													<div class="col-md-6">
														Số lượng học sinh:
														<bean:write name="x" property="soLuongHocSinh" />
													</div>
													<div class="col-md-6">
														Khu vực:
														<bean:write name="x" property="khuVuc" />
													</div>
													<div class="col-md-6">
														Lương:
														<bean:write name="x" property="luongSD" />
														VND/ tháng
													</div>

												</div>
												<!-- end content row -->
											</div>
										</div>
										<!-- end result panel -->
									</div>
								</div>
							</logic:iterate>
						</div>
						<div class="row" style="text-align: center;">
							<div class="col-md-4 col-md-offset-4">
								<ul style="margin: 0px" class="pagination pagination-sm"
									id="myPager"></ul>
							</div>
						</div>
					</div>
				</div>
				<!-- end main-search -->
				<div class="col-md-2">
					<div class="thumbnail">
						<img src="img/dat-quang-cao.gif" alt="" style="border-radius: 3px"><br>
						<img src="img/FPT1.gif" alt="" style="border-radius: 3px">
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="footer.jsp"></jsp:include><br>
</body>
</html:html>