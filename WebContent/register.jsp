<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Đăng ký</title>
</head>
<body>
	<html:form action="/registerAction">
		Tên Đăng Nhập:<html:text property="account"></html:text>
		<html:errors property="nullAccountError" />
		<html:errors property="emptyAccountError" />
		<bean:write name="registerForm" property="error" />
		<br />
		Mật Khẩu:<html:password property="password"></html:password>
		<html:errors property="nullPasswordError" />
		<html:errors property="emptyPasswordError" />
		<br />
		Nhập Lại:<html:password property="passwordConfirm"></html:password>
		<html:errors property="errorMatch" />
		<br />
		Họ Tên:<html:text property="username" ></html:text>
		<html:errors property="nullUsernameError" />
		<html:errors property="emptyUsernameError" />
		<br />
		Giới Tính:
			Nam: <html:radio property="gender" value="Nam" />
			Nữ: <html:radio property="gender" value="Nữ" />
			Không Xác Định: <html:radio property="gender" value="Không Xác Định" />
			<html:errors property="nullGenderError" />
		<br />
		Ngày Sinh:<html:text property="birthday"></html:text>
		<html:errors property="errorBirthday" />
		<br />
		Email:<html:text property="email"></html:text>
		<html:errors property="errorEmail" />
		<br />
		Số Điện Thoại:<html:text property="phone"></html:text>
		<html:errors property="errorPhone" />
		<br />
		<html:submit property="submit" value="Đăng ký"></html:submit>
	</html:form>
</body>
</html:html>