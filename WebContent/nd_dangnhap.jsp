<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<!DOCTYPE html>
<html>
<head>
<title>Đăng nhập</title>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="bootstrap-theme.min.css">
<link rel="stylesheet" href="css/myStyleSheet.css">
<script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/myJavaScript.js"></script>
</head>
<body>
<jsp:include page="headerND.jsp"></jsp:include>
<div class="container content">
		
		<div class="row">

			<div class="col-md-4 col-md-offset-4">
				<div class="panel panel-warning">
					<div class="panel-heading">
						<h4 class="text-center">Đăng nhập</h4>
					</div>
					<div class="panel-body">
					<html:form action="ndDangNhap">
						<br>
					  <div class="form-group">
					    <label>Tên đăng nhập</label>
					    <html:text property="tenDangNhap" styleClass="form-control" />
						<html:errors property="tenDangNhapError" />
					  </div>
					  <div class="form-group">
					    <label>Mật khẩu</label>
					    <html:password property="passWord" styleClass="form-control" />
						<html:errors property="matKhauError" />
					  </div>
					  <hr>
					  <p><bean:write name="ndDangNhapForm" property="errorMS" /></p>
					  <html:submit styleClass="btn btn-warning" property="submit" value="Đăng nhập" />
					  <html:link action="/searchCourseAction" styleClass="btn btn-default">Hủy</html:link>
					</html:form>
					</div>
					<div class="panel-footer">
						Chưa có tài khoản? <html:link action="/dangKyAction">Đăng ký</html:link>
						<h5><html:link action="/ndQuenMatKhau">Quên mật khẩu</html:link> </h5>
						<h6><html:link action="/nvDangNhap">Đăng nhập cho nhân viên</html:link> </h6>
					</div>
				</div>
			</div>
			</div>
		</div>

<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>