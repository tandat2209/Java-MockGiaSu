<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Display Page</title>
	</head>
	<body>
		<table border="1" align="center">
			<tr>
				<th><c:out value="TenDangNhap"/></th>
				<th><c:out value="HoTen"/></th>
				<th><c:out value="GioiTinh"/></th>
				<th><c:out value="NgaySinh"/></th>
				<th><c:out value="SoDienThoai"/></th>
				<th><c:out value="Email"/></th>
				<th><c:out value="SoLanCamOn"/></th>
				<th><c:out value="SoLanCanhBao"/></th>
				<th><c:out value="SoBaiDang"/></th>
				<th><c:out value="TrangThai"/></th>
			</tr>
			<logic:iterate id="x" name="displayForm" property="userList">
			<tr>
				<td><bean:write name="x" property="tenDangNhap"/></td>
				<td><bean:write name="x" property="hoTen"/></td>
				<td><bean:write name="x" property="gioiTinh"/></td>
				<td><bean:write name="x" property="ngaySinh"/></td>
				<td><bean:write name="x" property="soDienThoai"/></td>
				<td><bean:write name="x" property="email"/></td>
				<td><bean:write name="x" property="soLanCamOn"/></td>
				<td><bean:write name="x" property="soLanCanhBao"/></td>
				<td><bean:write name="x" property="soBaiDang"/></td>
				<td><bean:write name="x" property="trangThai"/></td>
			</tr>
			</logic:iterate>	
		</table>
	</body>
</html:html>