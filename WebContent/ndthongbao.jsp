<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Danh sách thông báo</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/myStyleSheet.css" rel="stylesheet">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</head>
<body>
	<jsp:include page="headerND.jsp"></jsp:include>
	<div class="container content">
	<div class="thumbnail">
		<div class="row">
			<div class="col-md-3 col-xs-12">
		
					<p>
						<html:link styleClass="btn btn-default center-block btn-type"
							action="/dangBaiGiaSu">Đăng bài gia sư</html:link>
					</p>
					<p>
						<html:link styleClass="btn btn-default center-block btn-type"
							action="/dangBaiSuatDay">Đăng bài suất dạy</html:link>
					</p>
					<hr>
					<p>
						<html:link styleClass="btn btn-default center-block btn-type"
							action="/ndThongtinCaNhan">Thông tin cá nhân</html:link>
					</p>
					<p>
						<html:link styleClass="btn btn-default center-block btn-type"
							action="/lichSuDangBai">Lịch sử đăng bài</html:link>
					</p>
					<p>
						<html:link styleClass="btn btn-warning center-block btn-type"
							action="/ndThongBao">Danh sách thông báo</html:link>
					</p>
	
			</div>
			<div class="col-md-7 col-xs-12">
				<div class="panel panel-danger">
				<h4 class="text-center">Danh sách thông báo</h4><hr>
				<table class="table table-information">
						<logic:iterate id="thongbao" name="ndThongbaoForm"
							property="listThongBaoND">
							<tr>
							<bean:define id="maBD" name="thongbao" property="maBaiDang"></bean:define>
							<td>
								<div class="text-danger text-center">
									Bài đăng
									<html:link action="/ndChiTietBaiDang?maBD=${maBD}">
										<bean:write name="thongbao" property="tieuDe" />
									</html:link>
									đã ẩn vì bị tố cáo sai phạm
									</div>
							</td>
							</tr>
						</logic:iterate>
				</table>
				</div>
			</div>
			<div class="col-md-2 col-xs-12">
				<div class="thumbnail">
					<img src="img/dat-quang-cao.gif" alt="" style="border-radius: 3px"><br>
					<img src="img/FPT1.gif" alt="" style="border-radius: 3px">
				</div>
			</div>
		</div>
		</div>
	</div>
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>