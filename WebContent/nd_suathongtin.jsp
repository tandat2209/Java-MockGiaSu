<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Thông tin cá nhân</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/myStyleSheet.css" rel="stylesheet">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</head>
<body>
	<jsp:include page="headerND.jsp"></jsp:include>
	<div class="container content">
		<div class="thumbnail">
			<div class="row">
				<div class="col-md-3 col-xs-12">
						<p><html:link styleClass="btn btn-default center-block btn-type" action="/dangBaiGiaSu">Đăng bài gia sư</html:link></p>
		         <p><html:link styleClass="btn btn-default center-block btn-type" action="/dangBaiSuatDay">Đăng bài suất dạy</html:link></p>
		   		<hr>	
		         <p><html:link styleClass="btn btn-warning center-block btn-type" action="/ndThongtinCaNhan">Thông tin cá nhân</html:link></p>
		         <p><html:link styleClass="btn btn-default center-block btn-type" action="/lichSuDangBai">Lịch sử đăng bài</html:link></p>
		         <p><html:link styleClass="btn btn-default center-block btn-type" action="/ndThongBao">Danh sách thông báo</html:link></p>
				</div>
				<div class="col-md-7">
					<html:form action="/ndSuaThongTin">
						<div class="panel panel-warning">
							<div class="panel-heading">
								<h4 class="panel-title text-center">Thông tin cá nhân</h4>
							</div>
							<div class="panel-body">
								<div class="col-md-4">
								<logic:empty name="ndThongTinCaNhanForm" property="anhDaiDien">
									<img src="img/avatar.png" alt="avatar"
										class="img-circle img-responsive">
								</logic:empty>
								<logic:present name="ndThongTinCaNhanForm" property="anhDaiDien">
									<bean:define id="path" name="ndThongTinCaNhanForm" property="anhDaiDien"></bean:define>
										<img src="${path}" alt="avatar"
										class="img-responsive">
								</logic:present>
								
							</div>
								<div class="col-md-8">

									<table class="table table-default table-information">
										<tbody>
											<tr>
												<td>Tên đăng nhập:</td>
												<td><html:text styleClass="form-control"
														readonly="true" property="tenDangNhap" /></td>

											</tr>
											<tr>
												<td>Họ và tên:</td>
												<td><html:text styleClass="form-control"
														property="tenND" />
													<p style="color: red;">
														<html:errors property="errHoTen" />
													</p></td>
											</tr>
											<tr>
												<td>Ngày sinh:</td>
												<td><html:text styleClass="form-control"
														property="ngaySinh" />
													<p style="color: red;">
														<html:errors property="errNgaySinh" />
													</p></td>
											</tr>
											<tr>
												<td>Giới tính:</td>
												<td>
													<div class="input-group">
														<html:radio property="gioiTinh" value="Nam"
															styleClass="checkbox-inline"> Nam</html:radio>
														<html:radio property="gioiTinh" value="Nữ"
															styleClass="checkbox-inline">Nữ</html:radio>
														<html:radio property="gioiTinh" value="Khác"
															styleClass="checkbox-inline">Khác</html:radio>
													</div>
												</td>
											</tr>
											<tr>
												<td>Điện thoại:</td>
												<td><html:text styleClass="form-control"
														property="soDienThoai" />
													<p style="color: red;">
														<html:errors property="errPhone" />
													</p></td>
											</tr>
											<tr>
												<td>Email:</td>
												<td><html:text styleClass="form-control"
														property="email" />
													<p style="color: red;">
														<html:errors property="errEmail" />
													</p></td>
											</tr>

										</tbody>
									</table>

								</div>
							</div>

							<div class="panel-footer text-right">
								<html:submit property="submit" styleClass="btn btn-warning"
									value="Xong"> Xong</html:submit>
								<html:link styleClass="btn btn-default" action="/ndThongtinCaNhan">Trở
									về</html:link >

							</div>

						</div>
					</html:form>
				</div>
				<div class="col-md-2 col-xs-12">
					<div class="thumbnail">
						<img src="img/dat-quang-cao.gif" alt="" style="border-radius: 3px"><br>
						<img src="img/FPT1.gif" alt="" style="border-radius: 3px">
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="footer.jsp"></jsp:include>

</body>
</html>