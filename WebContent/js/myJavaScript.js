$(document).ready(function(){
		$('.date').each(function(){
			$(this).text(humanized_time_span($(this).text()));
		})
		$('#search-indicator').click(function(){
			$('.advanced-search').slideToggle();
		});
		
		$('.search-input').attr('placeholder', 'Tìm kiếm lớp, môn học');
		$('.tieudeInput').attr('title', "Tiêu đề nên bao gồm lớp và môn! vd: Toán 12");
		$('.thoigiandayInput').attr("title", "vd: T2-T4-T6 7pm-9pm, Cả tuần 5am-8am, Luôn rảnh ...");
		$('.tieudeInput').tooltip();
		$('.thoigiandayInput').tooltip();
		
	});

