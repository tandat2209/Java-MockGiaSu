<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<!DOCTYPE html>
<html>
<head>
<title>Đăng nhập</title>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="bootstrap-theme.min.css">
<link rel="stylesheet" href="css/myStyleSheet.css">
<script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/myJavaScript.js"></script>
</head>
<body>
<jsp:include page="headerND.jsp"></jsp:include>
<div class="container content">
	<div class="row">
	<div class="col-md-4 col-md-offset-4">
		<h1 style="font-size: 100px; font-weight: sans-serif;">Oops!</h1>
		<p> Đã có một số lỗi xảy ra! <html:link action="searchCourseAction">Về trang chủ</html:link></p>
	</div>
	</div>
</div>
<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>