<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Chi tiết bài đăng</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/myStyleSheet.css" rel="stylesheet">
<script src="js/jquery-1.11.3.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</head>
<body>
	<jsp:include page="headerND.jsp"></jsp:include>
	<div class="container content">
	<div class="thumbnail">
		<div class="row">
			<div class="col-md-3 col-xs-12">
		        	<p><html:link styleClass="btn btn-default center-block btn-type"
							action="/dangBaiGiaSu">Đăng bài gia sư</html:link></p>
					<p><html:link styleClass="btn btn-default center-block btn-type"
							action="/dangBaiSuatDay">Đăng bài suất dạy</html:link></p>
					<hr>
					<p><html:link styleClass="btn btn-default center-block btn-type"
							action="/ndThongtinCaNhan">Thông tin cá nhân</html:link></p>
					<p><html:link styleClass="btn btn-warning center-block btn-type"
							action="/lichSuDangBai">Lịch sử đăng bài</html:link></p>
					<p><html:link styleClass="btn btn-default center-block btn-type"
							action="/ndThongBao">Danh sách thông báo</html:link></p>
		       
			</div>
			<bean:define id="baidang" name="ndChiTietBaiDangForm" property="baiDang" />
			<div class="col-md-7 col-xs-12">
				<div class="panel panel-warning">
					<div class="panel-heading">
						<div class="row">
							<div class="col-md-6 col-xs-12 title-left">
								<bean:write name="baidang" property="tieuDe" />
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="row">
								</div>
								<div class="row">
									<div class="title-right">
										Vào lúc:
										<bean:write name="baidang" property="thoiGianDang" />
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">Thông tin gia sư</h4>
									</div>
									<div class="panel-body">
										<table class="table table-default table-information">
											<tr>
												<td>Họ tên gia sư: <strong><bean:write
															name="baidang" property="tenGiaSu" /></strong></td>
											</tr>
											<tr>
												<td>Hiện đang là: <strong><bean:write
															name="baidang" property="ngheNghiep" /></strong></td>
											</tr>
											<tr>
												<td>Nơi công tác: <strong><bean:write
															name="baidang" property="noiCongTac" /></strong></td>
											</tr>
											<tr>
												<td>Giới tính: <strong><bean:write
															name="baidang" property="gioiTinh" /></strong></td>
											</tr>
											<tr>
												<td>Ngày sinh: <strong><bean:write
															name="baidang" property="ngaySinh" /></strong></td>
											</tr>
											<tr>
												<td>Quê quán: <strong><bean:write
															name="baidang" property="queQuan" /></strong></td>
											</tr>
											<tr>
												<td>Thời gian rảnh: <strong><bean:write
															name="baidang" property="thoiGianDay" /></strong></td>
											</tr>
											<tr>
												<td>Số điện thoại: <strong><bean:write
															name="baidang" property="soDienThoai" /></strong></td>
											</tr>
									
										</table>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">Thông tin dạy</h4>
									</div>
									<div class="panel-body">
										<table class="table table-default table-information">
											<tr>
												<td>Lớp dạy: <strong><bean:write
															name="baidang" property="lop" /></strong></td>
											</tr>
											<tr>
												<td>Môn dạy: <strong><bean:write
															name="baidang" property="mon" /></strong></td>
											</tr>
											<tr>
												<td>Khu vực dạy: <strong> <bean:write name="ndChiTietBaiDangForm" property="khuVuc"/>
												</strong></td>
											</tr>
											<tr>
												<td>Lương: <strong><bean:write
															name="baidang" property="luongGS" /></strong> VNĐ/ buổi</td>
											</tr>
											
							
										</table>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading">Mô tả thêm</div>
									<div class="panel-body">
										<bean:write name="baidang" property="moTaThem" />
									</div>
								</div>
							</div>
						</div>
						
						<div class="panel-footer text-right">
						<bean:define id="maBD" name="baidang" property="maBD"></bean:define>
							<html:link styleClass="btn btn-default" action="/suaGiaSu?maBD=${maBD}"><span class="glyphicon glyphicon-edit"></span> Sửa</html:link>
							<html:link styleClass="btn btn-default" action="/lichSuDangBai">Trở về</html:link>

						</div>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-xs-12">
				<div class="thumbnail">
				<h5><strong>Có thể bạn muốn tìm?</strong></h5>
					<logic:iterate id="bd" name="ndChiTietBaiDangForm" property="lienquanList">
							<bean:define id="maBD" name="bd" property="maBD" />
							<p><html:link action="/chiTietBaiDangAction?maBD=${maBD}"># <bean:write name="bd" property="tieuDe"/></html:link></p>
					</logic:iterate>
				</div>
			</div>
		</div>
		</div>
	</div>
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>