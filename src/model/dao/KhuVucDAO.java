/**
 * 
 */
package model.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import model.bean.KhuVuc;

/**
 * KhuVucDAO.java
 * 
 * Version 1.0
 * 
 * Date: Aug 16, 2015
 * 
 * Copyright
 * 
 * Modification Logs: DATE AUTHOR DESCRIPTION
 * -----------------------------------------------------------------------
 * 8:28:13 PM HuyNV Create
 */
public class KhuVucDAO {
	/**
	 * @param maBD
	 * @return
	 */
	public static String getKhuVuc(String maBD) {
		String query = String.format(
				"select b.tenKV from dbo.BAIDANG_KHUVUC a inner join KHUVUC b "
						+ "on a.maKhuVuc=b.maKV	where a.maBaiDang='%s'", maBD);
		String khuVuc = "";
		DatabaseDAO db = new DatabaseDAO();
		try {
			Statement st = db.getConnect().createStatement();
			ResultSet rs = st.executeQuery(query);
			while (rs.next()) {
				khuVuc += ", " + rs.getString("tenKV");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			db.closedConnect();
		}
		return khuVuc;
	}

	// lấy mã khu vực cho suất dạy
	public String getKhuVucSD(String maBD) {
		String query = String.format(
				"select maKhuVuc from BAIDANG_KHUVUC where maBaiDang='%s'",
				maBD);
		String khuVuc = "";
		System.out.println(query);
		DatabaseDAO db = new DatabaseDAO();
		try {
			Statement st = db.getConnect().createStatement();
			ResultSet rs = st.executeQuery(query);
			while (rs.next()) {
				khuVuc += rs.getString("maKhuVuc");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			db.closedConnect();
		}
		return khuVuc;
	}

	/**
	 * @param maBD
	 * @param khuVuc
	 */
	public void updateSuatDayKV(String maBD, String khuVuc) {
		// TODO Auto-generated method stub
		String query = String.format(
				"update BAIDANG_KHUVUC set maKhuVuc='%s' where maBaiDang='%s'",
				khuVuc, maBD);
		System.out.println(query);
		DatabaseDAO db = new DatabaseDAO();
		try {
			Connection con = db.getConnect();
			Statement st = con.createStatement();
			st.executeQuery(query);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			db.closedConnect();
		}

	}

	/**
	 * 
	 * @param @param maBD
	 * @param @return
	 * @return ArrayList<KhuVuc>
	 * @throws
	 */

	public ArrayList<KhuVuc> getKVList() {
		String query = String.format("select * from KHUVUC");
		System.out.println(query);
		DatabaseDAO db = new DatabaseDAO();
		ArrayList<KhuVuc> list = new ArrayList<KhuVuc>();
		KhuVuc kv;
		try {
			Connection con = db.getConnect();
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(query);
			while(rs.next()){
				kv = new KhuVuc(rs.getString("maKV"), rs.getString("tenKV"));
				list.add(kv);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			db.closedConnect();
		}
		return list;
	}

	/**	
	 * 	
	 * @param @param maBD
	 * @param @return
	 * @return ArrayList<String>
	 * @throws 	
	 */	
	
	public ArrayList<String> maLvList(String maBD) {
		String query = String.format(
				"select maKhuVuc from BAIDANG_KHUVUC where maBaiDang='%s'",
				maBD);
		ArrayList<String> list = new ArrayList<String>();
		System.out.println(query);
		DatabaseDAO db = new DatabaseDAO();
		String maKV;
		try {
			Statement st = db.getConnect().createStatement();
			ResultSet rs = st.executeQuery(query);
			while (rs.next()) {
				maKV = rs.getString("maKhuVuc");
				list.add(maKV);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			db.closedConnect();
		}
		return list;
	}

	/**
	 * @param maBD 	
	 * 	
	 * @param @param kvListBD
	 * @return void
	 * @throws 	
	 */	
	
	public void updateKvBaiDang(ArrayList<String> kvListBD, String maBD) {
		DatabaseDAO db = new DatabaseDAO();
		Connection con = db.getConnect();
		try {
			Statement st = con.createStatement();
			String queryDelete="delete BAIDANG_KHUVUC where maBaiDang='"+maBD+"'";
			System.out.println(queryDelete);
			st.executeUpdate(queryDelete);
			for (int i=0;i<kvListBD.size();i++){
				String queryInsert = String.format("insert into BAIDANG_KHUVUC values ('%s',%s)",maBD,kvListBD.get(i));
				System.out.println(queryInsert);
				st.executeUpdate(queryInsert);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			db.closedConnect();
		}
	}
	
	
	 /**	
	 * DatNVT
	 * get kv list trong bang KV_BD	
	 * @param @param maBD
	 * @param @return
	 * @return ArrayList<KhuVuc>
	 * @throws 	
	 */	
	public ArrayList<KhuVuc> getKhuVucListByMaBD(String maBD) {
	        String query = String.format("SELECT tenKV from KHUVUC kv join BAIDANG_KHUVUC bdkv"
	                + " on kv.maKV = bdkv.maKhuVuc where maBaiDang = '%s'", maBD);
	        DatabaseDAO db = new DatabaseDAO();
	        ArrayList<KhuVuc> khuVucList = new ArrayList<KhuVuc>();
	        try {
	            System.out.println(query);
	            Statement st = db.getConnect().createStatement();
	            ResultSet rs = st.executeQuery(query);
	            KhuVuc kv;
	            while(rs.next()){
	                kv = new KhuVuc();
	                kv.setTenKV(rs.getString("tenKV"));
	                khuVucList.add(kv);
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	        } finally {
	            db.closedConnect();
	        }
	        
	        return khuVucList;
	    }
}
