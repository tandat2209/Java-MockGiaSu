package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import model.bean.NhanVien;

public class NhanVienDAO {
	DatabaseDAO db = new DatabaseDAO();

//	public NhanVien getNVByTenDangNhap(String tenDangNhap) {
//		String query = String.format("SELECT tenDangNhap, matKhau, tenNV, gioiTinh, ngaySinh,"
//				+ " diaChi, email, soDienThoai, quyen "
//				+ "from NHANVIEN where tenDangNhap='%s'", tenDangNhap);
//		NhanVien nv = new NhanVien();
//		DatabaseDAO db = new DatabaseDAO();
//
//		try {
//		    System.out.println(query);
//			Statement st = db.getConnect().createStatement();
//			ResultSet rs = st.executeQuery(query);
//			while (rs.next()) {
//			    nv.setTenDangNhap(rs.getString("tenDangNhap"));
//			    nv.setMatKhau(rs.getString("matKhau"));
//				nv.setTenNV(rs.getString("tenNV"));
//				nv.setGioiTinh(rs.getString("gioiTinh"));
//				nv.setNgaySinh(rs.getString("ngaySinh"));
//				nv.setDiaChi(rs.getString("diaChi"));
//				nv.setEmail(rs.getString("email"));
//				nv.setSoDienThoai(rs.getString("soDienThoai"));
//				nv.setQuyen(rs.getString("quyen"));
//			}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		} finally {
//			db.closedConnect();
//		}
//		return nv;
//	}

	/**
	 * @param nhanVien
	 */
	public NhanVien getNV(String tenDangNhap) {
		String query = "SELECT * from NHANVIEN where tenDangNhap='"
				+ tenDangNhap + "'";
		NhanVien nv = new NhanVien();
		DatabaseDAO db = new DatabaseDAO();

		try {
			Statement st = db.getConnect().createStatement();
			ResultSet rs = st.executeQuery(query);
			if (rs.next()) {
				nv.setTenNV(rs.getString("tenNV"));
				nv.setGioiTinh(rs.getString("gioiTinh"));

				 //format ngaySinh
				if("0".equals(rs.getString("quyen"))){
					Date date = rs.getDate("ngaySinh");
					SimpleDateFormat dt1 = new SimpleDateFormat("dd-MM-yyyy");
					nv.setNgaySinh(dt1.format(date));
				}else {
					nv.setNgaySinh(rs.getString("ngaySinh"));
				}
				nv.setDiaChi(rs.getString("diaChi"));
				nv.setEmail(rs.getString("email"));
				nv.setSoDienThoai(rs.getString("soDienThoai"));
				nv.setTenDangNhap(rs.getString("tenDangNhap"));
				nv.setMatKhau(rs.getString("matKhau"));
				nv.setQuyen(rs.getString("quyen"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			db.closedConnect();
		}
		return nv;
	}
	
	public void capNhatThongTinNV(NhanVien nhanVien) {
		String query = "UPDATE NHANVIEN SET tenNV=?, gioiTinh=?, ngaySinh=convert(date,?,103), diaChi=?, "
				+ "email=?, soDienThoai=? where tenDangNhap=?";
		DatabaseDAO db = new DatabaseDAO();
		try {
			PreparedStatement st = db.getConnect().prepareStatement(query);
			st.setString(1, nhanVien.getTenNV());
			st.setString(2, nhanVien.getGioiTinh());
			st.setString(3, nhanVien.getNgaySinh());
			st.setString(4, nhanVien.getDiaChi());
			st.setString(5, nhanVien.getEmail());
			st.setString(6, nhanVien.getSoDienThoai());
			st.setString(7, nhanVien.getTenDangNhap());
			st.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			db.closedConnect();
		}
		
	}
	public ArrayList<NhanVien> getAll() {

		String query = "SELECT tenNV, gioiTinh, ngaySinh,"
				+ " diaChi, email, soDienThoai, tenDangNhap, matKhau,"
				+ " quyen from NHANVIEN where tenDangNhap != 'admin'";
		System.out.println(query);
		DatabaseDAO db = new DatabaseDAO();

		ArrayList<NhanVien> listNV = new ArrayList<NhanVien>();
		NhanVien nv = null;
		try {
			Statement st = db.getConnect().createStatement();
			ResultSet rs = st.executeQuery(query);
			while (rs.next()) {
				nv = new NhanVien();
				nv.setTenNV(rs.getString("tenNV"));
				nv.setGioiTinh(rs.getString("gioiTinh"));

				// format ngaySinh
				Date date = rs.getDate("ngaySinh");
				SimpleDateFormat dt1 = new SimpleDateFormat("dd-MM-yyyy");
				nv.setNgaySinh(dt1.format(date));

				nv.setDiaChi(rs.getString("diaChi"));
				nv.setEmail(rs.getString("email"));
				nv.setSoDienThoai(rs.getString("soDienThoai"));
				nv.setTenDangNhap(rs.getString("tenDangNhap"));
				nv.setMatKhau(rs.getString("matKhau"));
				nv.setQuyen(rs.getString("quyen"));
				listNV.add(nv);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			db.closedConnect();
		}
		return listNV;
	}

	public ArrayList<NhanVien> getBySearchName(String name) {
		String sql = "SELECT tenNV, gioiTinh, ngaySinh, diaChi, email, soDienThoai, tenDangNhap, matKhau, quyen from NHANVIEN where tenDangNhap like ? or tenNV like ?";

		StringBuffer query = new StringBuffer();
		String[] arr = name.split(" ");
		for (int i = 0; i < arr.length; i++) {
			query.append("%");
			query.append(arr[i]);
		}
		query.append("%");

		DatabaseDAO db = new DatabaseDAO();

		ArrayList<NhanVien> listNV = new ArrayList<NhanVien>();
		NhanVien nv = null;
		try {
			PreparedStatement st = db.getConnect().prepareStatement(sql);
			st.setString(1, query.toString());
			st.setString(2, query.toString());
			ResultSet rs = st.executeQuery();
			while (rs.next()) {
				nv = new NhanVien();
				nv.setTenNV(rs.getString("tenNV"));
				nv.setGioiTinh(rs.getString("gioiTinh"));

				// format ngaySinh
				Date date = rs.getDate("ngaySinh");
				SimpleDateFormat dt1 = new SimpleDateFormat("dd-MM-yyyy");
				nv.setNgaySinh(dt1.format(date));

				nv.setDiaChi(rs.getString("diaChi"));
				nv.setEmail(rs.getString("email"));
				nv.setSoDienThoai(rs.getString("soDienThoai"));
				nv.setTenDangNhap(rs.getString("tenDangNhap"));
				nv.setMatKhau(rs.getString("matKhau"));
				nv.setQuyen(rs.getString("quyen"));
				listNV.add(nv);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			db.closedConnect();
		}
		return listNV;
	}

	public boolean updateNhanVien(NhanVien nv) {
		String sql = "update  NHANVIEN set matKhau = ?, tenNV = ?,gioiTinh = ?,ngaySinh = Convert(date,?,103),diaChi = ?,email = ?,soDienThoai = ? Where tenDangNhap = ?";
		DatabaseDAO bd = new DatabaseDAO();
		try {
			PreparedStatement st = bd.getConnect().prepareStatement(sql);
			st.setString(1, nv.getMatKhau());
			st.setString(2, nv.getTenNV());
			st.setString(3, nv.getGioiTinh());
			st.setString(4, nv.getNgaySinh());
			st.setString(5, nv.getDiaChi());
			st.setString(6, nv.getEmail());
			st.setString(7, nv.getSoDienThoai());
			st.setString(8, nv.getTenDangNhap());
			st.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean xoaNhanVien(String tenDangNhap) {
		String sql = "delete NHANVIEN where tenDangNhap=?";
		DatabaseDAO bd = new DatabaseDAO();
		try {
			PreparedStatement st = bd.getConnect().prepareStatement(sql);
			st.setString(1, tenDangNhap);
			st.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean themNhanVien(NhanVien nv) throws SQLException {

		String sql = "Insert into  NHANVIEN values(?,?,?,?,Convert(date,?,103),?,?,?,0)";
		DatabaseDAO bd = new DatabaseDAO();
		
			PreparedStatement st = bd.getConnect().prepareStatement(sql);
			st.setString(1, nv.getTenDangNhap());
			st.setString(2, nv.getMatKhau());
			st.setString(3, nv.getTenNV());
			st.setString(4, nv.getGioiTinh());
			st.setString(5, nv.getNgaySinh());
			st.setString(6, nv.getDiaChi());
			st.setString(7, nv.getEmail());
			st.setString(8, nv.getSoDienThoai());
			st.executeUpdate();
		
		return true;
	}

	/**	
	 * 	
	 * @param @param tenDangNhap
	 * @param @param pwd1
	 * @return void
	 * @throws 	
	 */	
	
	public void nvDoiMatKhau(String tenDangNhap, String pwd1) {
		// TODO Auto-generated method stub
		DatabaseDAO db = new DatabaseDAO();
		Connection con = db.getConnect();
		try {
			String sql = String.format("update NHANVIEN set matKhau='%s' where tenDangNhap='%s'",pwd1,tenDangNhap);
			System.out.println(sql);
			Statement st = con.createStatement();
			st.executeUpdate(sql);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			db.closedConnect();
		}
	}

	/**
	 * @param tenDangNhap
	 * @return
	 */
	public boolean checkNhanVien(String tenDangNhap) {
		String query = String.format("SELECT * from NHANVIEN where tenDangNhap='%s'",tenDangNhap);
		DatabaseDAO db = new DatabaseDAO();
		try {
		    System.out.println(tenDangNhap);
			Statement st = db.getConnect().createStatement();
			ResultSet rs = st.executeQuery(query);
			if(rs.next())
				return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			db.closedConnect();
		}
		return false;
	}
}
