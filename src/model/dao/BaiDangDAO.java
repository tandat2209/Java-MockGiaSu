/**
 * 
 */
package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import common.SearchProcess;
import common.StringProcess;
import model.bean.BaiDang;


/**				
 * BaiDangDAO.java			
 *				
 * Version 1.0				
 *				
 * Date: Aug 14, 2015			
 *				
 * Copyright 				
 *				
 * Modification Logs:				
 * DATE                 AUTHOR          DESCRIPTION				
 * -----------------------------------------------------------------------				
 * Aug 14, 2015        HuyNV          Create				
 */

public class BaiDangDAO {
	
	//lay list bai dang cua nguoi dung
	public static ArrayList<BaiDang> getBaiDangList(String trangThai) {
		String query = "SELECT maBD, loaiBD, tieuDe, maNguoiDang, thoiGianDang, trangThai "
		        + "from BAIDANG where trangThai like '%"+trangThai+"%' order by thoiGianDang desc";
		        
		ArrayList<BaiDang> baiDangList = new ArrayList<BaiDang>();
		DatabaseDAO db = new DatabaseDAO();
		try {
			Statement st = db.getConnect().createStatement();
			System.out.println(query);
			ResultSet rs = st.executeQuery(query);
			BaiDang bd;
			while (rs.next()) {
				bd = new BaiDang();
				bd.setMaBD(rs.getString("maBD"));
				bd.setLoaiBD(rs.getString("loaiBD"));
				bd.setTieuDe(rs.getString("tieuDe"));
				bd.setMaNguoiDang(rs.getString("maNguoiDang"));
				/* lay ngay gio tu database */
                Timestamp myTimestamp = rs.getTimestamp("thoiGianDang");
                bd.setThoiGianDang(StringProcess.getThoiGianDang(
                		new SimpleDateFormat("HH:mm:ss dd-MM-yyyy").format(myTimestamp)));
				bd.setTrangThai(rs.getString("trangThai"));
				baiDangList.add(bd);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			db.closedConnect();
		}

		return baiDangList;
	}
	
	// get bai dang list cua nhan vien 
	public ArrayList<BaiDang> getBaiDangList() {
		String query = "SELECT maBD, loaiBD, tieuDe, maNguoiDang, thoiGianDang, trangThai "
		        + "from BAIDANG order by thoiGianDang desc";
		        
		ArrayList<BaiDang> baiDangList = new ArrayList<BaiDang>();
		DatabaseDAO db = new DatabaseDAO();
		try {
			Statement st = db.getConnect().createStatement();
			System.out.println(query);
			ResultSet rs = st.executeQuery(query);
			BaiDang bd;
			while (rs.next()) {
				bd = new BaiDang();
				bd.setMaBD(rs.getString("maBD"));
				bd.setLoaiBD(rs.getString("loaiBD"));
				bd.setTieuDe(rs.getString("tieuDe"));
				bd.setMaNguoiDang(rs.getString("maNguoiDang"));
				/* lay ngay gio tu database */
				Timestamp myTimestamp = rs.getTimestamp("thoiGianDang");
				bd.setThoiGianDang(StringProcess.getThoiGianDang(
                		new SimpleDateFormat("HH:mm:ss dd-MM-yyyy").format(myTimestamp)));
				bd.setTrangThai(rs.getString("trangThai"));
				baiDangList.add(bd);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			db.closedConnect();
		}

		return baiDangList;
	}
	/**
	 * @param maBD
	 * @return
	 */
	
	//get bai dang
	public BaiDang getBaiDang(String maBD) {
		  String query = String.format("SELECT maBD, loaiBD, tieuDe, moTaThem, maNguoiDang, thoiGianDang, trangThai, "
	                + "tenGiaSu, gioiTinh, soDienThoai, noiCongTac, ngheNghiep, ngaySinh, queQuan, luongGS, "
	                + "tenPhuHuynh, diaChi, mon, lop, soBuoiTrenTuan, soLuongHocSinh, thoiGianDay,luongSD "
	                + "from BAIDANG where maBD = '%s' ", maBD);
	        BaiDang bd = new BaiDang();
	        DatabaseDAO db = new DatabaseDAO();
	        try {
	        	System.out.println(query);
	            Statement st = db.getConnect().createStatement();
	            ResultSet rs = st.executeQuery(query);
	            if(rs.next()){
	                bd.setMaBD(rs.getString("maBD"));
	                bd.setLoaiBD(rs.getString("loaiBD"));
	                bd.setTieuDe(rs.getString("tieuDe"));
	                bd.setMoTaThem(rs.getString("moTaThem"));
	                bd.setMaNguoiDang(rs.getString("maNguoiDang"));
	                /* lay ngay gio tu database */
					Timestamp myTimestamp = rs.getTimestamp("thoiGianDang");
					bd.setThoiGianDang(new SimpleDateFormat("HH:mm:ss dd-MM-yyyy").format(myTimestamp));
	                bd.setTrangThai(rs.getString("trangThai"));
	                bd.setTenGiaSu(rs.getString("tenGiaSu"));
	                bd.setGioiTinh(rs.getString("gioiTinh"));
	                bd.setSoDienThoai(rs.getString("soDienThoai"));
	                bd.setNoiCongTac(rs.getString("noiCongTac"));
	                bd.setNgheNghiep(rs.getString("ngheNghiep"));
	                
	              //format ngaySinh
					if("Gia sư".equals(rs.getString("loaiBD"))){
						Date date = rs.getDate("ngaySinh");
						SimpleDateFormat dt1 = new SimpleDateFormat("dd/MM/yyyy");
						System.out.println(dt1);
						bd.setNgaySinh(dt1.format(date));
					}else {
						bd.setNgaySinh(rs.getString("ngaySinh"));
					}
					
	                bd.setQueQuan(rs.getString("queQuan"));
	                bd.setLuongGS(""+rs.getInt("luongGS"));
	                bd.setTenPhuHuynh(rs.getString("tenPhuHuynh"));
	                bd.setDiaChi(rs.getString("diaChi"));
	                bd.setMon(rs.getString("mon"));
	                bd.setLop(rs.getString("lop"));
	                bd.setSoBuoiTrenTuan(rs.getString("soBuoiTrenTuan"));
	                bd.setSoLuongHocSinh(rs.getString("soLuongHocSinh"));
	                bd.setThoiGianDay(rs.getString("thoiGianDay"));
	                bd.setLuongSD(""+rs.getInt("luongSD"));
	                
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        } finally{
	            db.closedConnect();
	        }
	        return bd;
	}

	/**
	 * @param maBD
	 * @return
	 */
	public static String getKhuVuc(String maBD) {
		String query = String.format("select b.tenKV from dbo.BAIDANG_KHUVUC a inner join KHUVUC b "
						+ "on a.maKhuVuc=b.maKV	where a.maBaiDang='%s'",maBD);
        String khuVuc="";
        DatabaseDAO db = new DatabaseDAO();
        System.out.println(query);
        try {
            Statement st = db.getConnect().createStatement();
            ResultSet rs = st.executeQuery(query);
            while(rs.next()){
                khuVuc+= ", "+rs.getString("tenKV");
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally{
            db.closedConnect();
        }
        return khuVuc;
	}

	/**
	 * @param suatday
	 * update suất dạy
	 *
	 */
	public static void updateSuatDay(BaiDang suatday) {
		// TODO Auto-generated method stub
		DatabaseDAO db = new DatabaseDAO();
		try {
			String query ="update BAIDANG set tieuDe=?, tenPhuHuynh=?, soDienThoai=?, diaChi=?, lop=?, mon=?, "
						+ "soBuoiTrenTuan=?, soLuongHocSinh=?, thoiGianDay=?, moTaThem=? where maBD=?";
			Connection con = db.getConnect();
			PreparedStatement st = con.prepareStatement(query);
			st.setString(1, suatday.getTieuDe());
			st.setString(2, suatday.getTenPhuHuynh());
			st.setString(3, suatday.getSoDienThoai());
			st.setString(4, suatday.getDiaChi());
			st.setString(5, suatday.getLop());
			st.setString(6, suatday.getMon());
			st.setString(7, suatday.getSoBuoiTrenTuan());
			st.setString(8, suatday.getSoLuongHocSinh());
			st.setString(9, suatday.getThoiGianDay());
			st.setString(10, suatday.getMoTaThem());
			st.setString(11, suatday.getMaBD());
			System.out.println(query);
			System.out.println(suatday.getTieuDe());
			st.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
        } finally{
            db.closedConnect();
        
		}
	}

	/**	
	 * 	
	 * @param @param bd
	 * @return void
	 * @throws 	
	 */	
	public static void updateBaiDang(BaiDang bd) {
		// TODO Auto-generated method stub
		DatabaseDAO db = new DatabaseDAO();
		Connection con = db.getConnect();
		try {
			String query="update BAIDANG set tieuDe=?, mon=? ,lop=? , thoiGianDay=? ,soDienThoai=? , " +
					"moTaThem=? , luongSD=?, tenGiaSu=?, gioiTinh=?," +
					"noiCongTac=?, ngheNghiep=?, ngaySinh=convert(date,?, 103), queQuan=?, luongGS=?, " +
					"tenPhuHuynh=?, diaChi=?, soBuoiTrenTuan=?, soLuongHocSinh=? where maBD=? ";
			PreparedStatement ps = con.prepareStatement(query);
			ps.setString(1, bd.getTieuDe());
			ps.setString(2, bd.getMon());
			ps.setString(3, bd.getLop());
			ps.setString(4, bd.getThoiGianDay());
			ps.setString(5, bd.getSoDienThoai());
			ps.setString(6, bd.getMoTaThem());
			ps.setString(7, bd.getLuongSD());
			ps.setString(8, bd.getTenGiaSu());
			ps.setString(9, bd.getGioiTinh());
			ps.setString(10, bd.getNoiCongTac());
			ps.setString(11, bd.getNgheNghiep());
			ps.setString(12, bd.getNgaySinh());
			ps.setString(13, bd.getQueQuan());
			ps.setString(14, bd.getLuongGS());
			ps.setString(15, bd.getTenPhuHuynh());
			ps.setString(16, bd.getDiaChi());
			ps.setString(17, bd.getSoBuoiTrenTuan());
			ps.setString(18, bd.getSoLuongHocSinh());
			ps.setString(19, bd.getMaBD());
			ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
        } finally{
            db.closedConnect();
        }
	}

	/**	
	 * 	
	 * @param @param bd
	 * @return void
	 * @throws 	
	 */	
	
	public static void insertBaiDang(BaiDang bd) {
		// TODO Auto-generated method stub
		DatabaseDAO db = new DatabaseDAO();
		Connection con = db.getConnect();
		try {
			String query="insert into BAIDANG(tieuDe,mon,lop, thoiGianDay,soDienThoai, " +
					"moTaThem, tenGiaSu, gioiTinh," +
					"noiCongTac, ngheNghiep, ngaySinh, queQuan, luongGS, " +
					"tenPhuHuynh, diaChi, soBuoiTrenTuan, soLuongHocSinh,luongSD,maBD,loaiBD,maNguoiDang) " +
					"values(?,?,?,?,?,?,?,?,?,?,convert(date,?,103),?,?,?,?,?,?,?,?,?,?) ";
			PreparedStatement ps = con.prepareStatement(query);
			ps.setString(1, bd.getTieuDe());
			ps.setString(2, bd.getMon());
			ps.setString(3, bd.getLop());
			ps.setString(4, bd.getThoiGianDay());
			ps.setString(5, bd.getSoDienThoai());
			ps.setString(6, bd.getMoTaThem());
			ps.setString(7, bd.getTenGiaSu());
			ps.setString(8, bd.getGioiTinh());
			ps.setString(9, bd.getNoiCongTac());
			ps.setString(10, bd.getNgheNghiep());
			ps.setString(11, bd.getNgaySinh());
			ps.setString(12, bd.getQueQuan());
			ps.setString(13, bd.getLuongGS());
			ps.setString(14, bd.getTenPhuHuynh());
			ps.setString(15, bd.getDiaChi());
			ps.setString(16, bd.getSoBuoiTrenTuan());
			ps.setString(17, bd.getSoLuongHocSinh());
			ps.setString(18, bd.getLuongSD());
			ps.setString(19, bd.getMaBD());
			ps.setString(20, bd.getLoaiBD());
			ps.setString(21, bd.getMaNguoiDang());
			ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
        } finally{
            db.closedConnect();
        }
	}
	
	/**
	 * @param maBD
	 */
	public void tuchoiBaiDang(String maBD) {
		String query = String.format("UPDATE BAIDANG SET trangThai=N'Từ chối' "
				+ "where maBD = '%s'",maBD);
		DatabaseDAO db = new DatabaseDAO();
		try {
			System.out.println(query);
			Statement st = db.getConnect().createStatement();
			st.executeUpdate(query);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			db.closedConnect();
		}
		
	}

	/**
	 * @param maBD
	 */
	public void duyetBaiDang(String maBD) {
		String query = String.format("UPDATE BAIDANG SET trangThai=N'Đã duyệt' "
				+ "where maBD = '%s'",maBD);
		DatabaseDAO db = new DatabaseDAO();
		try {
			System.out.println(query);
			Statement st = db.getConnect().createStatement();
			st.executeUpdate(query);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			db.closedConnect();
		}
	}

	/**	
	 * 	
	 * @param @param maBD
	 * @return void
	 * @throws 	
	 */	
	
	public void deleteBaiDang(String maBD) {
		// TODO Auto-generated method stub
		DatabaseDAO db = new DatabaseDAO();
		Connection con = db.getConnect();
		try {
			String query = "delete from BAIDANG_KHUVUC where maBaiDang='"+maBD+"'";
			String querytb = "delete from THONGBAO where maBaiDang='"+maBD+"'";
			String querydel = "delete from BAIDANG where maBD='"+maBD+"'";
			System.out.println(query);
			System.out.println(querytb);
			System.out.println(querydel);
			Statement st = con.createStatement();
			st.executeUpdate(query);
			st.executeUpdate(querytb);
			st.executeUpdate(querydel);
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			db.closedConnect();
		}
	}

	/**
	 * @param maNguoiDang
	 * @return
	 */
	public ArrayList<BaiDang> getBaiDangListByMaND(String maNguoiDang) {
		String query = String.format("SELECT maBD, loaiBD, tieuDe, maNguoiDang, thoiGianDang, trangThai "
		        + "from BAIDANG where maNguoiDang='%s' order by thoiGianDang desc", maNguoiDang);
		ArrayList<BaiDang> baiDangList = new ArrayList<BaiDang>();
		DatabaseDAO db = new DatabaseDAO();
		try {
			Statement st = db.getConnect().createStatement();
			ResultSet rs = st.executeQuery(query);
			BaiDang bd;
			while(rs.next()){
				bd = new BaiDang();
				bd.setMaBD(rs.getString("maBD"));
				bd.setLoaiBD(rs.getString("loaiBD"));
				bd.setTieuDe(rs.getString("tieuDe"));
				bd.setMaNguoiDang(rs.getString("maNguoiDang"));
				System.out.println("something");
				/* lay	 ngay gio tu database */
                Timestamp myTimestamp = rs.getTimestamp("thoiGianDang");
                bd.setThoiGianDang(StringProcess.getThoiGianDang(
                		new SimpleDateFormat("HH:mm:ss dd-MM-yyyy").format(myTimestamp)));
				bd.setTrangThai(rs.getString("trangThai"));
				baiDangList.add(bd);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			db.closedConnect();
		}
		return baiDangList;
	}

	/**
	 * @param bd
	 * @return
	 */
    public static ArrayList<BaiDang> getSDLienQuanList(BaiDang bd) {
    	String []mon = SearchProcess.removeAccent(bd.getMon()).trim().split("[\\W]");
    	System.out.println(StringProcess.removeAccent(bd.getLop()));
    	// chuyen co dau -> khong dau, roi cat chuoi -> danh sach cac lop;
    	String []lop = SearchProcess.removeAccent(bd.getLop()).trim().split("[\\W]");
    	
    	StringBuffer timmon = new StringBuffer();
    	if(mon.length>0){
    		timmon.append(" dbo.fRemoveAcent(mon) like '%"+ mon[0]+"%' ");
    		for (int i = 1; i < mon.length; i++) {
    			if("".equals(mon[i].trim()))
    				continue;
	            timmon.append(" or dbo.fRemoveAcent(mon) like '%" + mon[i] + "%' ");
            }
    	}
    	StringBuffer timlop = new StringBuffer();
    	if(lop.length>0){
    		timlop.append(" dbo.fRemoveAcent(lop) like '%"+ lop[0]+"%' ");
    		for (int i = 1; i < lop.length; i++) {
    			if("".equals(lop[i].trim()))
    				continue;
	            timlop.append(" or dbo.fRemoveAcent(lop) like '%" + lop[i] + "%' ");
            }
    	}
//	    StringBuffer query = new StringBuffer("SELECT maBD, tieuDe from BAIDANG where loaiBD=N'Suất dạy' "
//	    		+ "and (("+timmon+") or ("+timlop+")) order by thoiGianDang desc");
    	StringBuffer query1 = new StringBuffer("SELECT maBD, tieuDe from BAIDANG where maBD <> '"+bd.getMaBD()+"' and loaiBD=N'Suất dạy' "
                + "and ("+timmon+") order by thoiGianDang desc");
    	StringBuffer query2 = new StringBuffer("SELECT maBD, tieuDe from BAIDANG where maBD <> '"+bd.getMaBD()+"' and loaiBD=N'Suất dạy' "
                + "and  ("+timlop+") order by thoiGianDang desc");
	    ArrayList<BaiDang> list1 = new ArrayList<BaiDang>();
	    ArrayList<BaiDang> list2 = new ArrayList<BaiDang>();
	    ArrayList<BaiDang> list = new ArrayList<BaiDang>();
	    DatabaseDAO db = new DatabaseDAO();
	    try {
	        //list 1
	        Statement st = db.getConnect().createStatement();
	        ResultSet rs = st.executeQuery(query1.toString());
	        BaiDang bd1;
	        while(rs.next()){
	        	bd1 = new BaiDang();
	        	bd1.setMaBD(rs.getString("maBD"));
	        	bd1.setTieuDe(rs.getString("tieuDe"));
	        	list1.add(bd1);
	        };
	        
	        //list 2
            st = db.getConnect().createStatement();
            rs = st.executeQuery(query2.toString());
            while(rs.next()){
                bd1 = new BaiDang();
                bd1.setMaBD(rs.getString("maBD"));
                bd1.setTieuDe(rs.getString("tieuDe"));
                list2.add(bd1);
            };
	        
        } catch (SQLException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
        } finally{
        	db.closedConnect();
        }
	    
	    for (int i = 0; i < list1.size(); i++) {
            for (int j = 0; j < list2.size(); j++) {
                if(list1.get(i).getMaBD().equalsIgnoreCase(list2.get(j).getMaBD()))
                {
                    list.add(list1.get(i));
                    list2.remove(j);
                }
            }
        }
	    
	    
	    //list 1
	    for (int i = 0; i < list1.size(); i++) {
	        
            if (!list.contains(list1.get(i))) {
                list.add(list1.get(i));
            }
        }

	    
	    //list 2
	    for (int i = 0; i < list2.size(); i++) {
            if (!list.contains(list2.get(i))) {
                list.add(list2.get(i));
            }
        }
	    
	    
	    
	    return list;
    }

	/**
	 * @param bd
	 * @return
	 */
    public static ArrayList<BaiDang> getGSLienQuanList(BaiDang bd) {
    	// chuyen co dau -> khong dau, roi cat chuoi -> danh sach cac mon

    	String []mon = SearchProcess.removeAccent(bd.getMon()).trim().split("[\\W]");
    	System.out.println(StringProcess.removeAccent(bd.getLop()));
    	// chuyen co dau -> khong dau, roi cat chuoi -> danh sach cac lop;
    	String []lop = SearchProcess.removeAccent(bd.getLop()).trim().split("[\\W]");
    	
    	StringBuffer timmon = new StringBuffer();
    	if(mon.length>0){
    		timmon.append(" dbo.fRemoveAcent(mon) like '%"+ mon[0]+"%' ");
    		for (int i = 1; i < mon.length; i++) {
    			if("".equals(mon[i].trim()))
    				continue;
	            timmon.append(" or dbo.fRemoveAcent(mon) like '%" + mon[i] + "%' ");
            }
    	}
    	StringBuffer timlop = new StringBuffer();
    	if(lop.length>0){
    		timlop.append(" dbo.fRemoveAcent(lop) like '%"+ lop[0]+"%' ");
    		for (int i = 1; i < lop.length; i++) {
    			if("".equals(lop[i].trim()))
    				continue;
	            timlop.append(" or dbo.fRemoveAcent(lop) like '%" + lop[i] + "%' ");
            }
    	}
    	
//	    StringBuffer query = new StringBuffer("SELECT maBD, tieuDe from BAIDANG where loaiBD=N'Gia sư' "
//	    		+ "and (("+timmon+") or ("+timlop+")) order by thoiGianDang desc");
	    
	    StringBuffer query1 = new StringBuffer("SELECT maBD, tieuDe from BAIDANG where maBD <> '"+bd.getMaBD()+"' and loaiBD=N'Gia sư' "
                + "and ("+timmon+") order by thoiGianDang desc ");
        StringBuffer query2 = new StringBuffer("SELECT maBD, tieuDe from BAIDANG where maBD <> '"+bd.getMaBD()+"' and loaiBD=N'Gia sư' "
                + "and  ("+timlop+") order by thoiGianDang desc");
        ArrayList<BaiDang> list1 = new ArrayList<BaiDang>();
        ArrayList<BaiDang> list2 = new ArrayList<BaiDang>();
        ArrayList<BaiDang> list = new ArrayList<BaiDang>();
        DatabaseDAO db = new DatabaseDAO();
        try {
            //list 1
            Statement st = db.getConnect().createStatement();
            ResultSet rs = st.executeQuery(query1.toString());
            BaiDang bd1;
            while(rs.next()){
                bd1 = new BaiDang();
                bd1.setMaBD(rs.getString("maBD"));
                bd1.setTieuDe(rs.getString("tieuDe"));
                list1.add(bd1);
            };
            
            //list 2
            st = db.getConnect().createStatement();
            rs = st.executeQuery(query2.toString());
            while(rs.next()){
                bd1 = new BaiDang();
                bd1.setMaBD(rs.getString("maBD"));
                bd1.setTieuDe(rs.getString("tieuDe"));
                list2.add(bd1);
            };
            
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally{
            db.closedConnect();
        }
        
        for (int i = 0; i < list1.size(); i++) {
            for (int j = 0; j < list2.size(); j++) {
                if(list1.get(i).getMaBD().equalsIgnoreCase(list2.get(j).getMaBD()))
                {
                    list.add(list1.get(i));
                    list2.remove(j);
                }
            }
        }
        
        
        //list 1
        for (int i = 0; i < list1.size(); i++) {
            
            if (!list.contains(list1.get(i))) {
                list.add(list1.get(i));
            }
        }

        
        //list 2
        for (int i = 0; i < list2.size(); i++) {
            if (!list.contains(list2.get(i))) {
                list.add(list2.get(i));
            }
        }
	    
//	    ArrayList<BaiDang> list = new ArrayList<BaiDang>();
//	    DatabaseDAO db = new DatabaseDAO();
//	    try {
//	    	System.out.println(query);
//	        Statement st = db.getConnect().createStatement();
//	        ResultSet rs = st.executeQuery(query.toString());
//	        BaiDang bd1;
//	        while(rs.next()){
//	        	bd1 = new BaiDang();
//	        	bd1.setMaBD(rs.getString("maBD"));
//	        	bd1.setTieuDe(rs.getString("tieuDe"));
//	        	list.add(bd1);
//	        };
//	        
//        } catch (SQLException e) {
//	        // TODO Auto-generated catch block
//	        e.printStackTrace();
//        } finally{
//        	db.closedConnect();
//        }
        ArrayList<BaiDang> temp = new ArrayList<BaiDang>();
        for (int i = 0; i < list.size(); i++) {
            temp.add(list.get(i));
            if(i==4) return temp;
        }
	    return temp;
    }
}
