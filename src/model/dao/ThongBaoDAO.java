/**
 * 
 */
package model.dao;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import model.bean.ThongBao;


/**
 * ThongBaoDAO.java
 *
 * Version 1.0
 *
 * Date: Aug 16, 2015
 *
 * Copyright
 *
 * Modification Logs: DATE AUTHOR DESCRIPTION
 * -----------------------------------------------------------------------
 * 10:03:52 AM HuyNV Create
 */
public class ThongBaoDAO {

	/**
	 * @param tenDangNhap
	 * @return
	 */
	public static ArrayList<ThongBao> getThongBaoListND(String tenDangNhap) {
		String query = "select tenDangNhap, tieuDe, maBaiDang, ngayBao from dbo.NGUOIDUNG nd join dbo.BAIDANG bd "
				+ "on nd.tenDangNhap=bd.maNguoiDang join dbo.THONGBAO tb on bd.maBD=tb.maBaiDang "
				+ "where tenDangNhap='" + tenDangNhap + "' and tb.trangThai=N'Đã xử lý' and bd.trangThai=N'Ẩn' "
						+ "order by ngayBao";
		ArrayList<ThongBao> thongBaoList = new ArrayList<ThongBao>();
		DatabaseDAO db = new DatabaseDAO();
		try {
			System.out.println(query);
			Statement st = db.getConnect().createStatement();	
			ResultSet rs = st.executeQuery(query);
			ThongBao tb;
			while (rs.next()) {
				tb = new ThongBao();
				tb.setMaBaiDang(rs.getString("maBaiDang"));
				tb.setTieuDe(rs.getString("tieuDe"));
				tb.setMaNguoiDang(rs.getString("tenDangNhap"));
				thongBaoList.add(tb);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			db.closedConnect();
		}

		return thongBaoList;
	}
	
	public ArrayList<ThongBao> getThongBaoList() {
        String query = "SELECT maTB, maBaiDang, tieuDe, maNguoiDang, ngayBao, THONGBAO.trangThai "
                + "from THONGBAO join BAIDANG on THONGBAO.maBaiDang = BAIDANG.maBD "
                + " order by ngayBao desc ";
        ArrayList<ThongBao> thongBaoList = new ArrayList<ThongBao>();
        DatabaseDAO db = new DatabaseDAO();
        try {
            System.out.println(query);
            Statement st = db.getConnect().createStatement();
            ResultSet rs = st.executeQuery(query);
            ThongBao tb;
            while(rs.next()){
                tb = new ThongBao();
                tb.setMaTB(rs.getString("maTB"));
                tb.setMaBaiDang(rs.getString("maBaiDang"));
                tb.setTieuDe(rs.getString("tieuDe"));
                tb.setMaNguoiDang(rs.getString("maNguoiDang"));
                tb.setNgayBao(rs.getString("ngayBao"));
                tb.setTrangThai(rs.getString("trangThai"));
                thongBaoList.add(tb);
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally{
            db.closedConnect();
        }
        
        return thongBaoList;
    }

    public ThongBao getThongBaoByMaTB(String maTB) {
        String query = String.format("SELECT maTB, maBaiDang, tieuDe, maNguoiDang, ngayBao, email, "
                + "THONGBAO.soDienThoai, noiDung, THONGBAO.trangThai from THONGBAO join BAIDANG "
                + "on THONGBAO.maBaiDang = BAIDANG.maBD "
                + "where maTB = '%s'", maTB);
        ThongBao tb = new ThongBao();
        DatabaseDAO db = new DatabaseDAO();
        try {
            System.out.println(query);
            Statement st = db.getConnect().createStatement();
            ResultSet rs = st.executeQuery(query);
            if(rs.next()){
                tb.setMaTB(rs.getString("maTB"));
                tb.setMaBaiDang(rs.getString("maBaiDang"));
                tb.setTieuDe(rs.getString("tieuDe"));
                tb.setMaNguoiDang(rs.getString("maNguoiDang"));
                tb.setNgayBao(rs.getString("ngayBao"));
                tb.setEmail(rs.getString("email"));
                tb.setSoDienThoai(rs.getString("soDienThoai"));
                tb.setNoiDung(rs.getString("noiDung"));
                tb.setTrangThai(rs.getString("trangThai"));
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally{
            db.closedConnect();
        }
        return tb;
    }

	public void anThongBao(String maTB, String maBaiDang, String maNguoiDang) {
		String queryTB = String.format("UPDATE THONGBAO SET trangThai=N'Đã xử lý' where maTB = '%s'", maTB);
		String queryBD = String.format("UPDATE BAIDANG SET trangThai=N'Ẩn' where maBD = '%s'", maBaiDang);
		String queryND = String.format("UPDATE NGUOIDUNG SET soLanCanhBao = soLanCanhBao + 1 "
				+ "where tenDangNhap='%s'", maNguoiDang);
		DatabaseDAO db = new DatabaseDAO();
		try {
			Statement st = db.getConnect().createStatement();
			System.out.println(queryTB);
			st.executeUpdate(queryTB);
			System.out.println(queryBD);
			st.executeUpdate(queryBD);
			System.out.println(queryND);
			st.executeUpdate(queryND);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			db.closedConnect();
		}
		
	}

	public void boquaThongBao(String maTB) {
		
		String query = String.format("UPDATE THONGBAO SET trangThai=N'Đã xử lý' where maTB = '%s'", maTB);
		DatabaseDAO db = new DatabaseDAO();
		try {
			Statement st = db.getConnect().createStatement();
			System.out.println(query);
			st.executeUpdate(query);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			db.closedConnect();
		}
		
	}

}
