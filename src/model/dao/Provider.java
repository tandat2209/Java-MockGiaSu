package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import common.SearchProcess;
import common.StringProcess;
import model.bean.BaiDang;
import model.bean.NguoiDung;
import model.bean.ThongBao;
import forms.SearchCourseForm;
import forms.SearchTutorForm;


public class Provider {
	DatabaseDAO db = new DatabaseDAO();
	public void register(NguoiDung nd) {
		Connection connect = db.getConnect();
		String sql = "insert into NGUOIDUNG(tenDangNhap,matKhau,hoTen,"
				+ "gioiTinh,ngaySinh,soDienThoai,email) values(?,?,?,?,CONVERT(DATE,?,103),?,?)";
		try {
			PreparedStatement ps = connect.prepareStatement(sql);
			ps.setString(1, nd.getTenDangNhap());
			ps.setString(2, nd.getMatKhau());
			ps.setString(3, nd.getHoTen());
			ps.setString(4, nd.getGioiTinh());
			ps.setString(5, nd.getNgaySinh());
			ps.setString(6, nd.getSoDienThoai());
			ps.setString(7, nd.getEmail());
			ps.executeUpdate();
			System.out.println("insert successfully");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			db.closedConnect();
		}
	}

	public ArrayList<NguoiDung> getUserList() {
		ArrayList<NguoiDung> list = new ArrayList<NguoiDung>();
		Connection connect = db.getConnect();
		String sql = "select * from NGUOIDUNG";
		try {
			PreparedStatement ps = connect.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				NguoiDung nd = new NguoiDung();
				nd.setTenDangNhap(rs.getString(1));
				nd.setMatKhau(rs.getString(2));
				nd.setHoTen(rs.getString(3));
				nd.setGioiTinh(rs.getString(4));
				nd.setNgaySinh(new SimpleDateFormat("dd/MM/yyyy").format(rs.getDate(5)));
				nd.setSoDienThoai(rs.getString(6));
				nd.setEmail(rs.getString(7));
				nd.setSoLanCanhBao("" + rs.getInt(8));
				nd.setSoBaiDang("" + rs.getInt(9));
				nd.setTrangThai(rs.getString(10));
				list.add(nd);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			db.closedConnect();
		}
		return list;
	}

	public ArrayList<BaiDang> getCourseList(SearchCourseForm f) {
		StringBuffer tem = new StringBuffer(
				"SELECT DISTINCT maBD,loaiBD,tieuDe,mon,lop,thoiGianDay,"
						+ "moTaThem,maNguoiDang,thoiGianDang,trangThai,tenPhuHuynh,diaChi,"
						+ "soBuoiTrenTuan,soLuongHocSinh,luongSD,tenKV "
						+ "FROM (BAIDANG b JOIN BAIDANG_KHUVUC bk ON b.maBD=bk.maBaiDang) JOIN KHUVUC k ON bk.maKhuVuc=k.maKV "
						+ "where loaiBD=N'Suất dạy' and trangThai = N'Đã duyệt'");
		if (!"0".equals(f.getArea()) && f.getArea()!=null) {
			tem.append("and tenKV=N'" + f.getArea() + "' ");
		}
		if (!"0".equals(f.getNumberOfPeriod()) && f.getNumberOfPeriod()!=null) {
			tem.append("and soBuoiTrenTuan='" + f.getNumberOfPeriod() + "' ");
		}
		if (!"0".equals(f.getNumberOfStudent()) && f.getNumberOfStudent()!=null) {
			tem.append("and soLuongHocSinh='" + f.getNumberOfStudent() + "' ");
		}
		if (!"0".equals(f.getMinOfSalary()) && f.getMinOfSalary()!=null) {
			tem.append("and luongSD >=" + f.getMinOfSalary() + " ");
		}
		if (!"0".equals(f.getMaxOfSalary()) && f.getMaxOfSalary()!=null) {
			tem.append("and luongSD <=" + f.getMaxOfSalary() + " ");

		}
		tem.append("order by thoiGianDang desc");
		String sql = tem.toString();
		System.out.println(sql);
		ArrayList<BaiDang> list = new ArrayList<BaiDang>();
		Connection connect = db.getConnect();
		try {
			Statement st = connect.createStatement();
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				BaiDang bd = new BaiDang();
				bd.setMaBD(rs.getString(1));
				bd.setLoaiBD(rs.getString(2));
				bd.setTieuDe(rs.getString(3));
				bd.setMon(rs.getString(4));
				bd.setLop(rs.getString(5));
				bd.setThoiGianDay(rs.getString(6));
				bd.setMoTaThem(rs.getString(7));
				bd.setMaNguoiDang(rs.getString(8));
				/* lay ngay gio tu database */
				Timestamp myTimestamp = rs.getTimestamp(9);
				bd.setThoiGianDang(
						new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(myTimestamp));
				bd.setTrangThai(rs.getString(10));
				bd.setTenPhuHuynh(rs.getString(11));
				bd.setDiaChi(rs.getString(12));
				bd.setSoBuoiTrenTuan("" + rs.getInt(13));
				bd.setSoLuongHocSinh("" + rs.getInt(14));
				bd.setLuongSD(StringProcess.getMoneyVND("" + rs.getInt(15)));
				bd.setKhuVuc(rs.getString(16));
				if (SearchProcess.isCorrect(f.getInput(), bd.getTieuDe())) {
					list.add(bd);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			db.closedConnect();
		}
		return list;
	}

	public ArrayList<BaiDang> getTutorList(SearchTutorForm f) {
		StringBuffer tem = new StringBuffer(
				"SELECT maBD,loaiBD,tieuDe,mon,lop,thoiGianDay,"
						+ "soDienThoai,moTaThem,maNguoiDang,thoiGianDang,trangThai,tenGiaSu,noiCongTac,"
						+ "gioiTinh,ngheNghiep,ngaySinh,queQuan,luongGS from BAIDANG where loaiBD=N'Gia sư' and trangThai = N'Đã duyệt' ");
		if (!"0".equals(f.getCareer()) && f.getCareer()!=null) {
			tem.append("and ngheNghiep=N'" + f.getCareer() + "' ");
		}
		if (!"0".equals(f.getHometown())&& f.getHometown()!=null) {
			tem.append("and queQuan=N'" + f.getHometown() + "' ");
		}
		if (!"0".equals(f.getGender()) && f.getGender()!=null) {
			tem.append("and gioiTinh=N'" + f.getGender() + "' ");
		}
		if (f.getArea() != null) {
			tem.append("and maBD in ( ");
			int size = f.getArea().length;
			for (int i = 0; i < size; i++) {
				if (i == 0) {
					tem.append("select maBD from (BAIDANG b join BAIDANG_KHUVUC bk on b.maBD=bk.maBaiDang) "
							+ "join KHUVUC k on bk.maKhuVuc=k.maKV where tenKV=N'"
							+ f.getArea()[i] + "' ");
				} else {
					tem.append(" intersect select maBD from (BAIDANG b join BAIDANG_KHUVUC bk on b.maBD=bk.maBaiDang)"
							+ " join KHUVUC k on bk.maKhuVuc=k.maKV where tenKV=N'"
							+ f.getArea()[i] + "' ");
				}
			}
			tem.append(") ");
		}
		if (!"0".equals(f.getMinOfWage()) && f.getMinOfWage()!=null) {
			tem.append("and luongGS >=" + f.getMinOfWage() + " ");
		}
		if (!"0".equals(f.getMaxOfWage()) && f.getMaxOfWage()!=null) {
			tem.append("and luongGS <=" + f.getMaxOfWage() + " ");
		}
		tem.append("order by thoiGianDang desc");
		String sql = tem.toString();
		System.out.println(sql);
		ArrayList<BaiDang> list = new ArrayList<BaiDang>();
		Connection connect = db.getConnect();
		try {
			Statement st = connect.createStatement();
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				BaiDang bd = new BaiDang();
				bd.setMaBD(rs.getString(1));
				bd.setLoaiBD(rs.getString(2));
				bd.setTieuDe(rs.getString(3));
				bd.setMon(rs.getString(4));
				bd.setLop(rs.getString(5));
				bd.setThoiGianDay(rs.getString(6));
				bd.setSoDienThoai(rs.getString(7));
				bd.setMoTaThem(rs.getString(8));
				bd.setMaNguoiDang(rs.getString(9));
				/* lay ngay gio tu database */
				Timestamp myTimestamp = rs.getTimestamp(10);
				bd.setThoiGianDang(
						new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(myTimestamp));
				bd.setTrangThai(rs.getString(11));
				bd.setTenGiaSu(rs.getString(12));
				bd.setNoiCongTac(rs.getString(13));
				bd.setGioiTinh(rs.getString(14));
				bd.setNgheNghiep(rs.getString(15));
				bd.setNgaySinh(rs.getString(16));
				bd.setQueQuan(rs.getString(17));
				bd.setLuongGS(StringProcess.getMoneyVND(rs.getString(18)));
				if (SearchProcess.isCorrect(f.getInput(), bd.getTieuDe())) {
					list.add(bd);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			db.closedConnect();
		}
		return list;
	}
	
	public void insertAccnouncement(ThongBao tb) {
		Connection connect = db.getConnect();
		String sql = "INSERT INTO THONGBAO(maBaiDang,email,soDienThoai,noiDung) values(?,?,?,N?)";
		try {
			PreparedStatement ps = connect.prepareStatement(sql);
			ps.setString(1, tb.getMaBaiDang());
			ps.setString(2, tb.getEmail());
			ps.setString(3, tb.getSoDienThoai());
			ps.setString(4, tb.getNoiDung());
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			db.closedConnect();
		}
	}
	public BaiDang getCourseByTitle(String maBD){
		BaiDang bd = new BaiDang();
		Connection connect = db.getConnect();
		String sql = "SELECT maBD,loaiBD,tieuDe,mon,lop,thoiGianDay,moTaThem,maNguoiDang," +
				"thoiGianDang,trangThai,tenPhuHuynh,diaChi,soBuoiTrenTuan,soLuongHocSinh," +
				"luongSD FROM BAIDANG where maBD=?";
		PreparedStatement ps;
		try {
			ps = connect.prepareStatement(sql);
			ps.setString(1, maBD);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				bd.setMaBD(rs.getString(1));
				bd.setLoaiBD(rs.getString(2));
				bd.setTieuDe(rs.getString(3));
				bd.setMon(rs.getString(4));
				bd.setLop(rs.getString(5));
				bd.setThoiGianDay(rs.getString(6));
				bd.setMoTaThem(rs.getString(7));
				bd.setMaNguoiDang(rs.getString(8));
				/* lay ngay gio tu database */
				Timestamp myTimestamp = rs.getTimestamp(9);
				bd.setThoiGianDang(
						new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(myTimestamp));
				bd.setTrangThai(rs.getString(10));
				bd.setTenPhuHuynh(rs.getString(11));
				bd.setDiaChi(rs.getString(12));
				bd.setSoBuoiTrenTuan("" + rs.getInt(13));
				bd.setSoLuongHocSinh("" + rs.getInt(14));
				bd.setLuongSD(StringProcess.getMoneyVND(rs.getInt(15)+""));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			db.closedConnect();
		}
		return bd;
	}
	
	public static void main(String[] args) {
		Provider p = new Provider();
		p.getUserList();
	}
}
