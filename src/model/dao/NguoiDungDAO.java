package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import model.bean.NguoiDung;

public class NguoiDungDAO {

	public ArrayList<NguoiDung> getNguoiDungArray() {
		DatabaseDAO db = new DatabaseDAO();
		Connection con = db.getConnect();
		String query = "SELECT  hoTen, gioiTinh, ngaySinh, soDienThoai, email, tenDangNhap, matKhau, "
				+ "soBaiDang, soLanCanhBao, trangThai from NGUOIDUNG";
		ArrayList<NguoiDung> nguoiDungArray = new ArrayList<NguoiDung>();
		try {
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(query);
			while (rs.next()) {
				NguoiDung nd = new NguoiDung();
				nd.setHoTen(rs.getString("hoTen"));
				nd.setGioiTinh(rs.getString("gioiTinh"));
				nd.setNgaySinh(rs.getString("ngaySinh"));
				nd.setSoDienThoai(rs.getString("soDienThoai"));
				nd.setEmail(rs.getString("email"));
				nd.setTenDangNhap(rs.getString("tenDangNhap"));
				nd.setMatKhau(rs.getString("matKhau"));
				nd.setSoLanCanhBao(rs.getInt("soLanCanhBao") + "");
				nd.setTrangThai(rs.getString("trangThai"));
				nd.setSoBaiDang("soBaiDang");
				nguoiDungArray.add(nd);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			db.closedConnect();
		}

		return nguoiDungArray;
	}

	/**
	 * 
	 * @param @param tenDangNhap
	 * @param @return
	 * @return NguoiDung
	 * @throws
	 */

	public static NguoiDung getThongTinNguoiDung(String tenDangNhap) {
		NguoiDung nd = new NguoiDung();
		DatabaseDAO db = new DatabaseDAO();
		Connection con = db.getConnect();
		String query = "SELECT hoTen, gioiTinh, ngaySinh, soDienThoai, email, tenDangNhap, matKhau, "
				+ " soLanCanhBao,soBaiDang, trangThai, anhDaiDien from NGUOIDUNG where tenDangNhap='"
				+ tenDangNhap + "'";
		System.out.println(query);
		try {
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(query);
			while (rs.next()) {
				nd.setHoTen(rs.getString("hoTen"));
				nd.setGioiTinh(rs.getString("gioiTinh"));
				Date date = rs.getDate("ngaySinh");
				SimpleDateFormat dt1 = new SimpleDateFormat("dd/MM/yyyy");
				nd.setNgaySinh(dt1.format(date));
				nd.setSoDienThoai(rs.getString("soDienThoai"));
				nd.setEmail(rs.getString("email"));
				nd.setTenDangNhap(rs.getString("tenDangNhap"));
				nd.setMatKhau(rs.getString("matKhau"));
				nd.setSoBaiDang(rs.getString("soBaiDang"));
				nd.setSoLanCanhBao(rs.getString("soLanCanhBao"));
				nd.setTrangThai(rs.getString("trangThai"));
				nd.setAnhDaiDien(rs.getString("anhDaiDien"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			db.closedConnect();
		}
		return nd;
	}

	/**	
	 * 	
	 * @param @param nd
	 * @return void
	 * @throws 	
	 */	
	
	public static void updateThongTinND(NguoiDung nd) {
		// TODO Auto-generated method stub
		DatabaseDAO db = new DatabaseDAO();
		Connection con = db.getConnect();
		try {
			String sql = "update NGUOIDUNG set hoTen=?, gioiTinh=?, ngaySinh=convert(date,?,103), soDienThoai=?,email=? where tenDangNhap=?";
			PreparedStatement st = con.prepareStatement(sql);
			st.setString(1, nd.getHoTen());
			st.setString(2, nd.getGioiTinh());
			st.setString(3, nd.getNgaySinh());
			st.setString(4, nd.getSoDienThoai());
			st.setString(5, nd.getEmail());
			st.setString(6, nd.getTenDangNhap());
			st.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			db.closedConnect();
		}
	}
	/**
	 * DatNTV	
	 * 	
	 * @param @return
	 * @return ArrayList<NguoiDung>
	 * @throws
	 */
	public ArrayList<NguoiDung> getNguoiDungList() {
		String query = "SELECT tenDangNhap, hoTen, soBaiDang, trangThai from NGUOIDUNG order by tenDangNhap";
		ArrayList<NguoiDung> nguoiDungList = new ArrayList<NguoiDung>();
		DatabaseDAO db = new DatabaseDAO();
		try {
			Statement st = db.getConnect().createStatement();
			System.out.println(query);
			ResultSet rs = st.executeQuery(query);
			NguoiDung nd;
			while (rs.next()) {
				nd = new NguoiDung();
                nd.setTenDangNhap(rs.getString("tenDangNhap"));
                nd.setHoTen(rs.getString("hoTen"));
				nd.setSoBaiDang(rs.getString("soBaiDang"));
				nd.setTrangThai(rs.getString("trangThai"));
				nguoiDungList.add(nd);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			db.closedConnect();
		}
		return nguoiDungList;
	}
	
	public ArrayList<NguoiDung> getNguoiDungList(String searchString) {
		String query ="SELECT tenDangNhap, hoTen, soBaiDang, trangThai from NGUOIDUNG"
				+ " where hoTen like N'%"+searchString+"%' or tenDangNhap like '%"+searchString+"%' order by tenDangNhap";
		ArrayList<NguoiDung> nguoiDungList = new ArrayList<NguoiDung>();
        DatabaseDAO db = new DatabaseDAO();
        try {
            Statement st = db.getConnect().createStatement();
            System.out.println(query);
            ResultSet rs = st.executeQuery(query);
            NguoiDung nd;
            while (rs.next()) {
                nd = new NguoiDung();
                nd.setTenDangNhap(rs.getString("tenDangNhap"));
                nd.setHoTen(rs.getString("hoTen"));
                nd.setSoBaiDang(rs.getString("soBaiDang"));
                nd.setTrangThai(rs.getString("trangThai"));
                nguoiDungList.add(nd);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            db.closedConnect();
        }
        return nguoiDungList;
	}

	/**	
	 * 	
	 * @param @param tenDangNhap
	 * @param @param pwd1
	 * @return void
	 * @throws 	
	 */	
	
	public void doiMatKhau(String tenDangNhap, String pwd1) {
		// TODO Auto-generated method stub
		DatabaseDAO db = new DatabaseDAO();
		Connection con = db.getConnect();
		try {
			String sql = String.format("update NGUOIDUNG set matKhau='%s' where tenDangNhap='%s'",pwd1,tenDangNhap);
			System.out.println(sql);
			Statement st = con.createStatement();
			st.executeUpdate(sql);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			db.closedConnect();
		}
	}

	/**
	 * @param tenDangNhap
	 * @return
	 */
	public boolean checkNguoiDung(String tenDangNhap) {
		String query = String.format("SELECT * from NGUOIDUNG where tenDangNhap='%s'",tenDangNhap);
		DatabaseDAO db = new DatabaseDAO();
		try {
			Statement st = db.getConnect().createStatement();
			ResultSet rs = st.executeQuery(query);
			if(rs.next())
				return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			db.closedConnect();
		}
		return false;
	}
	/**
     * @param tenDangNhap
     */
    public void chanNguoiDung(String tenDangNhap) {
        String query = String.format("UPDATE NGUOIDUNG SET trangThai=N'Bị chặn' WHERE tenDangNhap='%s'", tenDangNhap);
        DatabaseDAO db = new DatabaseDAO();
        try {
            Statement st = db.getConnect().createStatement();
            st.executeUpdate(query);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally{
            db.closedConnect();
        }
    }

    /**
     * @param tenDangNhap
     */
    public void boChanNguoiDung(String tenDangNhap) {
        String query = String.format("UPDATE NGUOIDUNG SET trangThai=N'Bình thường' WHERE tenDangNhap='%s'", tenDangNhap);
        DatabaseDAO db = new DatabaseDAO();
        try {
            Statement st = db.getConnect().createStatement();
            st.executeUpdate(query);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally{
            db.closedConnect();
        }
    }

	/**
	 * @param tenDangNhap
	 */
    public void resetMatKhau(String tenDangNhap) {
	    String query = String.format("UPDATE NGUOIDUNG SET matKhau = 'e10adc3949ba59abbe56e057f20f883e'"
	    		+ " where tenDangNhap = '%s'", tenDangNhap);
	    DatabaseDAO db = new DatabaseDAO();
	    try {
	        Statement st = db.getConnect().createStatement();
	        st.executeUpdate(query);
        } catch (SQLException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
        } finally{
        	db.closedConnect();
        }
	    
    }

	/**
	 * @param tenDangNhap
	 * @param imgPath
	 */
    public void doiAnhDaiDien(String tenDangNhap, String imgPath) {
	    String query = String.format("UPDATE NGUOIDUNG SET anhDaiDien='%s'"
	    		+ " where tenDangNhap = '%s'", imgPath, tenDangNhap);
	    DatabaseDAO db = new DatabaseDAO();
	    try {
	        Statement st = db.getConnect().createStatement();
	        st.executeUpdate(query);
        } catch (SQLException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
        } finally{
        	db.closedConnect();
        }
	    
    }
}
