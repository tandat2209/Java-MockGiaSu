package model.dao;

import java.sql.Connection;
import java.sql.DriverManager;

public class DatabaseDAO {

	private Connection connect;

	public Connection getConnect() {
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			Connection connect = DriverManager.getConnection(
					"jdbc:sqlserver://localhost:1433;databaseName=Mock",
					"sa", "12345678");
			System.out.println("Ket Noi Thanh Cong Toi CSDL");

			return connect;

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Ket noi that bai");
			return null;
		}
	}

	public void closedConnect() {
		try {
			if (connect != null) {
				connect.close();
			}
		} catch (Exception e) {

		}
	}

}
