/**
 * 
 */
package model.bo;

import java.util.ArrayList;

import model.bean.KhuVuc;
import model.dao.KhuVucDAO;

/**
 * KhuVucBO.java
 *
 * Version 1.0
 *
 * Date: Aug 16, 2015
 *
 * Copyright 
 *
 * Modification Logs:
 * DATE                 AUTHOR          DESCRIPTION
 * -----------------------------------------------------------------------
 * 8:36:09 PM        	HuyNV          Create
 */
public class KhuVucBO {
	KhuVucDAO kvDAO = new KhuVucDAO();
	public String getKhuVucSD(String maBD){
		return kvDAO.getKhuVucSD(maBD);
	}
	/**
	 * @param maBD
	 * @param khuVuc
	 */
	public void updateSuatDayKV(String maBD, String khuVuc) {
		// TODO Auto-generated method stub
		kvDAO.updateSuatDayKV(maBD,khuVuc);
	}
	/**	
	 * 	
	 * @param @param maBD
	 * @param @return
	 * @return ArrayList<KhuVuc>
	 * @throws 	
	 */	
	
	public ArrayList<KhuVuc> getKVList() {
		// TODO Auto-generated method stub
		return kvDAO.getKVList();
	}
	/**	
	 * 	
	 * @param @param maBD
	 * @param @return
	 * @return ArrayList<String>
	 * @throws 	
	 */	
	//
	
	public ArrayList<String> maLvList(String maBD) {
		// TODO Auto-generated method stub
		return kvDAO.maLvList(maBD);
	}
	/**
	 * @param maBD 	
	 * 	
	 * @param @param kvListBD
	 * @return void
	 * @throws 	
	 */	
	
	public void updateKvBaiDang(ArrayList<String> kvListBD, String maBD) {
		// TODO Auto-generated method stub
		kvDAO.updateKvBaiDang(kvListBD,maBD);
	}
	
	/**
	 * DatNVT
	 * lay khu vuc list tu bang KV_BD
	 * @param @param maBD
	 * @param @return
	 * @return ArrayList<KhuVuc>
	 * @throws
	 */
	public ArrayList<KhuVuc> getKhuVucListByMaBD(String maBD) {
    return kvDAO.getKhuVucListByMaBD(maBD);
}

}
