/**
 * 
 */
package model.bo;

import java.util.ArrayList;

import model.bean.BaiDang;
import model.dao.BaiDangDAO;

/**				
 * BaiDangBO.java			
 *				
 * Version 1.0				
 *				
 * Date: Aug 14, 2015			
 *				
 * Copyright 				
 *				
 * Modification Logs:				
 * DATE                 AUTHOR          DESCRIPTION				
 * -----------------------------------------------------------------------				
 * Aug 14, 2015        HuyNV          Create				
 */

public class BaiDangBO {

	
	BaiDangDAO bdDAO = new BaiDangDAO();
	public static ArrayList<BaiDang> getlistBD(String trangThai) {
		// TODO Auto-generated method stub
		return BaiDangDAO.getBaiDangList(trangThai);
	}

	public ArrayList<BaiDang> getBaiDangList() {
		return bdDAO.getBaiDangList();
	}
	
	public ArrayList<BaiDang> getBaiDangListByMaND(String maNguoiDang){
		return bdDAO.getBaiDangListByMaND(maNguoiDang);
	}
	public BaiDang getBaiDang(String maBD) {
		// TODO Auto-generated method stub
		return bdDAO.getBaiDang(maBD);
	}

	/**
	 * @param maBD
	 * @return
	 */
	public String getKhuVuc(String maBD) {
		// TODO Auto-generated method stub
		return BaiDangDAO.getKhuVuc(maBD);
	}



	/**	
	 * 	
	 * @param @param bd
	 * @return void
	 * @throws 	
	 */	
	
	public void updateBaiDang(BaiDang bd) {
		// TODO Auto-generated method stub
		BaiDangDAO.updateBaiDang(bd);
	}

	/**	
	 * 	DatNTV
	 * 
	 * @param @param bd
	 * @return void
	 * @throws 	
	 */	
	
	public void insertBaiDang(BaiDang bd) {
		// TODO Auto-generated method stub
		BaiDangDAO.insertBaiDang(bd);
	}
	
public void duyetBaiDang(String maBD) {
		
		bdDAO.duyetBaiDang(maBD);
	}
	/**
	 * @param maBD
	 */
	public void tuchoiBaiDang(String maBD) {
		// TODO Auto-generated method stub
		bdDAO.tuchoiBaiDang(maBD);
	}

	/**	
	 * 	
	 * @param @param maBD
	 * @return void
	 * @throws 	
	 */	
	
	public void deleteBaiDang(String maBD) {
		// TODO Auto-generated method stub
		bdDAO.deleteBaiDang(maBD);
	}



	/**
	 * @param bd
	 * @return
	 */
    public static ArrayList<BaiDang> getSDLienQuanList(BaiDang bd) {
	    // TODO Auto-generated method stub
	    return BaiDangDAO.getSDLienQuanList(bd);
    }

	/**
	 * @param bd
	 * @return
	 */
    public static ArrayList<BaiDang> getGSLienQuanList(BaiDang bd) {
	    // TODO Auto-generated method stub
	    return BaiDangDAO.getGSLienQuanList(bd);
    }
}
