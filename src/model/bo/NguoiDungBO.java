package model.bo;

import java.util.ArrayList;

import model.bean.NguoiDung;
import model.dao.NguoiDungDAO;

public class NguoiDungBO {
	NguoiDungDAO ndDAO = new NguoiDungDAO();
	
	/**	
	 * 	
	 * @param @param tenDangNhap
	 * @param @return
	 * @return NguoiDung
	 * @throws 	
	 */	
	
	public NguoiDung getThongTinNguoiDung(String tenDangNhap) {
		// TODO Auto-generated method stub
		return NguoiDungDAO.getThongTinNguoiDung(tenDangNhap);
	}
	/**	
	 * 	
	 * @param @param nd
	 * @return void
	 * @throws 	
	 */	
	
	public void updateThongTinND(NguoiDung nd) {
		// TODO Auto-generated method stub
		NguoiDungDAO.updateThongTinND(nd);
	}
	
	public ArrayList<NguoiDung> getNguoiDungArray() {
		return ndDAO.getNguoiDungList();
	}
	
	public ArrayList<NguoiDung> getNguoiDungArray(String searchString) {
		return ndDAO.getNguoiDungList(searchString);
	}
	/**	
	 * 	
	 * @param @param tenDangNhap
	 * @param @param pwd1
	 * @return void
	 * @throws 	
	 */	
	
	public void ndDoiMatKhau(String tenDangNhap, String pwd1) {
		// TODO Auto-generated method stub
		ndDAO.doiMatKhau(tenDangNhap,pwd1);
	}
	/**
	 * @param tenDangNhap
	 * @return
	 */
	public boolean checkNguoiDung(String tenDangNhap) {
		// TODO Auto-generated method stub
		return ndDAO.checkNguoiDung(tenDangNhap);
	}
	/**
     * @param tenDangNhap
     */
    public void chanNguoiDung(String tenDangNhap) {
        ndDAO.chanNguoiDung(tenDangNhap);
        
    }
    /**
     * @param tenDangNhap
     */
    public void boChanNguoiDung(String tenDangNhap) {
        ndDAO.boChanNguoiDung(tenDangNhap);
    }
	/**
	 * @param tenDangNhap
	 */
    public void resetMatKhau(String tenDangNhap) {
	   ndDAO.resetMatKhau(tenDangNhap);
	    
    }
	/**
	 * @param tenDangNhap
	 * @param imgPath
	 */
    public void doiAnhDaiDien(String tenDangNhap, String imgPath) {
	    ndDAO.doiAnhDaiDien(tenDangNhap, imgPath);
	    
    }

}
