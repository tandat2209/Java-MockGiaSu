package model.bo;

import java.sql.SQLException;
import java.util.ArrayList;

import model.bean.NhanVien;
import model.dao.NhanVienDAO;

public class NhanVienBO {
	NhanVienDAO nvDAO = new NhanVienDAO();

//	public NhanVien getNVByTenDangNhap(String tenDangNhap) {
//		return nvDAO.getNVByTenDangNhap(tenDangNhap);
//	}

	public NhanVien getNV(String tenDangNhap) {
		return nvDAO.getNV(tenDangNhap);
	}
	/**
	 * @param nhanVien
	 */
	public void capNhatThongTinNV(NhanVien nhanVien) {
		nvDAO.capNhatThongTinNV(nhanVien);
	}

	public  ArrayList<NhanVien> getAll() {
		return nvDAO.getAll();
	}

	public  ArrayList<NhanVien> getBySearchName(String searchName) {
		return nvDAO.getBySearchName(searchName);
	}

	public  boolean updateNhanVien(NhanVien nv) {
		return nvDAO.updateNhanVien(nv);
	}

	public  boolean xoaNhanVien(String tenDangNhap) {
		return nvDAO.xoaNhanVien(tenDangNhap);
	}

	public  boolean themNhanVien(NhanVien nv) throws SQLException {
		return nvDAO.themNhanVien(nv);
	}
	/**	
	 * 	
	 * @param @param tenDangNhap
	 * @param @param pwd1
	 * @return void
	 * @throws 	
	 */	
	
	public void nvDoiMatKhau(String tenDangNhap, String pwd1) {
		// TODO Auto-generated method stub
		nvDAO.nvDoiMatKhau(tenDangNhap,pwd1);
	}
	/**
	 * Dung de kiem tra quyen cua nhanvien
	 * @param tenDangNhap
	 * @return true if co nhanVien, nguoc lai thi khong
	 */
	public boolean checkNhanVien(String tenDangNhap) {
		// TODO Auto-generated method stub
		return nvDAO.checkNhanVien(tenDangNhap);
	}
}
