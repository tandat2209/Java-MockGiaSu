/**
 * 
 */
package model.bo;

import java.util.ArrayList;

import model.bean.ThongBao;
import model.dao.ThongBaoDAO;

/**
 * ThongBaoBO.java
 *
 * Version 1.0
 *
 * Date: Aug 16, 2015
 *
 * Copyright 
 *
 * Modification Logs:
 * DATE                 AUTHOR          DESCRIPTION
 * -----------------------------------------------------------------------
 * 10:03:44 AM        	HuyNV          Create
 */
public class ThongBaoBO {
		/**
		 * @param tenDangNhap
		 * @return
		 */
		ThongBaoDAO tbDAO = new ThongBaoDAO();
		public static ArrayList<ThongBao> getThongBaoListND(String tenDangNhap) {
			// TODO Auto-generated method stub
			return ThongBaoDAO.getThongBaoListND(tenDangNhap);
		}
		public ArrayList<ThongBao> getThongBaoList() {
	        
	        return tbDAO.getThongBaoList();
	    }
	    public ThongBao getThongBaoByMaTB(String maTB) {
	        
	        return tbDAO.getThongBaoByMaTB(maTB);
	    }
		public void anThongBao(String maTB, String maBaiDang, String maNguoiDang) {
			
			tbDAO.anThongBao(maTB, maBaiDang, maNguoiDang);
		}
		public void boquaThongBao(String maTB) {
			
			tbDAO.boquaThongBao(maTB);
		}
}		
