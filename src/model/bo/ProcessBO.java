package model.bo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import forms.SearchCourseForm;
import forms.SearchTutorForm;
import forms.ToCaoForm;
import model.bean.BaiDang;
import model.bean.NguoiDung;
import model.bean.ThongBao;
import model.dao.Provider;

public class ProcessBO {
	public static void register(NguoiDung nd) {
		Provider p = new Provider();
		p.register(nd);
	}

	public static ArrayList<NguoiDung> getUserList() {
		Provider p = new Provider();
		return p.getUserList();
	}

	public static ArrayList<BaiDang> getCourseList(SearchCourseForm f) {
		Provider p = new Provider();
		return p.getCourseList(f);
	}

	public static ArrayList<BaiDang> getTutorList(SearchTutorForm f) {
		Provider p = new Provider();
		return p.getTutorList(f);
	}

	public static boolean isExist(String tenDN) {
		boolean check = false;
		Provider p = new Provider();
		ArrayList<NguoiDung> list = p.getUserList();
		int size = list.size();
		for (int i = 0; i < size; i++) {
			if (list.get(i).getTenDangNhap().equals(tenDN)) {
				check = true;
				break;
			}
		}
		return check;
	}

	public static boolean isExistEmail(String email) {
		boolean check = false;
		Provider p = new Provider();
		ArrayList<NguoiDung> list = p.getUserList();
		int size = list.size();
		for (int i = 0; i < size; i++) {
			if (list.get(i).getEmail().equals(email)) {
				check = true;
				break;
			}
		}
		return check;
	}

	public static void insertAccnouncement(ThongBao tb) {
		Provider p = new Provider();
		p.insertAccnouncement(tb);
	}
	public static BaiDang getCourseByTitle(String maBD){
		Provider p = new Provider();
		return p.getCourseByTitle(maBD);
	}
	public static void main(String[] args) {
		System.out.println(ProcessBO.getCourseByTitle("1").getLoaiBD());
	}
}
