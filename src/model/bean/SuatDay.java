/**
 * 
 */
package model.bean;

/**				
 * SuatDay.java			
 *				
 * Version 1.0				
 *				
 * Date: Aug 14, 2015			
 *				
 * Copyright 				
 *				
 * Modification Logs:				
 * DATE                 AUTHOR          DESCRIPTION				
 * -----------------------------------------------------------------------				
 * Aug 14, 2015        HuyNV          Create				
 */
public class SuatDay {
	private String maSD;
	private String tieuDe;
	private String tenPhuHuynh;
	private String diaChi;
	private String maKhuVuc;
	private String mon;
	private String lop;
	private String soBuoiTrenTuan;
	private String soLuongHocSinh;
	private String luong;
	private String thoiGianDay;
	private String moTaThem;
	private String thoiGianDang;
	private String trangThai;
	private String maNguoiDang;
	public String getMaSD() {
		return maSD;
	}
	public void setMaSD(String maSD) {
		this.maSD = maSD;
	}
	public String getTieuDe() {
		return tieuDe;
	}
	public void setTieuDe(String tieuDe) {
		this.tieuDe = tieuDe;
	}
	public String getTenPhuHuynh() {
		return tenPhuHuynh;
	}
	public void setTenPhuHuynh(String tenPhuHuynh) {
		this.tenPhuHuynh = tenPhuHuynh;
	}
	public String getDiaChi() {
		return diaChi;
	}
	public void setDiaChi(String diaChi) {
		this.diaChi = diaChi;
	}
	public String getMaKhuVuc() {
		return maKhuVuc;
	}
	public void setMaKhuVuc(String maKhuVuc) {
		this.maKhuVuc = maKhuVuc;
	}
	public String getMon() {
		return mon;
	}
	public void setMon(String mon) {
		this.mon = mon;
	}
	public String getLop() {
		return lop;
	}
	public void setLop(String lop) {
		this.lop = lop;
	}
	public String getSoBuoiTrenTuan() {
		return soBuoiTrenTuan;
	}
	public void setSoBuoiTrenTuan(String soBuoiTrenTuan) {
		this.soBuoiTrenTuan = soBuoiTrenTuan;
	}
	public String getSoLuongHocSinh() {
		return soLuongHocSinh;
	}
	public void setSoLuongHocSinh(String soLuongHocSinh) {
		this.soLuongHocSinh = soLuongHocSinh;
	}
	public String getLuong() {
		return luong;
	}
	public void setLuong(String luong) {
		this.luong = luong;
	}
	public String getThoiGianDay() {
		return thoiGianDay;
	}
	public void setThoiGianDay(String thoiGianDay) {
		this.thoiGianDay = thoiGianDay;
	}
	public String getMoTaThem() {
		return moTaThem;
	}
	public void setMoTaThem(String moTaThem) {
		this.moTaThem = moTaThem;
	}
	public String getThoiGianDang() {
		return thoiGianDang;
	}
	public void setThoiGianDang(String thoiGianDang) {
		this.thoiGianDang = thoiGianDang;
	}
	public String getTrangThai() {
		return trangThai;
	}
	public void setTrangThai(String trangThai) {
		this.trangThai = trangThai;
	}
	public String getMaNguoiDang() {
		return maNguoiDang;
	}
	public void setMaNguoiDang(String maNguoiDang) {
		this.maNguoiDang = maNguoiDang;
	}
	
}
