/**
 * 
 */
package model.bean;

/**				
 * GiaSu.java			
 *				
 * Version 1.0				
 *				
 * Date: Aug 14, 2015			
 *				
 * Copyright 				
 *				
 * Modification Logs:				
 * DATE                 AUTHOR          DESCRIPTION				
 * -----------------------------------------------------------------------				
 * Aug 14, 2015        HuyNV          Create				
 */
public class GiaSu {
	private String maGS;
	private String tieuDe;
	private String tenGS;
	private String gioiTinh;
	private String soDienThoai;
	private String noiCongTac;
	private String ngheNghiep;
	private String ngaySinh;
	private String queQuan;
	private String moTaThem;
	private String thoiGianDang;
	private String trangThai;
	private String maNguoiDang;
	public String getMaGS() {
		return maGS;
	}
	public void setMaGS(String maGS) {
		this.maGS = maGS;
	}
	public String getTieuDe() {
		return tieuDe;
	}
	public void setTieuDe(String tieuDe) {
		this.tieuDe = tieuDe;
	}
	public String getTenGS() {
		return tenGS;
	}
	public void setTenGS(String tenGS) {
		this.tenGS = tenGS;
	}
	public String getGioiTinh() {
		return gioiTinh;
	}
	public void setGioiTinh(String gioiTinh) {
		this.gioiTinh = gioiTinh;
	}
	public String getSoDienThoai() {
		return soDienThoai;
	}
	public void setSoDienThoai(String soDienThoai) {
		this.soDienThoai = soDienThoai;
	}
	public String getNoiCongTac() {
		return noiCongTac;
	}
	public void setNoiCongTac(String noiCongTac) {
		this.noiCongTac = noiCongTac;
	}
	public String getNgheNghiep() {
		return ngheNghiep;
	}
	public void setNgheNghiep(String ngheNghiep) {
		this.ngheNghiep = ngheNghiep;
	}
	public String getNgaySinh() {
		return ngaySinh;
	}
	public void setNgaySinh(String ngaySinh) {
		this.ngaySinh = ngaySinh;
	}
	public String getQueQuan() {
		return queQuan;
	}
	public void setQueQuan(String queQuan) {
		this.queQuan = queQuan;
	}
	public String getMoTaThem() {
		return moTaThem;
	}
	public void setMoTaThem(String moTaThem) {
		this.moTaThem = moTaThem;
	}
	public String getThoiGianDang() {
		return thoiGianDang;
	}
	public void setThoiGianDang(String thoiGianDang) {
		this.thoiGianDang = thoiGianDang;
	}
	public String getTrangThai() {
		return trangThai;
	}
	public void setTrangThai(String trangThai) {
		this.trangThai = trangThai;
	}
	public String getMaNguoiDang() {
		return maNguoiDang;
	}
	public void setMaNguoiDang(String maNguoiDang) {
		this.maNguoiDang = maNguoiDang;
	}
	
}
