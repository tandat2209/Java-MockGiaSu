/**
 * 
 */
package model.bean;

/**
 * BaiDang.java
 * 
 * Version 1.0
 * 
 * Date: Aug 14, 2015
 * 
 * Copyright
 * 
 * Modification Logs: DATE AUTHOR DESCRIPTION
 * ----------------------------------------------------------------------- Aug
 * 14, 2015 HuyNV Create
 */

public class BaiDang {
    private String maBD;
    private String loaiBD;
    private String tieuDe;
    private String mon;
    private String lop;
    private String moTaThem;
    private String maNguoiDang;
    private String thoiGianDang;
    private String trangThai;
    private String khuVuc;
    

	/*
     * phan cho loai bai dang gia su
     */
    private String tenGiaSu;
    private String gioiTinh;
    private String soDienThoai;
    private String noiCongTac;
    private String ngheNghiep;
    private String ngaySinh;
    private String queQuan;
    private String luongGS;

    /*
     * phan cho loai bai dang suat day
     */
    private String tenPhuHuynh;
    private String diaChi;
    private String soBuoiTrenTuan;
    private String soLuongHocSinh;
    private String luongSD;
    private String thoiGianDay;

    public String getKhuVuc() {
		return khuVuc;
	}

	public void setKhuVuc(String khuVuc) {
		this.khuVuc = khuVuc;
	}
    public String getMaBD() {
        return maBD;
    }

    public void setMaBD(String maBD) {
        this.maBD = maBD;
    }

    public String getLoaiBD() {
        return loaiBD;
    }

    public void setLoaiBD(String loaiBD) {
        this.loaiBD = loaiBD;
    }

    public String getTieuDe() {
        return tieuDe;
    }

    public void setTieuDe(String tieuDe) {
        this.tieuDe = tieuDe;
    }

    public String getMoTaThem() {
        return moTaThem;
    }

    public void setMoTaThem(String moTaThem) {
        this.moTaThem = moTaThem;
    }

    public String getMaNguoiDang() {
        return maNguoiDang;
    }

    public void setMaNguoiDang(String maNguoiDang) {
        this.maNguoiDang = maNguoiDang;
    }

    public String getThoiGianDang() {
        return thoiGianDang;
    }

    public void setThoiGianDang(String thoiGianDang) {
        this.thoiGianDang = thoiGianDang;
    }

    public String getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(String trangThai) {
        this.trangThai = trangThai;
    }

    public String getTenGiaSu() {
        return tenGiaSu;
    }

    public void setTenGiaSu(String tenGiaSu) {
        this.tenGiaSu = tenGiaSu;
    }

    public String getGioiTinh() {
        return gioiTinh;
    }

    public void setGioiTinh(String gioiTinh) {
        this.gioiTinh = gioiTinh;
    }

    public String getSoDienThoai() {
        return soDienThoai;
    }

    public void setSoDienThoai(String soDienThoai) {
        this.soDienThoai = soDienThoai;
    }

    public String getNoiCongTac() {
        return noiCongTac;
    }

    public void setNoiCongTac(String noiCongTac) {
        this.noiCongTac = noiCongTac;
    }

    public String getNgheNghiep() {
        return ngheNghiep;
    }

    public void setNgheNghiep(String ngheNghiep) {
        this.ngheNghiep = ngheNghiep;
    }

    public String getNgaySinh() {
        return ngaySinh;
    }

    public void setNgaySinh(String ngaySinh) {
        this.ngaySinh = ngaySinh;
    }

    public String getQueQuan() {
        return queQuan;
    }

    public void setQueQuan(String queQuan) {
        this.queQuan = queQuan;
    }

    public String getLuongGS() {
        return luongGS;
    }

    public void setLuongGS(String luongGS) {
        this.luongGS = luongGS;
    }

    public String getTenPhuHuynh() {
        return tenPhuHuynh;
    }

    public void setTenPhuHuynh(String tenPhuHuynh) {
        this.tenPhuHuynh = tenPhuHuynh;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public String getMon() {
        return mon;
    }

    public void setMon(String mon) {
        this.mon = mon;
    }

    public String getLop() {
        return lop;
    }

    public void setLop(String lop) {
        this.lop = lop;
    }

    public String getSoBuoiTrenTuan() {
        return soBuoiTrenTuan;
    }

    public void setSoBuoiTrenTuan(String soBuoiTrenTuan) {
        this.soBuoiTrenTuan = soBuoiTrenTuan;
    }

    public String getSoLuongHocSinh() {
        return soLuongHocSinh;
    }

    public void setSoLuongHocSinh(String soLuongHocSinh) {
        this.soLuongHocSinh = soLuongHocSinh;
    }

    public String getLuongSD() {
        return luongSD;
    }

    public void setLuongSD(String luongSD) {
        this.luongSD = luongSD;
    }

    public String getThoiGianDay() {
        return thoiGianDay;
    }

    public void setThoiGianDay(String thoiGianDay) {
        this.thoiGianDay = thoiGianDay;
    }

}
