package model.bean;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Key {

	public static String getKey() {
		
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = new Date();
		
		String[] arr = df.format(date).split(" ");
		
		StringBuffer key = new StringBuffer();
		
		// Lay dd/mm/yyyy
		String[]a = arr[0].split("/");
		key.append(a[0]);
		key.append(a[1]);
		key.append(a[2]);
		
		//Lay HH:mm:ss
		a = arr[1].split(":");
		key.append(a[0]);
		key.append(a[1]);
		key.append(a[2]);
		
		return key.toString();
	}
}
