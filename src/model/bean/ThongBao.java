/**
 * 
 */
package model.bean;

/**				
 * ThongBao.java			
 *				
 * Version 1.0				
 *				
 * Date: Aug 14, 2015			
 *				
 * Copyright 				
 *				
 * Modification Logs:				
 * DATE                 AUTHOR          DESCRIPTION				
 * -----------------------------------------------------------------------				
 * Aug 14, 2015        HuyNV          Create				
 */

public class ThongBao {
	private String maTB;
	private String maBaiDang;
	private String tieuDe;
	private String ngayBao;
	private String email;
	private String soDienThoai;
	private String noiDung;
	private String trangThai;
	private String maNguoiDang;
	public String getMaTB() {
		return maTB;
	}
	public void setMaTB(String maTB) {
		this.maTB = maTB;
	}
	public String getMaBaiDang() {
		return maBaiDang;
	}
	public void setMaBaiDang(String maBaiDang) {
		this.maBaiDang = maBaiDang;
	}
	public String getTieuDe() {
        return tieuDe;
    }
    public void setTieuDe(String tieuDe) {
        this.tieuDe = tieuDe;
    }
    public String getNgayBao() {
		return ngayBao;
	}
	public void setNgayBao(String ngayBao) {
		this.ngayBao = ngayBao;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSoDienThoai() {
		return soDienThoai;
	}
	public void setSoDienThoai(String soDienThoai) {
		this.soDienThoai = soDienThoai;
	}
	public String getNoiDung() {
		return noiDung;
	}
	public void setNoiDung(String noiDung) {
		this.noiDung = noiDung;
	}
	public String getTrangThai() {
		return trangThai;
	}
	public void setTrangThai(String trangThai) {
		this.trangThai = trangThai;
	}
	public String getMaNguoiDang() {
		return maNguoiDang;
	}
	public void setMaNguoiDang(String maNguoiDang) {
		this.maNguoiDang = maNguoiDang;
	}
	
}
