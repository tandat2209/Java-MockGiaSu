package model.bean;

public class NguoiDung {
    private String tenDangNhap;
    private String matKhau;
	private String hoTen;
	private String gioiTinh;
	private String ngaySinh;
	private String soDienThoai;
	private String email;
	private String soLanCamOn;
	private String soLanCanhBao;
	private String soBaiDang;
	private String trangThai;
	private String anhDaiDien;
	
	public String getAnhDaiDien() {
		return anhDaiDien;
	}
	public void setAnhDaiDien(String anhDaiDien) {
		this.anhDaiDien = anhDaiDien;
	}
	public String getSoBaiDang() {
		return soBaiDang;
	}
	public void setSoBaiDang(String soBaiDang) {
		this.soBaiDang = soBaiDang;
	}
	public String getNgaySinh() {
		return ngaySinh;
	}
	public void setNgaySinh(String ngaySinh) {
		this.ngaySinh = ngaySinh;
	}
	public String getHoTen() {
        return hoTen;
    }
    public void setHoTen(String hoTen) {
        this.hoTen = hoTen;
    }
	public String getGioiTinh() {
		return gioiTinh;
	}
	public void setGioiTinh(String gioiTinh) {
		this.gioiTinh = gioiTinh;
	}
	public String getSoDienThoai() {
		return soDienThoai;
	}
	public void setSoDienThoai(String soDienThoai) {
		this.soDienThoai = soDienThoai;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTenDangNhap() {
		return tenDangNhap;
	}
	public void setTenDangNhap(String tenDangNhap) {
		this.tenDangNhap = tenDangNhap;
	}
	public String getMatKhau() {
		return matKhau;
	}
	public void setMatKhau(String matKhau) {
		this.matKhau = matKhau;
	}
	public String getSoLanCamOn() {
		return soLanCamOn;
	}
	public void setSoLanCamOn(String soLanCamOn) {
		this.soLanCamOn = soLanCamOn;
	}
	public String getSoLanCanhBao() {
		return soLanCanhBao;
	}
	public void setSoLanCanhBao(String soLanCanhBao) {
		this.soLanCanhBao = soLanCanhBao;
	}
	public String getTrangThai() {
		return trangThai;
	}
	public void setTrangThai(String trangThai) {
		this.trangThai = trangThai;
	}
	
}
