/**
 * 
 */
package model.bean;

/**				
 * KhuVuc.java			
 *				
 * Version 1.0				
 *				
 * Date: Aug 14, 2015			
 *				
 * Copyright 				
 *				
 * Modification Logs:				
 * DATE                 AUTHOR          DESCRIPTION				
 * -----------------------------------------------------------------------				
 * Aug 14, 2015        HuyNV          Create				
 */

public class KhuVuc {
    private String maKV;
    private String tenKV;
    
    /**
	 * @param maKV
	 * @param tenKV
	 */
    public KhuVuc(){
    	
    }
	public KhuVuc(String maKV, String tenKV) {
		super();
		this.maKV = maKV;
		this.tenKV = tenKV;
	}
	public String getMaKV() {
        return maKV;
    }
    public void setMaKV(String maKV) {
        this.maKV = maKV;
    }
    public String getTenKV() {
        return tenKV;
    }
    public void setTenKV(String tenKV) {
        this.tenKV = tenKV;
    }
    
}
