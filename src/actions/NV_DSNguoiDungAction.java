package actions;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bean.NguoiDung;
import model.bo.NguoiDungBO;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import common.CheckQuyen;

import forms.NV_DSNguoiDungForm;

public class NV_DSNguoiDungAction extends Action {
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		/* check quyen */
		if(CheckQuyen.checkNhanVien(request.getSession().getAttribute("tenDangNhap")+"")==false){
			return mapping.findForward("nvDangNhap");
		}
		NV_DSNguoiDungForm dsNguoiDungForm = (NV_DSNguoiDungForm) form;
		String searchString = dsNguoiDungForm.getSearchString();
		NguoiDungBO ndBO = new NguoiDungBO();
		ArrayList<NguoiDung> nguoiDungList;
		/* lay danh sach nguoi dung tu co so du lieu roi day vao form */
		
		if(searchString==null || "".equals(searchString)){
			nguoiDungList =  ndBO.getNguoiDungArray();
			dsNguoiDungForm.setNguoiDungArray(nguoiDungList);
		} 
		/**
		 * Neu co dua vao chuoi tim kiem, thi loc theo du lieu tim kiem 
		 */
		else{
		    System.out.println(searchString);
			nguoiDungList = ndBO.getNguoiDungArray(searchString);
			dsNguoiDungForm.setNguoiDungArray(nguoiDungList);
		}
		return mapping.findForward("dsNguoiDung");
	}
}
