package actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bean.NguoiDung;
import model.bo.ProcessBO;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import forms.RegisterForm;

public class RegisterAction extends Action{

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		request.setCharacterEncoding("UTF-8");
		RegisterForm f = (RegisterForm) form;
		if(f.getSubmit()==null || "".equals(f.getSubmit())){
			return mapping.findForward("register");
		}
		else{
			if(ProcessBO.isExist(f.getAccount())){
				f.setError("Tài khoản đã tồn tại");
				return mapping.findForward("register");
			}else{
				NguoiDung nd = new NguoiDung();
				nd.setTenDangNhap(f.getAccount());
				nd.setMatKhau(f.getPassword());
				nd.setHoTen(f.getUsername());
				nd.setGioiTinh(f.getGender());
				nd.setNgaySinh(f.getBirthday());
				nd.setSoDienThoai(f.getPhone());
				nd.setEmail(f.getEmail());
				ProcessBO.register(nd);
				return mapping.findForward("success");
			}
		}
	}

}
