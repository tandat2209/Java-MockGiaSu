/**
 * 
 */
package actions;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import common.CheckQuyen;
import common.StringProcess;
import forms.ND_ChiTietBaiDangForm;
import model.bean.BaiDang;
import model.bo.BaiDangBO;
import model.bo.KhuVucBO;

/**
 * SuaBaiDangGiaSuAction.java
 *
 * Version 1.0
 *
 * Date: Aug 17, 2015
 *
 * Copyright
 *
 * Modification Logs: DATE AUTHOR DESCRIPTION
 * -----------------------------------------------------------------------
 * 8:48:35 PM HuyNV Create
 */
public class ND_SuaBaiDangGiaSuAction extends Action {
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.struts.action.Action#execute(org.apache.struts.action.
	 * ActionMapping, org.apache.struts.action.ActionForm,
	 * javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		if(CheckQuyen.checkNguoiDung(request.getSession().getAttribute("tenDangNhap")+"")==false){
			return mapping.findForward("ndDangNhap");
		}
		ND_ChiTietBaiDangForm f = (ND_ChiTietBaiDangForm) form;
		StringProcess v = new StringProcess();
		request.setCharacterEncoding("UTF-8");
		BaiDangBO bdBO = new BaiDangBO();
		KhuVucBO kvBO = new KhuVucBO();
		String maBD = f.getMaBD();
		if(f.getMaBD()==null || f.getMaBD().isEmpty()) 
	        return mapping.findForward("trangloi");
		if ("Cập nhật".equals(f.getSubmit())) {
			ActionErrors error = new ActionErrors();

			// validate Há»� tÃªn
			if (v.isEmptyString(f.getTenGiaSu())) {
				error.add("errHoTen", new ActionMessage("error.hoTenTrong"));
			}

			// validate giá»›i tÃ­nh
			if (f.getGioiTinh() == null || "".equals(f.getGioiTinh())) {
				error.add("errGioiTinh", new ActionMessage(
						"error.gioiTinhError"));
			}
			// validate sá»‘ Ä‘iá»‡n thoáº¡i
			if (v.isEmptyString(f.getSoDienThoai())) {
				error.add("errPhone", new ActionMessage("error.phoneTrong"));
			} else if (!v.isValidPhone(f.getSoDienThoai())) {
				error.add("errPhone", new ActionMessage("error.phoneError"));
			}
			// validate ngÃ y sinh
			if (v.isEmptyString(f.getNgaySinh())) {
				error.add("errNgaySinh", new ActionMessage(
						"error.ngaySinhTrong"));
			} else if (!v.isValidDate(f.getNgaySinh())) {
				error.add("errNgaySinh", new ActionMessage(
						"error.birthdayError"));
			}
			// validate tiÃªu Ä‘á»�
			if (v.isEmptyString(f.getTieuDe())) {
				error.add("errTieuDe", new ActionMessage("error.tieuDeTrong"));
			} else if (f.getTieuDe().trim().split(" ").length >= 50) {
				error.add("errTieuDe", new ActionMessage("error.tieuDeError"));
			}
			// validate khu vá»±c
			if (f.getMaKvArr() == null) {
				error.add("errKhuVuc", new ActionMessage("error.khuVucError"));
			}
			// validdate MÃ´n
			if (v.isEmptyString(f.getMon())) {
				error.add("errMon", new ActionMessage("error.monError"));
			}
			// validate Lá»›p
			if (v.isEmptyString(f.getLop())) {
				error.add("errLop", new ActionMessage("error.monLop"));
			}
			// validate Luong
			if (!StringProcess.isEmptyString(f.getLuongGS())) {
				if (StringProcess.isEmptyNumber(f.getLuongGS())) {
					error.add("errLuong", new ActionMessage("error.luong"));
				}
			}
			if (error.size() != 0) {
				saveErrors(request, error);
				f.setKvList(kvBO.getKVList());
				return mapping.findForward("suaBDGiaSu");
			} else {
				BaiDang bd = new BaiDang();
				bd.setMaBD(f.getMaBD());
				System.out.println(f.getMaBD());
				bd.setTieuDe(f.getTieuDe());
				bd.setMon(f.getMon());
				bd.setLop(f.getLop());
				bd.setThoiGianDay(f.getThoiGianDay());
				bd.setSoDienThoai(f.getSoDienThoai());
				bd.setMoTaThem(f.getMoTaThem());
				bd.setMaNguoiDang(f.getMaNguoiDang());
				bd.setThoiGianDang(f.getThoiGianDang());
				bd.setTrangThai(f.getTrangThai());
				bd.setTenGiaSu(f.getTenGiaSu());
				bd.setGioiTinh(f.getGioiTinh());
				bd.setNoiCongTac(f.getNoiCongTac());
				bd.setNgheNghiep(f.getNgheNghiep());
				bd.setNgaySinh(f.getNgaySinh());
				bd.setQueQuan(f.getQueQuan());
				bd.setLuongGS(f.getLuongGS());
				ArrayList<String> temparr = new ArrayList<String>();
				for (int i = 0; i < f.getMaKvArr().length; i++) {
					System.out.println(i);
					temparr.add(f.getMaKvArr()[i]);
				}
				bdBO.updateBaiDang(bd);
				kvBO.updateKvBaiDang(temparr, bd.getMaBD());
				return mapping.findForward("chitietBDGiaSu");
			}
		} else {
			BaiDang bd = bdBO.getBaiDang(maBD);
			if(bd == null || bd.getMaBD() == null || bd.getMaBD().isEmpty())
                return mapping.findForward("trangloi");
			f.setMaBD(bd.getMaBD());
			f.setTieuDe(bd.getTieuDe());
			f.setMon(bd.getMon());
			f.setLop(bd.getLop());
			f.setThoiGianDay(bd.getThoiGianDay());
			f.setSoDienThoai(bd.getSoDienThoai());
			f.setMoTaThem(bd.getMoTaThem());
			f.setMaNguoiDang(bd.getMaNguoiDang());
			f.setThoiGianDang(bd.getThoiGianDang());
			f.setTrangThai(bd.getTrangThai());
			f.setTenGiaSu(bd.getTenGiaSu());
			f.setGioiTinh(bd.getGioiTinh());
			f.setNoiCongTac(bd.getNoiCongTac());
			f.setNgheNghiep(bd.getNgheNghiep());
			f.setNgaySinh(bd.getNgaySinh());
			f.setQueQuan(bd.getQueQuan());
			f.setLuongGS(bd.getLuongGS());
			ArrayList<String> templist = kvBO.maLvList(maBD);
			String[] temparr = new String[templist.size()];
			System.out.println("size arr: " + temparr.length);
			for (int i = 0; i < temparr.length; i++) {
				temparr[i] = templist.get(i);
			}
			f.setMaKvArr(temparr);
			// dua danh sach khu vuc vao de gia su chon
			f.setKvList(kvBO.getKVList());
			return mapping.findForward("suaBDGiaSu");
		}
	}
}
