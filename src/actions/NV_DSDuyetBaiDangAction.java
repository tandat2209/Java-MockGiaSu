package actions;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.bean.BaiDang;
import model.bo.BaiDangBO;



import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import common.CheckQuyen;

import forms.NV_DSDuyetBaiDangForm;

public class NV_DSDuyetBaiDangAction extends Action {
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		HttpSession session = request.getSession();
		NV_DSDuyetBaiDangForm nvduyetbaiForm = (NV_DSDuyetBaiDangForm) form;
		String tenDangNhap = (String) session.getAttribute("tenDangNhap");
		if(CheckQuyen.checkNhanVien(tenDangNhap)== false){
			return mapping.findForward("nvDangNhap");
		}
		nvduyetbaiForm.setTenDangNhap(tenDangNhap);
		BaiDangBO bdBO = new BaiDangBO();
		String loai = nvduyetbaiForm.getLoai();
		String trangthai = nvduyetbaiForm.getTrangThai();
		
		/* lay tat ca bai dang tu database */
		ArrayList<BaiDang> allBD = bdBO.getBaiDangList();
		/**
		 * loai: táº¥t cáº£ 
		 * trangthai: táº¥t cáº£
		 */
		if (loai == null && trangthai == null || "Tất cả".equals(loai) && "Tất cả".equalsIgnoreCase(trangthai)) {
			nvduyetbaiForm.setBaiDangList(allBD);
		}

		/**
		 * chon theo loai
		 */
		else if (loai != null && (trangthai == null || "Tất cả".equalsIgnoreCase(trangthai))) {
			int size = allBD.size();
			ArrayList<BaiDang> temp = new ArrayList<BaiDang>();
			for (int i = 0; i < size; i++) {
				if (loai.equals(allBD.get(i).getLoaiBD())) {
					temp.add(allBD.get(i));
				}
			}
			nvduyetbaiForm.setBaiDangList(temp);
		}
		/**
		 * chon theo trangthai
		 */
		else if ((loai == null || "Tất cả".equals(loai)) && trangthai != null) {
			int size = allBD.size();
			ArrayList<BaiDang> temp = new ArrayList<BaiDang>();
			for (int i = 0; i < size; i++) {
				if (trangthai.equalsIgnoreCase(allBD.get(i).getTrangThai())) {
					temp.add(allBD.get(i));
				}
			}
			nvduyetbaiForm.setBaiDangList(temp);
		}
		/**
		 * chon loai,  chon trangthai
		 */
		else {
			int size = allBD.size();
			ArrayList<BaiDang> temp = new ArrayList<BaiDang>();
			for (int i = 0; i < size; i++) {
				if (trangthai.equalsIgnoreCase(allBD.get(i).getTrangThai())
						&& loai.equals(allBD.get(i).getLoaiBD())) {
					temp.add(allBD.get(i));
				}
			}
			nvduyetbaiForm.setBaiDangList(temp);
		}
		return mapping.findForward("dsDuyetBaiDang");
	}
}
