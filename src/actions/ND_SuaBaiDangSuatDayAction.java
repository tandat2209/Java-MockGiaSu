/**
 * 
 */
package actions;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import common.CheckQuyen;
import common.StringProcess;
import forms.ND_ChiTietBaiDangForm;
import model.bean.BaiDang;
import model.bo.BaiDangBO;
import model.bo.KhuVucBO;

/**
 * ND_SuaBaiDangSuatDayAction.java
 *
 * Version 1.0
 *
 * Date: Aug 17, 2015
 *
 * Copyright 
 *
 * Modification Logs:
 * DATE                 AUTHOR          DESCRIPTION
 * -----------------------------------------------------------------------
 * 11:29:47 PM        	HuyNV          Create
 */
public class ND_SuaBaiDangSuatDayAction extends Action {
/* (non-Javadoc)
 * @see org.apache.struts.action.Action#execute(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
 */
@Override
public ActionForward execute(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response)
				throws Exception {
	// TODO Auto-generated method stub
	if(CheckQuyen.checkNguoiDung(request.getSession().getAttribute("tenDangNhap")+"")==false){
		return mapping.findForward("ndDangNhap");
	}
	request.setCharacterEncoding("UTF-8");
	ND_ChiTietBaiDangForm f = (ND_ChiTietBaiDangForm) form;
	KhuVucBO kvBo = new KhuVucBO();
	BaiDangBO bdBo = new BaiDangBO();
	String maBD = f.getMaBD();
	if(f.getMaBD()==null || f.getMaBD().isEmpty()) 
        return mapping.findForward("trangloi");
	if("Cập nhật".equals(f.getSubmit())){
		ActionErrors error = new ActionErrors();
		//validate Há»� tÃªn
		if(StringProcess.isEmptyString(f.getTenPhuHuynh())){
			error.add("errHoTen", new ActionMessage("error.hoTenTrong"));
		}
		//validate sá»‘ Ä‘iá»‡n thoáº¡i
		if(StringProcess.isEmptyString(f.getSoDienThoai())){
			error.add("errPhone", new ActionMessage("error.phoneTrong"));
		} else if(!StringProcess.isValidPhone(f.getSoDienThoai())){
			error.add("errPhone", new ActionMessage("error.phoneError"));
		}
		//validate tiÃªu Ä‘á»�
		if(StringProcess.isEmptyString(f.getTieuDe())){
			error.add("errTieuDe", new ActionMessage("error.tieuDeTrong"));
		}else if(f.getTieuDe().trim().split(" ").length>=50){
			error.add("errTieuDe", new ActionMessage("error.tieuDeError"));
		}
		//validate Ä‘á»‹a chá»‰
		if(StringProcess.isEmptyString(f.getDiaChi())){
			error.add("errDiaChi", new ActionMessage("error.diachiError"));
		}
		//validate MÃ´n
		if (StringProcess.isEmptyString(f.getMon())) {
			error.add("errMon", new ActionMessage("error.monError"));
		}
		//validate Lá»›p
		if(StringProcess.isEmptyString(f.getLop())){
			error.add("errLop", new ActionMessage("error.monLop"));
		}
		//validate luong
		if (!StringProcess.isEmptyString(f.getLuongSD())) {
			if (StringProcess.isEmptyNumber(f.getLuongSD())) {
				error.add("errLuong", new ActionMessage("error.luong"));
			}
		}
		if(error.size()!=0){
			saveErrors(request, error);
			System.out.println(f.getDiaChi());
			f.setKvList(kvBo.getKVList());
			return mapping.findForward("suaBDSuatDay");
		} else {
		BaiDang bd = new BaiDang();
		bd.setMaBD(f.getMaBD());
		bd.setTieuDe(f.getTieuDe());
		bd.setMon(f.getMon());
		bd.setLop(f.getLop());
		bd.setThoiGianDay(f.getThoiGianDay());
		bd.setSoDienThoai(f.getSoDienThoai());
		bd.setMoTaThem(f.getMoTaThem());
		bd.setGioiTinh(f.getGioiTinh());
		bd.setTenPhuHuynh(f.getTenPhuHuynh());
		bd.setDiaChi(f.getDiaChi());
		bd.setSoBuoiTrenTuan(f.getSoBuoiTrenTuan());
		bd.setSoLuongHocSinh(f.getSoLuongHocSinh());
		bd.setLuongSD(f.getLuongSD());
		ArrayList<String> kvListBD = new ArrayList<String>();
		kvListBD.add(f.getKhuVucUpdateSD());
		kvBo.updateKvBaiDang(kvListBD, bd.getMaBD());
		bdBo.updateBaiDang(bd);
		return mapping.findForward("chitiet");
		}
	}else {
		//lay du lieu bai dang tu DB
		BaiDang bd = bdBo.getBaiDang(maBD);
		if(bd == null || bd.getMaBD() == null || bd.getMaBD().isEmpty())
            return mapping.findForward("trangloi");
		ArrayList<String> maKvList = kvBo.maLvList(maBD);
		f.setKvList(kvBo.getKVList());
		
		//set vao Form
		f.setMaBD(bd.getMaBD());
		f.setTieuDe(bd.getTieuDe());
		f.setLoaiBD(bd.getLoaiBD());
		f.setMon(bd.getMon());
		f.setLop(bd.getLop());
		f.setThoiGianDay(bd.getThoiGianDay());
		f.setSoDienThoai(bd.getSoDienThoai());
		f.setMoTaThem(bd.getMoTaThem());
		f.setGioiTinh(bd.getGioiTinh());
		f.setTenPhuHuynh(bd.getTenPhuHuynh());
		f.setDiaChi(bd.getDiaChi());
		f.setSoBuoiTrenTuan(bd.getSoBuoiTrenTuan());
		f.setSoLuongHocSinh(bd.getSoLuongHocSinh());
		f.setLuongSD(bd.getLuongSD());
		String KvSD = maKvList.get(0);
		f.setKhuVucUpdateSD(KvSD);
		System.out.println(KvSD);
		
		return mapping.findForward("suaBDSuatDay");
	}
	
}
}
