package actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.bean.NhanVien;
import model.bo.NhanVienBO;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import common.StringProcess;
import forms.NV_DangNhapForm;

public class NV_DangNhapAction extends Action {
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        
        NV_DangNhapForm dnForm = (NV_DangNhapForm) form;
        if (dnForm.getSubmit() == null || "".equals(dnForm.getSubmit())) {
            return mapping.findForward("nvDangNhap");
        } else {
            ActionErrors errors = new ActionErrors();
            if (dnForm.getTenDangNhap() == null
                    || dnForm.getTenDangNhap().trim().length() == 0) {
                errors.add("tenDangNhapError", new ActionMessage(
                        "error.tenDangNhap"));
            }
            if (dnForm.getMatKhau() == null
                    || dnForm.getMatKhau().trim().length() == 0) {
                errors.add("matKhauError", new ActionMessage("error.matKhau"));
            }
            if (errors.size() != 0) {
                saveErrors(request, errors);
                return mapping.findForward("nvDangNhap");
            } else {
                /* lay tenDangNhap va matkhau tu form */
                String tenDangNhap = dnForm.getTenDangNhap();
                String matkhau = dnForm.getMatKhau();

                /* lay nhanvien theo tenDN va matkhau nguoi dung nhap vao */
                NhanVienBO nvBO = new NhanVienBO();
                NhanVien nhanvien = nvBO.getNV(tenDangNhap);
                System.out.println(StringProcess.encryptMD5(matkhau));
                System.out.println(nhanvien.getMatKhau());
                if (tenDangNhap.equals(nhanvien.getTenDangNhap())
                        && StringProcess.encryptMD5(matkhau).equals(nhanvien.getMatKhau())) {

                    /* tenDN va MK dung --> dua vao session tenDN va quyen */
                    HttpSession session = request.getSession();
                    session.setAttribute("tenDangNhap", tenDangNhap);
                    if ("0".equals(nhanvien.getQuyen())) {
                        return mapping.findForward("dsDuyetBaiDang");
                    } else {
                        return mapping.findForward("dsNhanVien");
                    }

                }else {
                    dnForm.setError("Tên đăng nhập hoặc mật khẩu sai!");
                    return mapping.findForward("nvDangNhap");
                }
            }
        }
    }
}
