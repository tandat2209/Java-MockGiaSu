/**
 * 
 */
package actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.bean.NguoiDung;
import model.bo.NguoiDungBO;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import common.CheckQuyen;
import common.StringProcess;
import forms.ND_DanNhapForm;

/**
 * ND_DangNhap.java
 * 
 * Version 1.0
 * 
 * Date: Aug 13, 2015
 * 
 * Copyright
 * 
 * Modification Logs: DATE AUTHOR DESCRIPTION
 * ----------------------------------------------------------------------- Aug
 * 13, 2015 HuyNV Create
 */

public class ND_DangNhapAction extends Action {
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.struts.action.Action#execute(org.apache.struts.action.
	 * ActionMapping, org.apache.struts.action.ActionForm,
	 * javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
					throws Exception {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		ND_DanNhapForm nd_DangNhap = (ND_DanNhapForm) form;
		NguoiDungBO ndBO = new NguoiDungBO();
		System.out.println(nd_DangNhap.getSubmit());
		// nếu submid bang dang nhap
		if ("Đăng nhập".equals(nd_DangNhap.getSubmit())) {
			nd_DangNhap.setSubmit("");
			ActionErrors errors = new ActionErrors();
			if(StringProcess.isEmptyString(nd_DangNhap.getTenDangNhap())){
				errors.add("tenDangNhapError", new ActionMessage("error.tenDangNhap"));
			}
			if(StringProcess.isEmptyString(nd_DangNhap.getPassWord())){
				errors.add("matKhauError", new ActionMessage("error.matKhau"));
			}
			if(errors.size()!=0){
				saveErrors(request, errors);
				return mapping.findForward("ndDangNhap");
			} else {
			
			/* lay tenDangNhap va matkhau tu form */
			String tenDangNhap = nd_DangNhap.getTenDangNhap();
			String matkhau = StringProcess.encryptMD5(nd_DangNhap.getPassWord());
			
			/* lay nhanvien theo tenDN va matkhau nguoi dung nhap vao */
			NguoiDung nd = ndBO.getThongTinNguoiDung(tenDangNhap);
			
			// neu ten dang nhap va mat khau dung
			if (tenDangNhap.equals(nd.getTenDangNhap())
					&& matkhau.equals(nd.getMatKhau())) {
				if("Bị chặn".equals(nd.getTrangThai())){
					nd_DangNhap.setSubmit("");
					return mapping.findForward("blockND");
				}else{
				/* tenDN va MK dung --> dua vao session tenDN va quyen */
				HttpSession session = request.getSession();
				session.setAttribute("tenDangNhap", tenDangNhap);
				nd_DangNhap.setLogined(true);
				//vao trang thong tin ca nhan
				return mapping.findForward("ndthongtincanhan");
				}
			} else {
				nd_DangNhap.setErrorMS("Tài khoản hoặc mật khẩu sai!");
				return mapping.findForward("ndDangNhap");
			}
			}
		}else {
		 return mapping.findForward("ndDangNhap");
		 }

	}
}
