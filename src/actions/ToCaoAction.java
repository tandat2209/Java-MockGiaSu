package actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bean.ThongBao;
import model.bo.ProcessBO;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import forms.ND_ChiTietBaiDangForm;
import forms.ToCaoForm;

public class ToCaoAction extends Action {

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
					throws Exception {
		request.setCharacterEncoding("UTF-8");
		ND_ChiTietBaiDangForm f = (ND_ChiTietBaiDangForm) form;
		ThongBao tb = new ThongBao();
		tb.setMaBaiDang(f.getMaBD());
		tb.setEmail(f.getEmail());
		tb.setSoDienThoai(f.getPhone());
		tb.setNoiDung(f.getContent());
		ProcessBO.insertAccnouncement(tb);
		System.out.println("thanh cong");
		return mapping.findForward("success");
	}

}
