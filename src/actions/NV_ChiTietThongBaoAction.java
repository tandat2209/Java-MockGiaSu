package actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bo.ThongBaoBO;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import common.CheckQuyen;

import forms.ThongBaoForm;

/**
 * @author DatNVT
 *
 */
public class NV_ChiTietThongBaoAction extends Action {
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
    	/* check quyen */
		if(CheckQuyen.checkNhanVien(request.getSession().getAttribute("tenDangNhap")+"")==false){
			return mapping.findForward("nvDangNhap");
		}
        ThongBaoForm tbForm = (ThongBaoForm) form;
        String maTB = tbForm.getMaTB();
        ThongBaoBO tbBO = new ThongBaoBO();
        tbForm.setThongBao(tbBO.getThongBaoByMaTB(maTB));
        return mapping.findForward("chitietTB");
    }
}
