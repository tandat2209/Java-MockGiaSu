/**
 * 
 */
package actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.bean.NguoiDung;
import model.bo.NguoiDungBO;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import common.CheckQuyen;

import forms.ND_ThongTinCaNhanForm;

/**
 * ND_ThongTinCaNhanAction.java
 * 
 * Version 1.0
 * 
 * Date: Aug 13, 2015
 * 
 * Copyright
 * 
 * Modification Logs: DATE AUTHOR DESCRIPTION
 * ----------------------------------------------------------------------- Aug
 * 13, 2015 HuyNV Create
 */

public class ND_ThongTinCaNhanAction extends Action {
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.struts.action.Action#execute(org.apache.struts.action.
	 * ActionMapping, org.apache.struts.action.ActionForm,
	 * javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		// TODO Auto-generated method stub
		if(CheckQuyen.checkNguoiDung(request.getSession().getAttribute("tenDangNhap")+"")==false){
			return mapping.findForward("ndDangNhap");
		}
		request.setCharacterEncoding("UTF-8");
		ND_ThongTinCaNhanForm nd_ThongTin = (ND_ThongTinCaNhanForm) form;
		/* lay tenDangNhap, quyen tu session */
		HttpSession session = request.getSession();
		String tenDangNhap = (String) session.getAttribute("tenDangNhap");
		NguoiDung nd = new NguoiDungBO().getThongTinNguoiDung(tenDangNhap);
		nd_ThongTin.setTenDangNhap(nd.getTenDangNhap());
		nd_ThongTin.setTenND(nd.getHoTen());
		nd_ThongTin.setGioiTinh(nd.getGioiTinh());
		nd_ThongTin.setNgaySinh(nd.getNgaySinh());
		nd_ThongTin.setEmail(nd.getEmail());
		nd_ThongTin.setSoDienThoai(nd.getSoDienThoai());
		nd_ThongTin.setMatKhau(nd.getMatKhau());
		nd_ThongTin.setAnhDaiDien(nd.getAnhDaiDien());
		return mapping.findForward("ndThongTinCaNhan");
	}
}
