package actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bean.NhanVien;
import model.bo.NhanVienBO;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import common.CheckQuyen;

import forms.NhanVienForm;

public class NV_CapNhatThongTinAction extends Action {
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		/* check quyen */
		if(CheckQuyen.checkNhanVien(request.getSession().getAttribute("tenDangNhap")+"")==false){
			return mapping.findForward("nvDangNhap");
		}
		NhanVienForm nvForm = (NhanVienForm) form;
		NhanVienBO nvBO = new NhanVienBO();
		String tenDangNhap = nvForm.getTenDangNhap();
		
		
		String submit = nvForm.getSubmit();
		
		// neu submit chua co du lieu, day toi jsp cap nhat du lieu
		if(submit==null || "".equals(submit)){
			NhanVien nhanvien = nvBO.getNV(tenDangNhap);
			nvForm.setTenDangNhap(nhanvien.getTenDangNhap());
			nvForm.setTenNV(nhanvien.getTenNV());
			nvForm.setNgaySinh(nhanvien.getNgaySinh());
			nvForm.setGioiTinh(nhanvien.getGioiTinh());
			nvForm.setDiaChi(nhanvien.getDiaChi());
			nvForm.setSoDienThoai(nhanvien.getSoDienThoai());
			nvForm.setEmail(nhanvien.getEmail());
			return mapping.findForward("capnhatthongtin");
		}
		
		// neu submit da co du lieu, tuc nguoi dung dap nhat thong tin
		// luu thong tin da nhap vao database
		// roi chuyen toi trang action xemthongtincanhan
		else{
			nvForm.setSubmit("");
			NhanVien nhanvien = new NhanVien();
			nhanvien.setTenDangNhap(nvForm.getTenDangNhap());
			nhanvien.setTenNV(nvForm.getTenNV());
			nhanvien.setNgaySinh(nvForm.getNgaySinh());
			nhanvien.setGioiTinh(nvForm.getGioiTinh());
			nhanvien.setDiaChi(nvForm.getDiaChi());
			nhanvien.setSoDienThoai(nvForm.getSoDienThoai());
			nhanvien.setEmail(nvForm.getEmail());
			nvBO.capNhatThongTinNV(nhanvien);
			return mapping.findForward("xemthongtin");
		}
	}
}
