package actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import common.StringProcess;
import forms.Admin_NhanVienForm;
import model.bean.NhanVien;
import model.bo.NhanVienBO;

public class Admin_ThemNhanVienAction extends Action {
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
					throws Exception {
		request.setCharacterEncoding("UTF-8");
		Admin_NhanVienForm f = (Admin_NhanVienForm) form;
		
		if ("Thêm".equals(f.getSubmitThem())) {
			ActionErrors errors = new ActionErrors();

			// Validate tÃƒÂªn Ã„â€˜Ã„Æ’ng nhÃ¡ÂºÂ­p
			if (StringProcess.isEmptyString(f.getTenDangNhap())
					|| StringProcess.hasSpaceInString(f.getTenDangNhap())) {
				errors.add("tenDangNhapError",
						new ActionMessage("error.tenDangNhap_02"));

			}

			// Validate mÃ¡ÂºÂ­t khÃ¡ÂºÂ©u
			if (StringProcess.isEmptyString(f.getMatKhau())
					|| !StringProcess.has6Character(f.getMatKhau())) {
				errors.add("matKhauError",
						new ActionMessage("error.matKhau_02"));
			}else if(!f.getMatKhau().equals(f.getMatKhauXN())){
				errors.add("matKhauXNError",
						new ActionMessage("error.matKhauXN"));
			}

			// Validate tÃƒÂªn nhÃƒÂ¢n viÃƒÂªn
			if (StringProcess.isEmptyString(f.getTenNV())) {
				errors.add("tenNVError", new ActionMessage("error.tenNV_01"));
			}
			System.out.println(f.getNgaySinh());
			
			// Validate ngÃƒÂ y sinh
			if (StringProcess.isEmptyString(f.getNgaySinh())) {
				errors.add("ngaySinhError",new ActionMessage("error.ngaySinh_01"));
			}else if(!StringProcess.isValidDate(f.getNgaySinh())){
				errors.add("ngaySinhError",new ActionMessage("error.ngaySinh_02"));
			}

			// Validate email
			if (StringProcess.isEmptyString(f.getEmail())
					|| !StringProcess.CheckEmail(f.getEmail())) {
				errors.add("emailError", new ActionMessage("error.email_01"));
			}

			// Validate sÃ¡Â»â€˜ Ã„â€˜iÃ¡Â»â€¡n thoÃ¡ÂºÂ¡i
			if (StringProcess.isEmptyString(f.getSoDienThoai())
					|| !StringProcess.isValidPhone(f.getSoDienThoai())) {
				errors.add("soDienThoaiError",
						new ActionMessage("error.soDienThoai_01"));
			}

			if (errors.size() != 0) {
				saveErrors(request, errors);
				return mapping.findForward("admin_themnhanvien");
			} else {
				NhanVienBO nvBo = new NhanVienBO();
				NhanVien nv = new NhanVien();
				nv.setTenDangNhap(f.getTenDangNhap());
				nv.setMatKhau(StringProcess.encryptMD5(f.getMatKhau()));
				nv.setDiaChi(f.getDiaChi());
				nv.setEmail(f.getEmail());
				nv.setGioiTinh(f.getGioiTinh());
				nv.setNgaySinh(f.getNgaySinh());
				nv.setSoDienThoai(f.getSoDienThoai());
				nv.setTenNV(f.getTenNV());
				try {
					f.setErrTenDangNhap("");
					nvBo.themNhanVien(nv);
				} catch (Exception e) {

					f.setErrTenDangNhap("Tên đăng nhập đã tồn tại!");
					return mapping.findForward("admin_themnhanvien");
				}
				return mapping.findForward("admin_dsnhanvien");
			}

		}
		f.setGioiTinh("Nam");
		return mapping.findForward("admin_themnhanvien");
	}
}
