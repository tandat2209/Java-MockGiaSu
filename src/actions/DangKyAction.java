/**
 * 
 */
package actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import common.StringProcess;
import forms.DangKyForm;
import model.bean.NguoiDung;
import model.bo.ProcessBO;

/**
 * DangKyAction.java
 *
 * Version 1.0
 *
 * Date: Aug 23, 2015
 *
 * Copyright
 *
 * Modification Logs: DATE AUTHOR DESCRIPTION
 * -----------------------------------------------------------------------
 * 3:13:23 PM HuyNV Create
 */
public class DangKyAction extends Action {
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.struts.action.Action#execute(org.apache.struts.action.
	 * ActionMapping, org.apache.struts.action.ActionForm,
	 * javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
					throws Exception {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		DangKyForm f = (DangKyForm) form;
		System.out.println("Here"+f.getSubmit());
		if("Đăng ký".equals(f.getSubmit())){
			ActionErrors error = new ActionErrors();
			
			//validate ten dang nhap
			if(StringProcess.isEmptyString(f.getTenDangNhap())){
				error.add("errTenDangNhap", new ActionMessage("error.tenDangNhap"));
			}else if(ProcessBO.isExist(f.getTenDangNhap())){
				error.add("errTenDangNhap", new ActionMessage("error.tenDangNhapTT"));
			
			}
			//validate mat khau
			if (StringProcess.isEmptyString(f.getMatKhau())) {
				error.add("errMatKhau", new ActionMessage("error.matKhau"));
			} else if(f.getMatKhau().length()<6){
				error.add("errMatKhau", new ActionMessage("error.matKhau_03"));
			} else if(!f.getMatKhau().equals(f.getPwd1())){
				error.add("errMatKhauXN", new ActionMessage("error.matKhauXN"));
			}
			//validate ho ten
			if(StringProcess.isEmptyString(f.getTenND())){
				error.add("errHoTen", new ActionMessage("error.hoTenTrong"));
			}
			//validate so dien thoai
			if(StringProcess.isEmptyString(f.getSoDienThoai())){
				error.add("errPhone", new ActionMessage("error.phoneTrong"));
			} else if(!StringProcess.isValidPhone(f.getSoDienThoai())){
				error.add("errPhone", new ActionMessage("error.phoneError"));
			}
			//validate ngay sinh
			if(StringProcess.isEmptyString(f.getNgaySinh())){
				error.add("errNgaySinh", new ActionMessage("error.ngaySinhTrong"));
			} else if(!StringProcess.isValidDate(f.getNgaySinh())){
				error.add("errNgaySinh", new ActionMessage("error.birthdayError"));
			}
			//validate email
			if(StringProcess.isEmptyString(f.getEmail())){
				error.add("errEmail", new ActionMessage("error.errorTrong"));
			} else if(!StringProcess.CheckEmail(f.getEmail())){
				error.add("errEmail", new ActionMessage("error.emailError"));
			}
			if(error.size()!=0){
				saveErrors(request, error);
				return mapping.findForward("dangky");
			}else {
				NguoiDung nd = new NguoiDung();
				nd.setTenDangNhap(f.getTenDangNhap());
				nd.setMatKhau(StringProcess.encryptMD5(f.getMatKhau()));
				nd.setHoTen(f.getTenND());
				nd.setGioiTinh(f.getGioiTinh());
				nd.setNgaySinh(f.getNgaySinh());
				nd.setSoDienThoai(f.getSoDienThoai());
				nd.setEmail(f.getEmail());
				ProcessBO.register(nd);
				return mapping.findForward("dangnhap");
			}
		}else{
			return mapping.findForward("dangky");
		}
	}
	
}
