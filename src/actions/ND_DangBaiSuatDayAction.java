/**
 * 
 */
package actions;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.bean.BaiDang;
import model.bean.Key;
import model.bo.BaiDangBO;
import model.bo.KhuVucBO;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import common.CheckQuyen;
import common.StringProcess;
import forms.ND_ChiTietBaiDangForm;

/**
 * ND_DangBaiSuatDayAction.java
 *
 * Version 1.0
 *
 * Date: Aug 18, 2015
 *
 * Copyright
 *
 * Modification Logs: DATE AUTHOR DESCRIPTION
 * ----------------------------------------------------------------------- Aug
 * 18, 2015 HuyNV Create
 */

public class ND_DangBaiSuatDayAction extends Action {
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.struts.action.Action#execute(org.apache.struts.action.
	 * ActionMapping, org.apache.struts.action.ActionForm,
	 * javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		request.setCharacterEncoding("UTF-8");
		if(CheckQuyen.checkNguoiDung(request.getSession().getAttribute("tenDangNhap")+"")==false){
			return mapping.findForward("ndDangNhap");
		}
		ND_ChiTietBaiDangForm f = (ND_ChiTietBaiDangForm) form;
		StringProcess v = new StringProcess();
		// get tenDangNhap tu session
		HttpSession session = request.getSession();
		String tenDangNhap = (String) session.getAttribute("tenDangNhap");
		f.setMaNguoiDang(tenDangNhap);

		BaiDangBO bdBo = new BaiDangBO();
		KhuVucBO kvBo = new KhuVucBO();
		System.out.println("submit:" + f.getSubmit());
		System.out.println("Đăng bài".equals(f.getSubmit()));
		if ("Đăng bài".equals(f.getSubmit())) {
			ActionErrors error = new ActionErrors();
			System.out.println(f.getKhuVucUpdateSD());
			// validate Họ tên
			if (v.isEmptyString(f.getTenPhuHuynh())) {
				error.add("errHoTen", new ActionMessage("error.hoTenTrong"));
			}
			// validate số điện thoại
			if (v.isEmptyString(f.getSoDienThoai())) {
				error.add("errPhone", new ActionMessage("error.phoneTrong"));
			} else if (!v.isValidPhone(f.getSoDienThoai())) {
				error.add("errPhone", new ActionMessage("error.phoneError"));
			}
			// validate tiêu đề
			if (v.isEmptyString(f.getTieuDe())) {
				error.add("errTieuDe", new ActionMessage("error.tieuDeTrong"));
			} else if (f.getTieuDe().trim().split(" ").length >= 50) {
				error.add("errTieuDe", new ActionMessage("error.tieuDeError"));
			}
			// validate địa chỉ
			if (v.isEmptyString(f.getDiaChi())) {
				error.add("errDiaChi", new ActionMessage("error.diachiError"));
			}
			// validate Môn
			if (v.isEmptyString(f.getMon())) {
				error.add("errMon", new ActionMessage("error.monError"));
			}
			// validate Lớp
			if (v.isEmptyString(f.getLop())) {
				error.add("errLop", new ActionMessage("error.monLop"));
			}
			// validate Luong
			if (!StringProcess.isEmptyString(f.getLuongSD())) {
				if (StringProcess.isEmptyNumber(f.getLuongSD())) {
					error.add("errLuong", new ActionMessage("error.luong"));
				}
			}
			if (error.size() != 0) {
				System.out.println("errrrr");
				saveErrors(request, error);
				f.setKvList(kvBo.getKVList());
				return mapping.findForward("dangsuatday");
			} else {
				System.out.println("else");
				BaiDang bd = new BaiDang();
				String maBD = "BD" + Key.getKey();
				bd.setMaBD(maBD);
				f.setMaBD(maBD);
				bd.setMaNguoiDang(f.getMaNguoiDang());
				bd.setTieuDe(f.getTieuDe());
				bd.setLoaiBD("Suất dạy");
				bd.setMon(f.getMon());
				bd.setLop(f.getLop());
				bd.setThoiGianDay(f.getThoiGianDay());
				bd.setSoDienThoai(f.getSoDienThoai());
				bd.setMoTaThem(f.getMoTaThem());
				bd.setThoiGianDang(f.getThoiGianDang());
				bd.setGioiTinh(f.getGioiTinh());
				bd.setTenPhuHuynh(f.getTenPhuHuynh());
				bd.setDiaChi(f.getDiaChi());
				bd.setSoBuoiTrenTuan(f.getSoBuoiTrenTuan());
				bd.setSoLuongHocSinh(f.getSoLuongHocSinh());
				bd.setLuongSD(f.getLuongSD());
				ArrayList<String> kvListBD = new ArrayList<String>();
				kvListBD.add(f.getKhuVucUpdateSD());
				bdBo.insertBaiDang(bd);
				kvBo.updateKvBaiDang(kvListBD, bd.getMaBD());
				return mapping.findForward("lichsu");
			}
		} else {
			f.setKvList(kvBo.getKVList());
			return mapping.findForward("dangsuatday");
		}
	}
}
