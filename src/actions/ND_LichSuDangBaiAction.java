/**
 * 
 */
package actions;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.bean.BaiDang;
import model.bo.BaiDangBO;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import common.CheckQuyen;

import forms.ND_LichSuDangBaiForm;

/**				
 * ND_LichSuDangBaiAction.java			
 *				
 * Version 1.0				
 *				
 * Date: Aug 14, 2015			
 *				
 * Copyright 				
 *				
 * Modification Logs:				
 * DATE                 AUTHOR          DESCRIPTION				
 * -----------------------------------------------------------------------				
 * Aug 14, 2015        HuyNV          Create				
 */

public class ND_LichSuDangBaiAction extends Action{
	/* (non-Javadoc)
	 * @see org.apache.struts.action.Action#execute(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		// TODO Auto-generated method stub
		if(CheckQuyen.checkNguoiDung(request.getSession().getAttribute("tenDangNhap")+"")==false){
			return mapping.findForward("ndDangNhap");
		}
		BaiDangBO bdBO = new BaiDangBO();
		request.setCharacterEncoding("UTF-8");
		String tenDangNhap = (String) request.getSession().getAttribute("tenDangNhap");
		ND_LichSuDangBaiForm ndlichsu = (ND_LichSuDangBaiForm) form;
		String trangthai = ndlichsu.getTrangThai();
		String loai = ndlichsu.getLoai();
		ArrayList<BaiDang> allBD = bdBO.getBaiDangListByMaND(tenDangNhap);
		/* lay tat ca bai dang tu database */
		System.out.println(loai+trangthai);
		/**
		 * loai: táº¥t cáº£ 
		 * trangthai: táº¥t cáº£
		 */
		if (loai == null && trangthai == null || "Tất cả".equals(loai) && "Tất cả".equalsIgnoreCase(trangthai)) {
			ndlichsu.setListbd(allBD);
		}

		/**
		 * chon theo loai
		 */
		else if (loai != null && (trangthai == null || "Tất cả".equalsIgnoreCase(trangthai))) {
			int size = allBD.size();
			ArrayList<BaiDang> temp = new ArrayList<BaiDang>();
			for (int i = 0; i < size; i++) {
				if (loai.equals(allBD.get(i).getLoaiBD())) {
					temp.add(allBD.get(i));
				}
			}
			ndlichsu.setListbd(temp);
		}
		/**
		 * chon theo trangthai
		 */
		else if ((loai == null || "Tất cả".equals(loai)) && trangthai != null) {
			int size = allBD.size();
			ArrayList<BaiDang> temp = new ArrayList<BaiDang>();
			for (int i = 0; i < size; i++) {
				if (trangthai.equalsIgnoreCase(allBD.get(i).getTrangThai())) {
					temp.add(allBD.get(i));
				}
			}
			ndlichsu.setListbd(temp);
		}
		/**
		 * chon loai,  chon trangthai
		 */
		else {
			int size = allBD.size();
			ArrayList<BaiDang> temp = new ArrayList<BaiDang>();
			for (int i = 0; i < size; i++) {
				if (trangthai.equalsIgnoreCase(allBD.get(i).getTrangThai())
						&& loai.equals(allBD.get(i).getLoaiBD())) {
					temp.add(allBD.get(i));
				}
			}
			ndlichsu.setListbd(temp);
		}
		return mapping.findForward("lichsu");
	}
}
