/**
 * 
 */
package actions;

import java.io.File;
import java.io.FileOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bo.NguoiDungBO;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

import forms.AnhDaiDienForm;

/**				
 * ND_DoiAnhDaiDienAction.java			
 *				
 * Version 1.0				
 *				
 * Date: Aug 26, 2015			
 *				
 * Copyright 				
 *				
 * Modification Logs:				
 * DATE                 AUTHOR          DESCRIPTION				
 * -----------------------------------------------------------------------				
 * Aug 26, 2015        DatNVT           Create				
 */

public class ND_DoiAnhDaiDienAction extends Action {
	/* (non-Javadoc)
	 * @see org.apache.struts.action.Action#execute(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
	        HttpServletRequest request, HttpServletResponse response)
	        throws Exception {
	    AnhDaiDienForm f = (AnhDaiDienForm) form;
	    if(f.getSubmit()==null || f.getSubmit().isEmpty()){
	    	return mapping.findForward("trangdoianh");
	    }
	    
	    else{
	    	
    	    String tenDangNhap = f.getTenDangNhap();
    	    FormFile file = f.getImage();
    	    if(tenDangNhap == null || tenDangNhap.isEmpty() || file == null){
    	    	return mapping.findForward("trangloi");
    	    }
    	    String filePath= getServlet().getServletContext().getRealPath("/")+"upload";
    	    
    	    File folder = new File(filePath);
    	    if(!folder.exists()){
    	    	folder.mkdir();
    	    }
    	    String fileName = file.getFileName();
    	    if(fileName.isEmpty())
    	    	return mapping.findForward("thongtincanhan");
    	    if(!("").equals(fileName)){  
    	        System.out.println("Server path:" +filePath);
    	        File newFile = new File(filePath, fileName);       
    	        if(!newFile.exists()){
    	          FileOutputStream fos = new FileOutputStream(newFile);
    	          fos.write(file.getFileData());
    	          fos.flush();
    	          fos.close();
    	        }  
    //	        request.setAttribute("uploadedFilePath",newFile.getAbsoluteFile());
    //	        request.setAttribute("uploadedFileName",newFile.getName());
    	    }
    	    String imgPath = "upload/"+file.getFileName();
    	    System.out.println(imgPath);
    	    NguoiDungBO ndBO = new NguoiDungBO();
    	    ndBO.doiAnhDaiDien(tenDangNhap, imgPath);
    	    return mapping.findForward("thongtincanhan");
	    }
	}
}
