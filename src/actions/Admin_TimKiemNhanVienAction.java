package actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bo.NhanVienBO;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import forms.Admin_DSNhanVienForm;

public class Admin_TimKiemNhanVienAction extends Action {
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		request.setCharacterEncoding("UTF-8");
		Admin_DSNhanVienForm f = (Admin_DSNhanVienForm) form;
		NhanVienBO nvBo = new NhanVienBO();

		if (f.getSearchName().trim().length() == 0)
			f.setListNV(nvBo.getAll());
		else
			f.setListNV(nvBo.getBySearchName(f.getSearchName()));
		
		return mapping.findForward("admin_dsnhanvien");
	}
}
