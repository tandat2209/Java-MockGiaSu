package actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bean.ThongBao;
import model.bo.ThongBaoBO;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import common.CheckQuyen;

import forms.ThongBaoForm;

public class NV_XuLyThongBaoAction extends Action {
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		/* check quyen */
		if(CheckQuyen.checkNhanVien(request.getSession().getAttribute("tenDangNhap")+"")==false){
			return mapping.findForward("nvDangNhap");
		}
		ThongBaoForm tbForm = (ThongBaoForm) form;
		ThongBaoBO tbBO = new ThongBaoBO();
		
		// lay thongbao tu form
		
		String maTB = tbForm.getMaTB();
		ThongBao tb = tbBO.getThongBaoByMaTB(maTB);
		String maNguoiDang = tb.getMaNguoiDang();
		String maBaiDang = tb.getMaBaiDang();
		String xuly = tbForm.getXuly();
		if("an".equals(xuly)){
			tbBO.anThongBao(maTB, maBaiDang, maNguoiDang);
		} else if("boqua".equals(xuly)){
			tbBO.boquaThongBao(maTB);
		}
		return mapping.findForward("dsThongBao");
	}
}
