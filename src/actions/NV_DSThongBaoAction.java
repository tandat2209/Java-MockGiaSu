package actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bo.ThongBaoBO;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import common.CheckQuyen;

import forms.NV_DSThongBaoForm;

public class NV_DSThongBaoAction extends Action {
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
    	/* check quyen */
		if(CheckQuyen.checkNhanVien(request.getSession().getAttribute("tenDangNhap")+"")==false){
			return mapping.findForward("nvDangNhap");
		}
        NV_DSThongBaoForm dsThongBaoForm = (NV_DSThongBaoForm) form;
        ThongBaoBO tbBO = new ThongBaoBO();
        dsThongBaoForm.setThongBaoList(tbBO.getThongBaoList());
        return mapping.findForward("dsThongBao");
    }
}
