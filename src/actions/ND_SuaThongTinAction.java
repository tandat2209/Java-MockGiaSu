/**
 * 
 */
package actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.bean.NguoiDung;
import model.bo.NguoiDungBO;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import common.CheckQuyen;
import common.StringProcess;
import forms.ND_ThongTinCaNhanForm;

/**				
 * ND_SuaThongTinAction.java			
 *				
 * Version 1.0				
 *				
 * Date: Aug 14, 2015			
 *				
 * Copyright 				
 *				
 * Modification Logs:				
 * DATE                 AUTHOR          DESCRIPTION				
 * -----------------------------------------------------------------------				
 * Aug 14, 2015        HuyNV          Create				
 */

public class ND_SuaThongTinAction extends Action {
	/* (non-Javadoc)
	 * @see org.apache.struts.action.Action#execute(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		// TODO Auto-generated method stub
		if(CheckQuyen.checkNguoiDung(request.getSession().getAttribute("tenDangNhap")+"")==false){
			return mapping.findForward("ndDangNhap");
		}
		request.setCharacterEncoding("UTF-8");
		ND_ThongTinCaNhanForm nd_ThongTin = (ND_ThongTinCaNhanForm) form;
		/* lay tenDangNhap, quyen tu session */
		HttpSession session = request.getSession();
		String tenDangNhap = (String) session.getAttribute("tenDangNhap");
		NguoiDung nd;
		if("Xong".equals(nd_ThongTin.getSubmit())){
			ActionErrors error = new ActionErrors();
			
			//validate Họ tên
			if(StringProcess.isEmptyString(nd_ThongTin.getTenND())){
				error.add("errHoTen", new ActionMessage("error.hoTenTrong"));
			}
			//validate số điện thoại
			if(StringProcess.isEmptyString(nd_ThongTin.getSoDienThoai())){
				error.add("errPhone", new ActionMessage("error.phoneTrong"));
			} else if(!StringProcess.isValidPhone(nd_ThongTin.getSoDienThoai())){
				error.add("errPhone", new ActionMessage("error.phoneError"));
			}
			//validate ngày sinh
			if(StringProcess.isEmptyString(nd_ThongTin.getNgaySinh())){
				error.add("errNgaySinh", new ActionMessage("error.ngaySinhTrong"));
			} else if(!StringProcess.isValidDate(nd_ThongTin.getNgaySinh())){
				error.add("errNgaySinh", new ActionMessage("error.birthdayError"));
			}
			//validate email
			if(StringProcess.isEmptyString(nd_ThongTin.getEmail())){
				error.add("errEmail", new ActionMessage("error.errorTrong"));
			} else if(!StringProcess.CheckEmail(nd_ThongTin.getEmail())){
				error.add("errEmail", new ActionMessage("error.emailError"));
			}
			if(error.size()!=0){
				saveErrors(request, error);
				return mapping.findForward("suaThongtin");
			}else {
			nd = new NguoiDung();
			nd.setTenDangNhap(nd_ThongTin.getTenDangNhap());
			nd.setHoTen(nd_ThongTin.getTenND());
			nd.setGioiTinh(nd_ThongTin.getGioiTinh());
			nd.setNgaySinh(nd_ThongTin.getNgaySinh());
			System.out.println(nd.getNgaySinh());
			nd.setEmail(nd_ThongTin.getEmail());
			nd.setSoDienThoai(nd_ThongTin.getSoDienThoai());
			new NguoiDungBO().updateThongTinND(nd);
			nd_ThongTin.setSubmit("");
			return mapping.findForward("success");
			}
		}else{
			nd = new NguoiDungBO().getThongTinNguoiDung(tenDangNhap);
			nd_ThongTin.setTenDangNhap(nd.getTenDangNhap());
			nd_ThongTin.setTenND(nd.getHoTen());
			nd_ThongTin.setGioiTinh(nd.getGioiTinh());
			nd_ThongTin.setNgaySinh(nd.getNgaySinh());
			nd_ThongTin.setEmail(nd.getEmail());
			nd_ThongTin.setSoDienThoai(nd.getSoDienThoai());
			nd_ThongTin.setAnhDaiDien(nd.getAnhDaiDien());
			//nd_ThongTin.setMatKhau(nd.getMatKhau());
			return mapping.findForward("suaThongtin");
		}
	}
}
