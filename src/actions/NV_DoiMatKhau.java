/**
 * 
 */
package actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bean.NhanVien;
import model.bo.NguoiDungBO;
import model.bo.NhanVienBO;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import common.CheckQuyen;
import common.StringProcess;
import forms.NhanVienForm;

/**				
 * NV_DoiMatKhau.java			
 *				
 * Version 1.0				
 *				
 * Date: Aug 19, 2015			
 *				
 * Copyright 				
 *				
 * Modification Logs:				
 * DATE                 AUTHOR          DESCRIPTION				
 * -----------------------------------------------------------------------				
 * Aug 19, 2015        HuyNV          Create				
 */

public class NV_DoiMatKhau extends Action{
	/* (non-Javadoc)
	 * @see org.apache.struts.action.Action#execute(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		/* check quyen */
		if(CheckQuyen.checkNhanVien(request.getSession().getAttribute("tenDangNhap")+"")==false){
			return mapping.findForward("nvDangNhap");
		}
		// TODO Auto-generated method stub
		NhanVienForm f = (NhanVienForm)form;
		NhanVienBO nvBO = new NhanVienBO();
		NhanVien nv = nvBO.getNV(f.getTenDangNhap());
		String matKhau = nv.getMatKhau();
		if(f.getSubmit()!=null){
			ActionErrors error = new ActionErrors();
			System.out.println("++++"+matKhau);
			//validate mat khau
			if(!matKhau.equals(StringProcess.encryptMD5(f.getMatKhauCu()))){
				error.add("errMatKhauCu", new ActionMessage("error.matKhauCu"));
			}
			if (StringProcess.isEmptyString(f.getMatKhauMoi())) {
				error.add("errMatKhau", new ActionMessage("error.matKhau"));
			} else if(f.getMatKhauMoi().length()<6){
				error.add("errMatKhau", new ActionMessage("error.matKhau_03"));
			} else if(!f.getMatKhauMoi().equals(f.getMatKhauXN())){
				error.add("errMatKhauXN", new ActionMessage("error.matKhauXN"));
			}
			if(error.size()!=0){
				saveErrors(request, error);
				return mapping.findForward("nvdoimatkhau");
			}else {
				String mkMD5 = StringProcess.encryptMD5(f.getMatKhauMoi());
			nvBO.nvDoiMatKhau(f.getTenDangNhap(),mkMD5);
			return mapping.findForward("thanhcong");
			}
		}
		
		return mapping.findForward("nvdoimatkhau");
	}
}
