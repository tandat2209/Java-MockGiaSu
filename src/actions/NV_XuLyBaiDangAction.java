/**
 * 
 */
package actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bean.BaiDang;
import model.bo.BaiDangBO;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import common.CheckQuyen;

import forms.BaiDangForm;

/**				
 * NV_XuLyBaiDangAction.java			
 *				
 * Version 1.0				
 *				
 * Date: Aug 16, 2015			
 *				
 * Copyright 				
 *				
 * Modification Logs:				
 * DATE                 AUTHOR          DESCRIPTION				
 * -----------------------------------------------------------------------				
 * Aug 16, 2015        DatNVT           Create				
 */

public class NV_XuLyBaiDangAction extends Action {
	/* (non-Javadoc)
	 * @see org.apache.struts.action.Action#execute(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
	    request.setCharacterEncoding("UTF-8");
		/* check quyen */
		if(CheckQuyen.checkNhanVien(request.getSession().getAttribute("tenDangNhap")+"")==false){
			return mapping.findForward("nvDangNhap");
		}
		BaiDangForm bdForm = (BaiDangForm) form;
		BaiDangBO bdBO = new BaiDangBO();
		// lay maBD tu form

		String maBD = bdForm.getMaBD();
		String xuly = bdForm.getXuly();
		if("duyet".equalsIgnoreCase(xuly)){
			bdBO.duyetBaiDang(maBD);
		} else if("tuchoi".equalsIgnoreCase(xuly)){
			bdBO.tuchoiBaiDang(maBD);
		}
		return mapping.findForward("dsDuyetBD");
	}
}
