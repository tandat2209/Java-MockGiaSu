package actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bean.BaiDang;
import model.bo.BaiDangBO;
import model.bo.KhuVucBO;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import common.CheckQuyen;
import common.StringProcess;
import forms.BaiDangForm;

public class NV_ChiTietBaiDangAction extends Action {
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
    	/* check quyen */
		if(CheckQuyen.checkNhanVien(request.getSession().getAttribute("tenDangNhap")+"")==false){
			return mapping.findForward("nvDangNhap");
		}
        BaiDangForm bdForm = (BaiDangForm) form;
        
        // lay loai bai dang de xet
        String maBD = bdForm.getMaBD();
        
        if(maBD==null || maBD.isEmpty()) 
            return mapping.findForward("trangloi");
        BaiDangBO bdBO = new BaiDangBO();
        BaiDang bd = bdBO.getBaiDang(maBD);
        
        if(bd==null || bd.getMaBD()== null || bd.getMaBD().isEmpty()) 
            return mapping.findForward("trangloi");
        bd.setLuongGS(StringProcess.getMoneyVND(bd.getLuongGS()));
        bd.setLuongSD(StringProcess.getMoneyVND(bd.getLuongSD()));
        String loaiBD = bd.getLoaiBD();
        
        // dua du lieu vao form
        bdForm.setBaiDang(bd);
        
        // lay khu vuc de dua vao form
        KhuVucBO kvBO = new KhuVucBO();
        bdForm.setKhuVucList(kvBO.getKhuVucListByMaBD(maBD));
       
        // dua vao loaiBD de chuyen den trang xemchitiet bdGS hoac bdSD
        if("Gia sư".equals(loaiBD)){
            System.out.println("Chi tiet bai dang Gia su");
            return mapping.findForward("chitietGS");
        } else if("Suất dạy".equals(loaiBD)){
            System.out.println("Chi tiet bai dang Suat day");
            return mapping.findForward("chitietSD");
        }
        return mapping.findForward("dsduyetbaidang");
    }
}
