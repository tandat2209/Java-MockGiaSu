/**
 * 
 */
package actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bo.NhanVienBO;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import forms.Admin_DSNhanVienForm;

/**
 * Admin_DSNhanVienAction.java
 * 
 * Version 1.0
 * 
 * Date: Aug 13, 2015
 * 
 * Copyright
 * 
 * Modification Logs: DATE AUTHOR DESCRIPTION
 * -----------------------------------------------------------------------
 * 13, 2015 HuyNV Create
 */

public class Admin_DSNhanVienAction extends Action {
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		request.setCharacterEncoding("UTF-8");
		Admin_DSNhanVienForm f = (Admin_DSNhanVienForm) form;
		NhanVienBO nvBo = new NhanVienBO();
		
		//Nhấn nút tìm kiếm
		if ("submit".equals(f.getSubmit())) {
			f.setListNV(nvBo.getBySearchName(f.getSearchName()));
			f.setSearchName("");
		} 
		
		//Mặc định lấy tất cả
		else {
			f.setListNV(nvBo.getAll());
		}
		return mapping.findForward("admin_dsnhanvien");
	}
}
