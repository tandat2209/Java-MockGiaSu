package actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bean.BaiDang;
import model.bo.ProcessBO;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import forms.DetailInfoForm;

public class DetailInfoAction extends Action{
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		request.setCharacterEncoding("UTF-8");
		DetailInfoForm f = (DetailInfoForm) form;
		BaiDang bd = ProcessBO.getCourseByTitle(f.getMaBD());
		f.setLoaiBD(bd.getLoaiBD());
		f.setTieuDe(bd.getTieuDe());
		f.setMon(bd.getMon());
		f.setLop(bd.getLop());
		f.setMoTaThem(bd.getMoTaThem());
		f.setMaNguoiDang(bd.getMaNguoiDang());
		f.setThoiGianDang(bd.getThoiGianDang());
		f.setTrangThai(bd.getTrangThai());
		return mapping.findForward("displayDetailInfo");
	}
}
