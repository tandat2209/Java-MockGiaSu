package actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bean.NhanVien;
import model.bo.NhanVienBO;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import common.StringProcess;
import forms.Admin_NhanVienForm;

public class Admin_SuaNhanVienAction extends Action {
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
					throws Exception {
		request.setCharacterEncoding("UTF-8");
		Admin_NhanVienForm f = (Admin_NhanVienForm) form;
		NhanVienBO nvBO = new NhanVienBO();
		
		
		if("Lưu".equals(f.getSubmitSua())){
			ActionErrors errors = new ActionErrors();


			// Validate tÃƒÂªn nhÃƒÂ¢n viÃƒÂªn
			if (StringProcess.isEmptyString(f.getTenNV())) {
				errors.add("tenNVError", new ActionMessage("error.tenNV_01"));
			}
			System.out.println(f.getNgaySinh());
			
			// Validate ngÃƒÂ y sinh
			if (StringProcess.isEmptyString(f.getNgaySinh())) {
				errors.add("ngaySinhError",new ActionMessage("error.ngaySinh_01"));
			}else if(!StringProcess.isValidDate(f.getNgaySinh())){
				errors.add("ngaySinhError",new ActionMessage("error.ngaySinh_02"));
			}

			// Validate email
			if (StringProcess.isEmptyString(f.getEmail())
					|| !StringProcess.CheckEmail(f.getEmail())) {
				errors.add("emailError", new ActionMessage("error.email_01"));
			}

			// Validate sÃ¡Â»â€˜ Ã„â€˜iÃ¡Â»â€¡n thoÃ¡ÂºÂ¡i
			if (StringProcess.isEmptyString(f.getSoDienThoai())
					|| !StringProcess.isValidPhone(f.getSoDienThoai())) {
				errors.add("soDienThoaiError",
						new ActionMessage("error.soDienThoai_01"));
			}

			if (errors.size() != 0) {
				saveErrors(request, errors);
				return mapping.findForward("admin_suanhanvien");
			}else {
				System.out.println("edit......");
				NhanVien nv = new NhanVien();
				nv.setTenDangNhap(f.getTenDangNhap());
				nv.setDiaChi(f.getDiaChi());
				nv.setEmail(f.getEmail());
				nv.setGioiTinh(f.getGioiTinh());
				nv.setMatKhau(f.getMatKhau());
				nv.setNgaySinh(f.getNgaySinh());
				nv.setSoDienThoai(f.getSoDienThoai());
				nv.setTenNV(f.getTenNV());

				nvBO.updateNhanVien(nv);
				return mapping.findForward("admin_dsnhanvien");
			}
		}
		NhanVien nv = nvBO.getNV(f.getTenDangNhap());
		f.setTenNV(nv.getTenNV());
		f.setNgaySinh(nv.getNgaySinh());
		f.setDiaChi(nv.getDiaChi());
		f.setGioiTinh(nv.getGioiTinh());
		f.setEmail(nv.getEmail());
		f.setSoDienThoai(nv.getSoDienThoai());
		f.setMatKhau(nv.getMatKhau());
		return mapping.findForward("admin_suanhanvien");

	}
}
