/**
 * 
 */
package actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bean.NguoiDung;
import model.bo.NguoiDungBO;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import common.CheckQuyen;

import forms.NV_ChiTietNguoiDungForm;

/**				
 * NV_ChiTietNguoiDungAction.java			
 *				
 * Version 1.0				
 *				
 * Date: Aug 18, 2015			
 *				
 * Copyright 				
 *				
 * Modification Logs:				
 * DATE                 AUTHOR          DESCRIPTION				
 * -----------------------------------------------------------------------				
 * Aug 18, 2015        HuyNV          Create				
 */

public class NV_ChiTietNguoiDungAction extends Action {
/* (non-Javadoc)
 * @see org.apache.struts.action.Action#execute(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
 */
@Override
public ActionForward execute(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response)
		throws Exception {
	NV_ChiTietNguoiDungForm f = (NV_ChiTietNguoiDungForm) form;
	NguoiDungBO ndBO = new NguoiDungBO();
	if(f.getTenDangNhap()==null || f.getTenDangNhap().isEmpty()){
	    return mapping.findForward("trangloi");
	}
	NguoiDung nd = ndBO.getThongTinNguoiDung(f.getTenDangNhap());
	if(nd==null || nd.getTenDangNhap() == null || nd.getTenDangNhap().isEmpty()){
	    return mapping.findForward("trangloi");
	}
	f.setNguoiDung(nd);
	/* check quyen */
	if(CheckQuyen.checkNhanVien(request.getSession().getAttribute("tenDangNhap")+"")==true){
		return mapping.findForward("nvchitietND");
	}
	return mapping.findForward("chitietND");
}
}
