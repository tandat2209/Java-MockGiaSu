/**
 * 
 */
package actions;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import common.CheckQuyen;
import common.StringProcess;
import forms.ND_ChiTietBaiDangForm;
import model.bean.BaiDang;
import model.bean.Key;
import model.bo.BaiDangBO;
import model.bo.KhuVucBO;

/**
 * ND_DangBaiGiaSuAction.java
 *
 * Version 1.0
 *
 * Date: Aug 17, 2015
 *
 * Copyright
 *
 * Modification Logs: DATE AUTHOR DESCRIPTION
 * -----------------------------------------------------------------------
 * 11:59:44 PM HuyNV Create
 */
public class ND_DangBaiGiaSuAction extends Action {
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.struts.action.Action#execute(org.apache.struts.action.
	 * ActionMapping, org.apache.struts.action.ActionForm,
	 * javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		// TODO Auto-generated method stub
		if(CheckQuyen.checkNguoiDung(request.getSession().getAttribute("tenDangNhap")+"")==false){
			return mapping.findForward("ndDangNhap");
		}
		request.setCharacterEncoding("UTF-8");
		ND_ChiTietBaiDangForm f = (ND_ChiTietBaiDangForm) form;
		// get tenDangNhap tu session
		HttpSession session = request.getSession();
		String tenDangNhap = (String) session.getAttribute("tenDangNhap");
		f.setMaNguoiDang(tenDangNhap);

		BaiDangBO bdBo = new BaiDangBO();
		KhuVucBO kvBo = new KhuVucBO();
		System.out.println("submit:" + f.getSubmit());
		if ("Đăng bài".equals(f.getSubmit())) {
			ActionErrors error = new ActionErrors();

			// validate Họ tên
			if (StringProcess.isEmptyString(f.getTenGiaSu())) {
				error.add("errHoTen", new ActionMessage("error.hoTenTrong"));
			}
			// validate giới tính
			if (f.getGioiTinh() == null || "".equals(f.getGioiTinh())) {
				error.add("errGioiTinh", new ActionMessage(
						"error.gioiTinhError"));
			}
			// validate số điện thoại
			if (StringProcess.isEmptyString(f.getSoDienThoai())) {
				error.add("errPhone", new ActionMessage("error.phoneTrong"));
			} else if (!StringProcess.isValidPhone(f.getSoDienThoai())) {
				error.add("errPhone", new ActionMessage("error.phoneError"));
			}
			// validate ngày sinh
			if (StringProcess.isEmptyString(f.getNgaySinh())) {
				error.add("errNgaySinh", new ActionMessage(
						"error.ngaySinhTrong"));
			} else if (!StringProcess.isValidDate(f.getNgaySinh())) {
				error.add("errNgaySinh", new ActionMessage(
						"error.birthdayError"));
			}
			// validate tiêu đề
			if (StringProcess.isEmptyString(f.getTieuDe())) {
				error.add("errTieuDe", new ActionMessage("error.tieuDeTrong"));
			} else if (f.getTieuDe().trim().split(" ").length >= 50) {
				error.add("errTieuDe", new ActionMessage("error.tieuDeError"));
			}
			// validate khu vực
			if (f.getMaKvArr() == null) {
				error.add("errKhuVuc", new ActionMessage("error.khuVucError"));
			}
			// validdate Môn
			if (StringProcess.isEmptyString(f.getMon())) {
				error.add("errMon", new ActionMessage("error.monError"));
			}
			// validate Lớp
			if (StringProcess.isEmptyString(f.getLop())) {
				error.add("errLop", new ActionMessage("error.monLop"));
			}
			// validate Luong
			if (!StringProcess.isEmptyString(f.getLuongGS())) {
				if (StringProcess.isEmptyNumber(f.getLuongGS())) {
					error.add("errLuong", new ActionMessage("error.luong"));
				}
			}
			if (error.size() != 0) {
				saveErrors(request, error);
				f.setKvList(kvBo.getKVList());
				return mapping.findForward("danggiasu");
			} else {
				BaiDang bd = new BaiDang();
				String maBD = "BD" + Key.getKey();
				bd.setMaBD(maBD);
				f.setMaBD(maBD);
				System.out.println("maBD:" + f.getMaBD());
				bd.setMaNguoiDang(f.getMaNguoiDang());
				bd.setLoaiBD("Gia sư");
				bd.setTieuDe(f.getTieuDe());
				bd.setMon(f.getMon());
				bd.setLop(f.getLop());
				bd.setThoiGianDay(f.getThoiGianDay());
				bd.setSoDienThoai(f.getSoDienThoai());
				bd.setMoTaThem(f.getMoTaThem());
				bd.setMaNguoiDang(f.getMaNguoiDang());
				bd.setTrangThai(f.getTrangThai());
				bd.setTenGiaSu(f.getTenGiaSu());
				bd.setGioiTinh(f.getGioiTinh());
				bd.setNoiCongTac(f.getNoiCongTac());
				bd.setNgheNghiep(f.getNgheNghiep());
				bd.setNgaySinh(f.getNgaySinh());
				bd.setQueQuan(f.getQueQuan());
				bd.setLuongGS(f.getLuongGS());
				ArrayList<String> temparr = new ArrayList<String>();
				for (int i = 0; i < f.getMaKvArr().length; i++) {
					temparr.add(f.getMaKvArr()[i]);
				}
				bdBo.insertBaiDang(bd);
				kvBo.updateKvBaiDang(temparr, bd.getMaBD());
				return mapping.findForward("lichsu");
			}
		} else {
			f.setGioiTinh("Nam");
			f.setKvList(kvBo.getKVList());
			return mapping.findForward("danggiasu");
		}

	}
}
