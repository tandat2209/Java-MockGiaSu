/**
 * 
 */
package actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bean.NguoiDung;
import model.bo.NguoiDungBO;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import common.SendMail;
import common.StringProcess;
import forms.ND_ThongTinCaNhanForm;

/**				
 * ND_QuenMatKhauAction.java			
 *				
 * Version 1.0				
 *				
 * Date: Aug 26, 2015			
 *				
 * Copyright 				
 *				
 * Modification Logs:				
 * DATE                 AUTHOR          DESCRIPTION				
 * -----------------------------------------------------------------------				
 * Aug 26, 2015        DatNVT           Create				
 */

public class ND_QuenMatKhauAction extends Action {
	/* (non-Javadoc)
	 * @see org.apache.struts.action.Action#execute(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
	        HttpServletRequest request, HttpServletResponse response)
	        throws Exception {
	    request.setCharacterEncoding("UTF-8");
	    NguoiDungBO ndBO = new NguoiDungBO();
	    ND_ThongTinCaNhanForm f = (ND_ThongTinCaNhanForm) form;
	    String tenDangNhap = f.getTenDangNhap();
	    String email = f.getEmail();
	    if(tenDangNhap==null) 
	    	return mapping.findForward("quenmatkhau");
	    ActionErrors error = new ActionErrors();
	    //validate email
		if(StringProcess.isEmptyString(email)){
			error.add("errEmail", new ActionMessage("error.errorTrong"));
		} else if(!StringProcess.CheckEmail(email)){
			error.add("errEmail", new ActionMessage("error.emailError"));
		}
		if(error.size()!=0){
			saveErrors(request, error);
			return mapping.findForward("quenmatkhau");
		} else{
			NguoiDung nd = ndBO.getThongTinNguoiDung(tenDangNhap);
			if(email.equals(nd.getEmail())){
				ndBO.resetMatKhau(tenDangNhap);
//				SendMail.sendEmail(email);
				
				f.setSuccessMS("Mật khẩu đã được gửi tới email của bạn! :)");
				return mapping.findForward("quenmatkhau");
			} else{
				f.setErrorMS("Email hoặc Tên đăng nhập không đúng!");
				return mapping.findForward("quenmatkhau");
			}
		}
	}
}
