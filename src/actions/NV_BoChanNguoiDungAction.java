/**
 * 
 */
package actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bo.NguoiDungBO;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import common.CheckQuyen;
import forms.NV_ChiTietNguoiDungForm;

/**
 * NV_BoChanNguoiDungAction.java
 * 
 * Date: Aug 21, 2015
 * 
 * Copyright
 * 
 * Modification Logs: 
 * DATE      AUTHOR      DESCRIPTION
 * ------------------------------------
 * Aug 21, 2015    Fresher075     Create
 */

public class NV_BoChanNguoiDungAction extends Action {
    /* (non-Javadoc)
     * @see org.apache.struts.action.Action#execute(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        request.setCharacterEncoding("UTF-8");
        /* check quyen */
        if(CheckQuyen.checkNhanVien(request.getSession().getAttribute("tenDangNhap")+"")==false){
            return mapping.findForward("nvDangNhap");
        }
        NV_ChiTietNguoiDungForm f = (NV_ChiTietNguoiDungForm) form;
        NguoiDungBO ndBO = new NguoiDungBO();
        String tenDangNhap = f.getTenDangNhap();
        ndBO.boChanNguoiDung(tenDangNhap);
        return mapping.findForward("chitietND");
    }
}
