/**
 * 
 */
package actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bo.BaiDangBO;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import common.CheckQuyen;

import forms.ND_ChiTietBaiDangForm;

/**				
 * ND_xoaBaiDangAction.java			
 *				
 * Version 1.0				
 *				
 * Date: Aug 18, 2015			
 *				
 * Copyright 				
 *				
 * Modification Logs:				
 * DATE                 AUTHOR          DESCRIPTION				
 * -----------------------------------------------------------------------				
 * Aug 18, 2015        HuyNV          Create				
 */

public class ND_xoaBaiDangAction extends Action{
/* (non-Javadoc)
 * @see org.apache.struts.action.Action#execute(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
 */
@Override
public ActionForward execute(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response)
		throws Exception {
	// TODO Auto-generated method stub
	if(CheckQuyen.checkNguoiDung(request.getSession().getAttribute("tenDangNhap")+"")==false){
		return mapping.findForward("ndDangNhap");
	}
	ND_ChiTietBaiDangForm f = (ND_ChiTietBaiDangForm)form;
	BaiDangBO bdBo = new BaiDangBO();
	bdBo.deleteBaiDang(f.getMaBD());
	return mapping.findForward("xoaxong");
}
}
