/**
 * 
 */
package actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import common.StringProcess;

import forms.ND_ChiTietBaiDangForm;
import model.bean.BaiDang;
import model.bo.BaiDangBO;

/**
 * ChiTietBaiDangAction.java
 *
 * Version 1.0
 *
 * Date: Aug 22, 2015
 *
 * Copyright
 *
 * Modification Logs: DATE AUTHOR DESCRIPTION
 * -----------------------------------------------------------------------
 * 3:42:55 PM HuyNV Create
 */
public class ChiTietBaiDangAction extends Action {
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.struts.action.Action#execute(org.apache.struts.action.
	 * ActionMapping, org.apache.struts.action.ActionForm,
	 * javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
					throws Exception {
		ND_ChiTietBaiDangForm f = (ND_ChiTietBaiDangForm) form;
		request.setCharacterEncoding("UTF-8");

		// lay thÃ´ng tin bÃ i Ä‘Äƒng tá»« DB
		BaiDangBO baidangBO = new BaiDangBO();
		// String maBD = (String) request.getAttribute("maBD");
		if( f.getMaBD()==null || f.getMaBD().isEmpty()){
		    return mapping.findForward("trangloi");
		}
		BaiDang bd = baidangBO.getBaiDang(f.getMaBD());
		if(bd==null || bd.getMaBD() == null || bd.getMaBD().isEmpty()){
		    return mapping.findForward("trangloi");
		}
		bd.setLuongGS(StringProcess.getMoneyVND(bd.getLuongGS()));
	    bd.setLuongSD(StringProcess.getMoneyVND(bd.getLuongSD()));
		f.setBaiDang(bd);
		System.out.println("++++" + f.getBaiDang().getTieuDe());
		String khuVuc = baidangBO.getKhuVuc(f.getMaBD());
		f.setKhuVuc(khuVuc.substring(2, khuVuc.length()));
		String loaiBD = bd.getLoaiBD();
		if ("Gia sư".equals(loaiBD)) {
			f.setLienquanList(BaiDangBO.getGSLienQuanList(bd));
			System.out.println(f.getBaiDang().getTieuDe() + "+++++");
			return mapping.findForward("chitietGS");
		} else {
			f.setLienquanList(BaiDangBO.getSDLienQuanList(bd));
			return mapping.findForward("chitietSD");
		}
	}
}
