package actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.bean.NhanVien;
import model.bo.NhanVienBO;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import common.CheckQuyen;

import forms.NhanVienForm;

public class NV_ThongTinCaNhanAction extends Action {
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		NhanVienForm nvForm = (NhanVienForm) form;
		/* lay tenDangNhap, quyen tu session */
		HttpSession session = request.getSession();
		String tenDangNhap = (String) session.getAttribute("tenDangNhap");
		
		/* check quyen */
		if(CheckQuyen.checkNhanVien(tenDangNhap)== false){
			return mapping.findForward("nvDangNhap");
		}
		/* lay thong tin nhan vien tu tenDangNhap o session */
		/* dua thong tin vao form */
		NhanVienBO nvBO = new NhanVienBO();
		NhanVien nhanvien = nvBO.getNV(tenDangNhap);
		nvForm.setTenDangNhap(tenDangNhap);
		nvForm.setTenNV(nhanvien.getTenNV());
		nvForm.setGioiTinh(nhanvien.getGioiTinh());
		nvForm.setNgaySinh(nhanvien.getNgaySinh());
		nvForm.setDiaChi(nhanvien.getDiaChi());
		nvForm.setEmail(nhanvien.getEmail());
		nvForm.setSoDienThoai(nhanvien.getSoDienThoai());
		nvForm.setQuyen(nhanvien.getQuyen());
		nvForm.setMatKhau(nhanvien.getMatKhau());
		return mapping.findForward("nvThongTinCaNhan");
	}
}
