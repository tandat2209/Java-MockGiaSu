package actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bean.NhanVien;
import model.bo.NhanVienBO;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import forms.Admin_NhanVienForm;

public class Admin_XemNhanVienAction extends Action {
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		request.setCharacterEncoding("UTF-8");
		
		Admin_NhanVienForm f = (Admin_NhanVienForm) form;
		NhanVienBO nvBO = new NhanVienBO();
		f.setSubmitSua("");
		f.setSubmitThem("");
		
		NhanVien nv = nvBO.getNV(f.getTenDangNhap());
		f.setTenNV(nv.getTenNV());
		f.setNgaySinh(nv.getNgaySinh());
		f.setDiaChi(nv.getDiaChi());
		f.setGioiTinh(nv.getGioiTinh());
		f.setEmail(nv.getEmail());
		f.setSoDienThoai(nv.getSoDienThoai());
		f.setMatKhau(nv.getMatKhau());
		
		return mapping.findForward("admin_xemnhanvien");
	}
}
