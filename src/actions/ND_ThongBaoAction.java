/**
 * 
 */
package actions;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import common.CheckQuyen;

import forms.ND_ThongBaoForm;
import model.bean.ThongBao;
import model.bo.ThongBaoBO;

/**
 * ND_ThongBaoAction.java
 *
 * Version 1.0
 *
 * Date: Aug 16, 2015
 *
 * Copyright 
 *
 * Modification Logs:
 * DATE                 AUTHOR          DESCRIPTION
 * -----------------------------------------------------------------------
 * 10:00:22 AM        	HuyNV          Create
 */
public class ND_ThongBaoAction extends Action {
/* (non-Javadoc)
 * @see org.apache.struts.action.Action#execute(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
 */
@Override
public ActionForward execute(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response)
				throws Exception {
	// TODO Auto-generated method stub
	if(CheckQuyen.checkNguoiDung(request.getSession().getAttribute("tenDangNhap")+"")==false){
		return mapping.findForward("ndDangNhap");
	}
	request.setCharacterEncoding("UTF-8");
	ND_ThongBaoForm thongBaoForm = (ND_ThongBaoForm) form;
	
	//lấy tên đăng nhập từ session đưa vào form
	HttpSession session = request.getSession();
	String tenDangNhap = (String) session.getAttribute("tenDangNhap");
	thongBaoForm.setTenDangNhap(tenDangNhap);
	
	//lấy list thông báo từ DB
	ThongBaoBO tbBO = new ThongBaoBO();
	ArrayList<ThongBao> tb = tbBO.getThongBaoListND(tenDangNhap);
	thongBaoForm.setListThongBaoND(tb);
	return mapping.findForward("thongbao");
}
}
