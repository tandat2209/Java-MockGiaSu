package actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bo.NhanVienBO;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import forms.Admin_NhanVienForm;

public class Admin_XoaNhanVienAction extends Action {
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Admin_NhanVienForm f = (Admin_NhanVienForm) form;
		NhanVienBO nvBo = new NhanVienBO();
		System.out.println("Xoa: " + f.getTenDangNhap());
		nvBo.xoaNhanVien(f.getTenDangNhap());
		return mapping.findForward("admin_dsnhanvien");
	}
}
