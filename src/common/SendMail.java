/**
 * 
 */
package common;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**				
 * SendMail.java			
 *				
 * Version 1.0				
 *				
 * Date: Aug 26, 2015			
 *				
 * Copyright 				
 *				
 * Modification Logs:				
 * DATE                 AUTHOR          DESCRIPTION				
 * -----------------------------------------------------------------------				
 * Aug 26, 2015        DatNVT           Create				
 */

public class SendMail {
	private static String USER_NAME = "tandat2209";  // GMail user name (just the part before "@gmail.com")
    private static String PASSWORD = ""; // GMail password
    
    public static void sendEmail(String recipient){
    	String from = USER_NAME;
    	 String pass = PASSWORD;
    	 String[] to = { recipient };
    	 String subject = "[GiasuDN.net] CẬP NHẬT MẬT KHẨU";
         String body = "Chào bạn, mật khẩu của bạn đã được reset thành \"123456\" . "
         		+ "Vui lòng thay đổi mật khẩu sau khi đăng nhập!";
         sendFromGMail(from, pass, to, subject, body);
    }
    
    private static void sendFromGMail(String from, String pass, String[] to, String subject, String body) {
        Properties props = System.getProperties();
        String host = "smtp.gmail.com";
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.user", from);
        props.put("mail.smtp.password", pass);
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(USER_NAME, PASSWORD);
            }
        });
        MimeMessage message = new MimeMessage(session);

        try {
            message.setFrom(new InternetAddress(from));
            InternetAddress[] toAddress = new InternetAddress[to.length];

            // To get the array of addresses
            for( int i = 0; i < to.length; i++ ) {
                toAddress[i] = new InternetAddress(to[i]);
            }

            for( int i = 0; i < toAddress.length; i++) {
                message.addRecipient(Message.RecipientType.TO, toAddress[i]);
            }

            message.setSubject(subject);
            message.setText(body);
            Transport transport = session.getTransport("smtp");
            transport.connect(host, from, pass);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        }
        catch (AddressException ae) {
            ae.printStackTrace();
        }
        catch (MessagingException me) {
            me.printStackTrace();
        }
    }
    public static void main(String[] args) {
	    sendEmail("dracudakid@live.com");
    }
}
