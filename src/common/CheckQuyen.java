/**
 * 
 */
package common;

import model.bo.NguoiDungBO;
import model.bo.NhanVienBO;

/**				
 * CheckQuyen.java			
 *				
 * Version 1.0				
 *				
 * Date: Aug 20, 2015			
 *				
 * Copyright 				
 *				
 * Modification Logs:				
 * DATE                 AUTHOR          DESCRIPTION				
 * -----------------------------------------------------------------------				
 * Aug 20, 2015        DatNVT           Create				
 */

public class CheckQuyen {
	public static boolean checkNhanVien(String tenDangNhap){
		NhanVienBO nvBO = new NhanVienBO();
		return nvBO.checkNhanVien(tenDangNhap); 
	}
	public static boolean checkNguoiDung(String tenDangNhap){
		NguoiDungBO ndBO = new NguoiDungBO();
		return ndBO.checkNguoiDung(tenDangNhap);
	}
}
