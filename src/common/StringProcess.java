package common;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.Normalizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * StringProcess.java
 * 
 * Version 1.0
 * 
 * Date: Jan 20, 2015
 * 
 * Copyright
 * 
 * Modification Logs: DATE AUTHOR DESCRIPTION
 * ----------------------------------------------------------------------- 20,
 * 2015 LienHTM Create
 */

public class StringProcess {

	// convert Money
	public static String getMoneyVND(String money) {
		StringBuffer str = new StringBuffer();
		String temp = money.trim().split("\\.")[0];
		int size = temp.length();
		int count = 0;
		for (int i = size - 1; i > 0; i--) {
			str.append(temp.charAt(i));
			count++;
			if (count == 3) {
				str.append(".");
				count = 0;
			}
		}
		str.append(temp.charAt(0));
		return str.reverse().toString();
	}

	public static String getThoiGianDang(String thoiGianDang) {
		String[] datetime = thoiGianDang.split(" ");
		String hour = datetime[0];
		String date = datetime[1];
		return hour + " ngày " + date;
	}

	/**
	 * Kiá»ƒm tra s cÃ³ lÃ  xÃ¢u rá»—ng khÃ´ng?
	 * 
	 * @param s
	 * @return boolean
	 */
	public static boolean isEmptyString(String s) {
		if (s == null || s.trim().length() == 0)
			return true;
		return false;
	}

	/**
	 * Kiá»ƒm tra s cÃ³ chá»©a khoáº£ng tráº¯ng?
	 * 
	 * @param s
	 * @return
	 */
	public static boolean hasSpaceInString(String s) {
		if (s.contains(" "))
			return true;
		else
			return false;
	}

	/**
	 * Kiá»ƒm tra xÃ¢u s cÃ³ Ã­t nháº¥t 6 kÃ­ tá»±
	 * 
	 * @param s
	 * @return
	 */
	public static boolean has6Character(String s) {
		if (isEmptyString(s) || s.length() < 6)
			return false;
		else
			return true;
	}

	/**
	 * Validate SÄ�T: Chá»‰ gá»“m sá»‘ vÃ  cÃ³ Ã­t nháº¥t 10 chá»¯ sá»‘
	 * 
	 * @param s
	 * @return boolean
	 */
	public static boolean isValidPhone(String s) {

		if ("".equals(s))
			s = "";
		String regex = "[0-9]+";
		if (s.matches(regex) && (s.length() == 10 || s.length() == 11))
			return true;
		return false;
	}

	/**
	 * chi nhap kieu so
	 * 
	 * @param date
	 * @return boolean
	 */
	public static boolean isEmptyNumber(String s) {
		if ("".equals(s))
			s = "";
		String regex = "[0-9]+";
		if (s.matches(regex))
			return false;
		return true;
	}

	/**
	 * Kiá»ƒm tra date Ä‘Ãºng Ä‘á»‹nh dáº¡ng dd/MM/yyyy khÃ´ng?
	 * 
	 * @param date
	 * @return boolean
	 */
	public static boolean isValidDate(String date) {
		String regex1 = "^(3[01]|[12][0-9]|0[1-9]|[1-9])/(1[0-2]|0[1-9]|[1-9])/[0-9]{4}$";
		Pattern pattern1 = Pattern.compile(regex1);
		Matcher matcher1 = pattern1.matcher(date);
		boolean values1 = matcher1.matches();

		String regex2 = "^(3[01]|[12][0-9]|0[1-9]|[1-9])-(1[0-2]|0[1-9]|[1-9])-[0-9]{4}$";
		Pattern pattern2 = Pattern.compile(regex2);
		Matcher matcher2 = pattern2.matcher(date);
		boolean values2 = matcher2.matches();

		return values1 || values2;
	}

	/**
	 * Validate email
	 * 
	 * @param email
	 * @return
	 */

	public static boolean CheckEmail(String email) {
		String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
		        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		Pattern pattern = Pattern.compile(EMAIL_PATTERN);
		Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}

	public static boolean isPhoneNumberValid(String phoneNumber) {
		boolean isValid = false;
		String expression = "^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$";
		CharSequence inputStr = phoneNumber;
		Pattern pattern = Pattern.compile(expression);
		Matcher matcher = pattern.matcher(inputStr);
		if (matcher.matches()) {
			isValid = true;
		}
		return isValid;
	}

	public boolean CheckFullName(String fullName) {
		String USERNAME_PATTERN = "^[a-zA-Z\\sàáạã_-]{3,25}$";
		Pattern pattern = Pattern.compile(USERNAME_PATTERN);
		Matcher matcher = pattern.matcher(fullName);
		return matcher.matches();
	}

	public static String removeAccent(String s) {
		String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
		Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
		return pattern.matcher(temp).replaceAll("");
	}
	public static String encryptMD5(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(input.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            String hashtext = number.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }
}
