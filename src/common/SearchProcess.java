package common;

import java.util.ArrayList;
import java.util.Arrays;

public class SearchProcess {
	public static ArrayList<String> split(String input, String[] grades) {
		if ("".equals(input)) {
			return null;
		} else {
			ArrayList<String> list = new ArrayList<String>();
			String object = input;
			String grade = "";
			int size = grades.length;
			int position = -1;
			for (int i = 0; i < size; i++) {
				if (input.indexOf(grades[i]) != -1) {
					position = input.indexOf(grades[i]);
					object = input.substring(0, position);
					grade = input.substring(position);
					break;
				}
			}
			list.add(object);
			list.add(grade);
			return list;
		}
	}

	public static boolean isSubject(String subject, String db) {
		boolean check = true;
		subject = subject.replace(" ", ",");
		String[] subjectList = subject.split(",");
		for (int i = 0; i < subjectList.length; i++) {
			if ("li".equals(subjectList[i])) {
				subjectList[i] = subjectList[i].replace("i", "y");
			}
			if (db.indexOf(subjectList[i]) == -1) {
				check = false;
				break;
			}
		}
		return check;
	}

	public static boolean isGrade(String grade, String db) {
		boolean check = true;
		grade = grade.replace(" ", ",");
		String[] gradeList = grade.split(",");
		int size = gradeList.length;
		for (int i = 0; i < size; i++) {
			if (db.indexOf(gradeList[i]) == -1) {
				check = false;
				break;
			}
		}
		return check;
	}

	public static boolean isCorrect(String input, String database) {
		if (input == null) {
			return true;
		}
		input = input.toLowerCase();
		String inputCopy = removeAccent(input);
		database = database.toLowerCase();
		String databaseCopy = removeAccent(database);
		String[] grades = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10",
				"11", "12" };
		ArrayList<String> list = SearchProcess.split(input, grades);
		boolean isSubject = false;
		boolean isSubjectDBCopy = false;
		boolean isSubjectInCopy = false;
		String subject = "";
		String grade = "";
		if (list == null) {
			isSubject = true;
		} else {
			subject = list.get(0);
			grade = list.get(1);
			isSubject = SearchProcess.isSubject(subject, database);
			isSubjectDBCopy = SearchProcess.isSubject(subject, databaseCopy);
			isSubjectInCopy = SearchProcess.isSubject(inputCopy, database);
		}
		boolean isGrade = SearchProcess.isGrade(grade, database);
		return ((isSubject || isSubjectInCopy || isSubjectDBCopy) && isGrade) ? true
				: false;
	}

	private static char[] SOURCE_CHARACTERS = { 'À', 'Á', 'Â', 'Ã', 'È', 'É',
        'Ê', 'Ì', 'Í', 'Ò', 'Ó', 'Ô', 'Õ', 'Ù', 'Ú', 'Ý', 'à', 'á', 'â',
        'ã', 'è', 'é', 'ê', 'ì', 'í', 'ò', 'ó', 'ô', 'õ', 'ù', 'ú', 'ý',
        'Ă', 'ă', 'Đ', 'đ', 'Ĩ', 'ĩ', 'Ũ', 'ũ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ạ',
        'ạ', 'Ả', 'ả', 'Ấ', 'ấ', 'Ầ', 'ầ', 'Ẩ', 'ẩ', 'Ẫ', 'ẫ', 'Ậ', 'ậ',
        'Ắ', 'ắ', 'Ằ', 'ằ', 'Ẳ', 'ẳ', 'Ẵ', 'ẵ', 'Ặ', 'ặ', 'Ẹ', 'ẹ', 'Ẻ',
        'ẻ', 'Ẽ', 'ẽ', 'Ế', 'ế', 'Ề', 'ề', 'Ể', 'ể', 'Ễ', 'ễ', 'Ệ', 'ệ',
        'Ỉ', 'ỉ', 'Ị', 'ị', 'Ọ', 'ọ', 'Ỏ', 'ỏ', 'Ố', 'ố', 'Ồ', 'ồ', 'Ổ',
        'ổ', 'Ỗ', 'ỗ', 'Ộ', 'ộ', 'Ớ', 'ớ', 'Ờ', 'ờ', 'Ở', 'ở', 'Ỡ', 'ỡ',
        'Ợ', 'ợ', 'Ụ', 'ụ', 'Ủ', 'ủ', 'Ứ', 'ứ', 'Ừ', 'ừ', 'Ử', 'ử', 'Ữ',
        'ữ', 'Ự', 'ự', };

    private static char[] DESTINATION_CHARACTERS = { 'A', 'A', 'A', 'A', 'E',
        'E', 'E', 'I', 'I', 'O', 'O', 'O', 'O', 'U', 'U', 'Y', 'a', 'a',
        'a', 'a', 'e', 'e', 'e', 'i', 'i', 'o', 'o', 'o', 'o', 'u', 'u',
        'y', 'A', 'a', 'D', 'd', 'I', 'i', 'U', 'u', 'O', 'o', 'U', 'u',
        'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A',
        'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'E', 'e',
        'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E',
        'e', 'I', 'i', 'I', 'i', 'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o',
        'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o', 'O',
        'o', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u',
        'U', 'u', 'U', 'u', };

	public static char removeAccent(char ch) {
		int index = Arrays.binarySearch(SOURCE_CHARACTERS, ch);
		if (index >= 0) {
			ch = DESTINATION_CHARACTERS[index];
		}
		return ch;
	}

	public static String removeAccent(String s) {
		StringBuilder sb = new StringBuilder(s);
		for (int i = 0; i < sb.length(); i++) {
			sb.setCharAt(i, removeAccent(sb.charAt(i)));
		}
		return sb.toString();
	}
	public static void main(String[] args) {
		
		String [] some = removeAccent("ai cũng được").trim().split("[\\W]");
		System.out.println(some.length);
		for (int i = 0; i < some.length; i++) {
			if ("".equals(some[i].trim())){
				continue;
			}
	        System.out.println(some[i]);
        }
    }
}
