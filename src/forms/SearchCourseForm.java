package forms;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import model.bean.BaiDang;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class SearchCourseForm extends ActionForm {

	private String input;
	private String area;
	private String numberOfPeriod;
	private String numberOfStudent;
	private String minOfSalary;
	private String maxOfSalary;
	private ArrayList<BaiDang> courseList;

	public ArrayList<BaiDang> getCourseList() {
		return courseList;
	}

	public void setCourseList(ArrayList<BaiDang> courseList) {
		this.courseList = courseList;
	}

	public String getInput() {
		return input;
	}

	public void setInput(String input) {
		this.input = input;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getNumberOfPeriod() {
		return numberOfPeriod;
	}

	public void setNumberOfPeriod(String numberOfPeriod) {
		this.numberOfPeriod = numberOfPeriod;
	}

	public String getNumberOfStudent() {
		return numberOfStudent;
	}

	public void setNumberOfStudent(String numberOfStudent) {
		this.numberOfStudent = numberOfStudent;
	}

	public String getMinOfSalary() {
		return minOfSalary;
	}

	public void setMinOfSalary(String minOfSalary) {
		this.minOfSalary = minOfSalary;
	}

	public String getMaxOfSalary() {
		return maxOfSalary;
	}

	public void setMaxOfSalary(String maxOfSalary) {
		this.maxOfSalary = maxOfSalary;
	}
	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		try {
			request.setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
}
