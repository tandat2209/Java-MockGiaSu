package forms;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import model.bean.NguoiDung;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class NV_DSNguoiDungForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<NguoiDung> nguoiDungArray = new ArrayList<NguoiDung>();
	private String searchString;
	
	public String getSearchString() {
		return searchString;
	}
	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}
	public ArrayList<NguoiDung> getNguoiDungArray() {
		return nguoiDungArray;
	}
	public void setNguoiDungArray(ArrayList<NguoiDung> nguoiDungArray) {
		this.nguoiDungArray = nguoiDungArray;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	@Override
    public void reset(ActionMapping mapping, HttpServletRequest request) {
    	try {
			request.setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
    }

}
