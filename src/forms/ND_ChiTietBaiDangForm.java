/**
 * 
 */
package forms;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import model.bean.BaiDang;
import model.bean.KhuVuc;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * ND_ChiTietBaiDangForm.java
 *
 * Version 1.0
 *
 * Date: Aug 15, 2015
 *
 * Copyright
 *
 * Modification Logs: DATE AUTHOR DESCRIPTION
 * -----------------------------------------------------------------------
 * 11:10:22 PM HuyNV Create
 */
public class ND_ChiTietBaiDangForm extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String submit;
	private String maBD;
	private BaiDang baiDang;
	private String khuVuc;
	private String khuVucUpdateSD;
	private String loaiBD;
	private String tieuDe;
	private String mon;
	private String lop;
	private String moTaThem;
	private String maNguoiDang;
	private String thoiGianDang;
	private String trangThai;

	/*
	 * phan cho loai bai dang gia su
	 */
	private String tenGiaSu;
	private String gioiTinh;
	private String soDienThoai;
	private String noiCongTac;
	private String ngheNghiep;
	private String ngaySinh;
	private String queQuan;
	private String luongGS;

	/*
	 * phan cho loai bai dang suat day
	 */
	private String tenPhuHuynh;
	private String diaChi;
	private String soBuoiTrenTuan;
	private String soLuongHocSinh;
	private String luongSD;
	private String thoiGianDay;

	// phần của báo cáo sai phạm
	private String email;
	private String phone;
	private String content;

	// chua list maKV cua bai dang
	private String[] maKvArr;
	ArrayList<String> maKVList;

	// chua list cac bai dang co lien quan
	private ArrayList<BaiDang> lienquanList;

	// chua tat ca khu vuc trong bang KHUVUC
	private ArrayList<KhuVuc> kvList;

	// Constants
	private String[] tinh = { "Thanh Hoá", "Nghệ An", "Hà Tĩnh", "Quảng Bình",
	        "Quảng Trị", "Thừa Thiên Huế", "Đà Nẵng", "Quảng Nam",
	        "Quảng Ngãi", " Bình Định", "Phú Yên", "Khánh Hoà", "Kom Tum",
	        "Gia Lai", "Dak Lak" };
	private String[] nghe = { "Sinh viên", "Giáo viên", "Đã tốt nghiệp", "Khác" };

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.struts.action.ActionForm#reset(org.apache.struts.action.
	 * ActionMapping, javax.servlet.http.HttpServletRequest)
	 */
	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		try {
			request.setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.apache.struts.action.ActionForm#validate(org.apache.struts.action
	 * .ActionMapping, javax.servlet.http.HttpServletRequest)
	 */

	public String[] getNghe() {
		return nghe;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setNghe(String[] nghe) {
		this.nghe = nghe;
	}

	public String[] getTinh() {
		return tinh;
	}

	public void setTinh(String[] tinh) {
		this.tinh = tinh;
	}

	public String[] getMaKvArr() {
		return maKvArr;
	}

	public void setMaKvArr(String[] maKvArr) {
		this.maKvArr = maKvArr;
	}

	public String getKhuVucUpdateSD() {
		return khuVucUpdateSD;
	}

	public void setKhuVucUpdateSD(String khuVucUpdateSD) {
		this.khuVucUpdateSD = khuVucUpdateSD;
	}

	public ArrayList<String> getMaKVList() {
		return maKVList;
	}

	public void setMaKVList(ArrayList<String> maKVList) {
		this.maKVList = maKVList;
	}

	public ArrayList<KhuVuc> getKvList() {
		return kvList;
	}

	public void setKvList(ArrayList<KhuVuc> kvList) {
		this.kvList = kvList;
	}

	public String getSubmit() {
		return submit;
	}

	public void setSubmit(String submit) {
		this.submit = submit;
	}

	public String getLoaiBD() {
		return loaiBD;
	}

	public void setLoaiBD(String loaiBD) {
		this.loaiBD = loaiBD;
	}

	public String getTieuDe() {
		return tieuDe;
	}

	public void setTieuDe(String tieuDe) {
		this.tieuDe = tieuDe;
	}

	public String getMon() {
		return mon;
	}

	public void setMon(String mon) {
		this.mon = mon;
	}

	public String getLop() {
		return lop;
	}

	public void setLop(String lop) {
		this.lop = lop;
	}

	public String getMoTaThem() {
		return moTaThem;
	}

	public void setMoTaThem(String moTaThem) {
		this.moTaThem = moTaThem;
	}

	public String getMaNguoiDang() {
		return maNguoiDang;
	}

	public void setMaNguoiDang(String maNguoiDang) {
		this.maNguoiDang = maNguoiDang;
	}

	public String getThoiGianDang() {
		return thoiGianDang;
	}

	public void setThoiGianDang(String thoiGianDang) {
		this.thoiGianDang = thoiGianDang;
	}

	public String getTrangThai() {
		return trangThai;
	}

	public void setTrangThai(String trangThai) {
		this.trangThai = trangThai;
	}

	public String getTenGiaSu() {
		return tenGiaSu;
	}

	public void setTenGiaSu(String tenGiaSu) {
		this.tenGiaSu = tenGiaSu;
	}

	public String getGioiTinh() {
		return gioiTinh;
	}

	public void setGioiTinh(String gioiTinh) {
		this.gioiTinh = gioiTinh;
	}

	public String getSoDienThoai() {
		return soDienThoai;
	}

	public void setSoDienThoai(String soDienThoai) {
		this.soDienThoai = soDienThoai;
	}

	public String getNoiCongTac() {
		return noiCongTac;
	}

	public void setNoiCongTac(String noiCongTac) {
		this.noiCongTac = noiCongTac;
	}

	public String getNgheNghiep() {
		return ngheNghiep;
	}

	public void setNgheNghiep(String ngheNghiep) {
		this.ngheNghiep = ngheNghiep;
	}

	public String getNgaySinh() {
		return ngaySinh;
	}

	public void setNgaySinh(String ngaySinh) {
		this.ngaySinh = ngaySinh;
	}

	public String getQueQuan() {
		return queQuan;
	}

	public void setQueQuan(String queQuan) {
		this.queQuan = queQuan;
	}

	public String getLuongGS() {
		return luongGS;
	}

	public void setLuongGS(String luongGS) {
		this.luongGS = luongGS;
	}

	public String getTenPhuHuynh() {
		return tenPhuHuynh;
	}

	public void setTenPhuHuynh(String tenPhuHuynh) {
		this.tenPhuHuynh = tenPhuHuynh;
	}

	public String getDiaChi() {
		return diaChi;
	}

	public void setDiaChi(String diaChi) {
		this.diaChi = diaChi;
	}

	public String getSoBuoiTrenTuan() {
		return soBuoiTrenTuan;
	}

	public void setSoBuoiTrenTuan(String soBuoiTrenTuan) {
		this.soBuoiTrenTuan = soBuoiTrenTuan;
	}

	public String getSoLuongHocSinh() {
		return soLuongHocSinh;
	}

	public void setSoLuongHocSinh(String soLuongHocSinh) {
		this.soLuongHocSinh = soLuongHocSinh;
	}

	public String getLuongSD() {
		return luongSD;
	}

	public void setLuongSD(String luongSD) {
		this.luongSD = luongSD;
	}

	public String getThoiGianDay() {
		return thoiGianDay;
	}

	public void setThoiGianDay(String thoiGianDay) {
		this.thoiGianDay = thoiGianDay;
	}

	public BaiDang getBaiDang() {
		return baiDang;
	}

	public void setBaiDang(BaiDang baiDang) {
		this.baiDang = baiDang;
	}

	public String getKhuVuc() {
		return khuVuc;
	}

	public void setKhuVuc(String khuVuc) {
		this.khuVuc = khuVuc;
	}

	public String getMaBD() {
		return maBD;
	}

	public void setMaBD(String maBD) {
		this.maBD = maBD;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public ArrayList<BaiDang> getLienquanList() {
		return lienquanList;
	}

	public void setLienquanList(ArrayList<BaiDang> lienquanList) {
		this.lienquanList = lienquanList;
	}

}
