/**
 * 
 */
package forms;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;



/**				
 * AnhDaiDienForm.java			
 *				
 * Version 1.0				
 *				
 * Date: Aug 26, 2015			
 *				
 * Copyright 				
 *				
 * Modification Logs:				
 * DATE                 AUTHOR          DESCRIPTION				
 * -----------------------------------------------------------------------				
 * Aug 26, 2015        DatNVT           Create				
 */

public class AnhDaiDienForm extends ActionForm {
	private FormFile image;
	private String tenDangNhap;
	private String submit;
	
	public String getSubmit() {
		return submit;
	}

	public void setSubmit(String submit) {
		this.submit = submit;
	}

	public String getTenDangNhap() {
		return tenDangNhap;
	}

	public void setTenDangNhap(String tenDangNhap) {
		this.tenDangNhap = tenDangNhap;
	}

	public FormFile getImage() {
	    return image;
    }

	public void setImage(FormFile image) {
	    this.image = image;
    }
	
}
