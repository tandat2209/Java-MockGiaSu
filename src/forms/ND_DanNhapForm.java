/**
 * 
 */
package forms;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import common.StringProcess;

/**				
 * ND_DanNhapForm.java			
 *				
 * Version 1.0				
 *				
 * Date: Aug 13, 2015			
 *				
 * Copyright 				
 *				
 * Modification Logs:				
 * DATE                 AUTHOR          DESCRIPTION				
 * -----------------------------------------------------------------------				
 * Aug 13, 2015        HuyNV          Create				
 */

public class ND_DanNhapForm extends ActionForm{
	private String tenDangNhap;
	private String passWord;
	private String submit;
	private String errorMS;
	private boolean logined;
	
	public String getErrorMS() {
		return errorMS;
	}
	public void setErrorMS(String errorMS) {
		this.errorMS = errorMS;
	}
	public String getSubmit() {
		return submit;
	}
	public void setSubmit(String submit) {
		this.submit = submit;
	}
	public String getTenDangNhap() {
		return tenDangNhap;
	}
	public void setTenDangNhap(String tenDangNhap) {
		this.tenDangNhap = tenDangNhap;
	}
	public String getPassWord() {
		return passWord;
	}
	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}
	
	 @Override
	    public void reset(ActionMapping mapping, HttpServletRequest request) {
	    	try {
				request.setCharacterEncoding("UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
	    }
	public boolean isLogined() {
		return logined;
	}
	public void setLogined(boolean logined) {
		this.logined = logined;
	}
	    
}
