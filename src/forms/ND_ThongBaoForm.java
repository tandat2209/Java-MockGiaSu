/**
 * 
 */
package forms;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import model.bean.ThongBao;

/**
 * ND_ThongBaoForm.java
 *
 * Version 1.0
 *
 * Date: Aug 16, 2015
 *
 * Copyright 
 *
 * Modification Logs:
 * DATE                 AUTHOR          DESCRIPTION
 * -----------------------------------------------------------------------
 * 10:00:09 AM        	HuyNV          Create
 */
public class ND_ThongBaoForm extends ActionForm{
	private String tenDangNhap;
	private ArrayList<ThongBao>listThongBaoND;
	public ArrayList<ThongBao> getListThongBaoND() {
		return listThongBaoND;
	}

	public void setListThongBaoND(ArrayList<ThongBao> listThongBaoND) {
		this.listThongBaoND = listThongBaoND;
	}

	public String getTenDangNhap() {
		return tenDangNhap;
	}

	public void setTenDangNhap(String tenDangNhap) {
		this.tenDangNhap = tenDangNhap;
	}
	 @Override
	    public void reset(ActionMapping mapping, HttpServletRequest request) {
	    	try {
				request.setCharacterEncoding("UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
	    }
	    
}
