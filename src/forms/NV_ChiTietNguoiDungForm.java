/**
 * 
 */
package forms;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;

import model.bean.NguoiDung;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**				
 * ChiTietNguoiDungForm.java			
 *				
 * Version 1.0				
 *				
 * Date: Aug 18, 2015			
 *				
 * Copyright 				
 *				
 * Modification Logs:				
 * DATE                 AUTHOR          DESCRIPTION				
 * -----------------------------------------------------------------------				
 * Aug 18, 2015        HuyNV          Create				
 */

public class NV_ChiTietNguoiDungForm extends ActionForm {
	private NguoiDung nguoiDung;
	private String tenDangNhap;

	
	public String getTenDangNhap() {
		return tenDangNhap;
	}

	public void setTenDangNhap(String tenDangNhap) {
		this.tenDangNhap = tenDangNhap;
	}

	public NguoiDung getNguoiDung() {
		return nguoiDung;
	}

	public void setNguoiDung(NguoiDung nguoiDung) {
		this.nguoiDung = nguoiDung;
	}
	/* (non-Javadoc)
     * @see org.apache.struts.action.ActionForm#reset(org.apache.struts.action.ActionMapping, javax.servlet.http.HttpServletRequest)
     */
    @Override
    public void reset(ActionMapping mapping, HttpServletRequest request) {
        try {
            request.setCharacterEncoding("UTF-8");
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        super.reset(mapping, request);
    }
}
