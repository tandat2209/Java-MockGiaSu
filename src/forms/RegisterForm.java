package forms;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class RegisterForm extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String account;
	private String password;
	private String passwordConfirm;
	private String username;
	private String gender;
	private String birthday;
	private String email;
	private String phone;
	private String error;
	private String submit;

	public String getSubmit() {
		return submit;
	}

	public void setSubmit(String submit) {
		this.submit = submit;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordConfirm() {
		return passwordConfirm;
	}

	public void setPasswordConfirm(String passwordConfirm) {
		this.passwordConfirm = passwordConfirm;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		try {
			request.setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	@Override
	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		ActionErrors error = new ActionErrors();
		System.out.println(gender);
		if (isNullAccount()) {
			error.add("nullAccountError", new ActionMessage("error.nullError",
					"Account"));
		}

		if (isNullPassword()) {
			error.add("nullPasswordError", new ActionMessage("error.nullError",
					"Password"));
		}
		if (isNullUsername()) {
			error.add("nullUsernameError", new ActionMessage("error.nullError",
					"Username"));
		}
		if (isNullGender()) {
			error.add("nullGenderError", new ActionMessage("error.nullError",
					"Gender"));
		}
		if (isEmptyAccount()) {
			error.add("emptyAccountError", new ActionMessage("error.emptyError",
					"Account"));
		}
		if (isEmptyPassword()) {
			error.add("emptyPasswordError", new ActionMessage("error.emptyError",
					"Password"));
		}
		if (isEmptyUsername()) {
			error.add("emptyUsernameError", new ActionMessage("error.emptyError",
					"Username"));
		}
		
		if (!isMatch()) {
			error.add("errorMatch", new ActionMessage("error.matchError"));
			return error;
		}
		if (!isPhone()) {
			error.add("errorPhone", new ActionMessage("error.phoneError"));
		}
		if (!isBirthday()) {
			error.add("errorBirthday", new ActionMessage("error.birthdayError"));
		}

		// Validate email
		if (!isEmail()) {
			error.add("errorEmail", new ActionMessage("error.validError", "Email"));
		}
		return error;
	}

	private boolean isNullGender() {
		return gender==null;
	}

	private boolean isMatch() {
		boolean check = false;
		
		if (password.equals(passwordConfirm)) {
			check = true;
		}
		return check;
	}

	private boolean isEmail() {
		boolean check = false;
		String regex = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		if (email.matches(regex)) {
			check = true;
		}
		return check;
	}

	private boolean isBirthday() {
		boolean check = false;
		String regex = "(([012][0-9])||([3][01]))[/-](([0][0-9])||[1][0-2])[/-](19||20)[0-9][0-9]";
		if (birthday.matches(regex)) {
			check = true;
		}
		return check;
	}

	private boolean isPhone() {
		return phone.matches("[0-9]{10,11}") ? true : false;
	}

	private boolean isEmptyAccount() {
		return ("".equals(account)) ? true : false;
	}

	private boolean isEmptyPassword() {
		return ("".equals(password)) ? true : false;
	}

	private boolean isEmptyUsername() {
		return ("".equals(username)) ? true : false;
	}


	private boolean isNullAccount() {
		return (account == null) ? true : false;
	}

	private boolean isNullPassword() {
		return (account == null) ? true : false;
	}

	private boolean isNullUsername() {
		return (username == null) ? true : false;
	}
}
