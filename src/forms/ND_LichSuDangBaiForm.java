/**
 * 
 */
package forms;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import model.bean.BaiDang;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**				
 * ND_LichSuDangBai.java			
 *				
 * Version 1.0				
 *				
 * Date: Aug 14, 2015			
 *				
 * Copyright 				
 *				
 * Modification Logs:				
 * DATE                 AUTHOR          DESCRIPTION				
 * -----------------------------------------------------------------------				
 * Aug 14, 2015        HuyNV          Create				
 */

public class ND_LichSuDangBaiForm extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	ArrayList<BaiDang> listbd;
	private String trangThai;
	private String loai;
	private String soBaiDang;
	
	public String getSoBaiDang() {
		return soBaiDang;
	}

	public String getLoai() {
		return loai;
	}

	public void setLoai(String loai) {
		this.loai = loai;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getTrangThai() {
		return trangThai;
	}

	public void setTrangThai(String trangThai) {
		this.trangThai = trangThai;
	}

	public ArrayList<BaiDang> getListbd() {
		return listbd;
	}

	public void setListbd(ArrayList<BaiDang> listbd) {
		this.listbd = listbd;
		this.soBaiDang = listbd.size()+"";
	}
	
	 @Override
	    public void reset(ActionMapping mapping, HttpServletRequest request) {
	    	try {
				request.setCharacterEncoding("UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
	    }
	    
	
}
