package forms;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import model.bean.BaiDang;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * @author DatNVT
 *
 */
public class NV_DSDuyetBaiDangForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String tenDangNhap;
	private String loai;
	private String trangThai;
	private ArrayList<BaiDang> baiDangList;
	
	public String getTenDangNhap() {
		return tenDangNhap;
	}
	public void setTenDangNhap(String tenDangNhap) {
		this.tenDangNhap = tenDangNhap;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public ArrayList<BaiDang> getBaiDangList() {
		return baiDangList;
	}
	public void setBaiDangList(ArrayList<BaiDang> baiDangList) {
		this.baiDangList = baiDangList;
	}
    public String getLoai() {
        return loai;
    }
    public void setLoai(String loai) {
        this.loai = loai;
    }
    public String getTrangThai() {
		return trangThai;
	}
	public void setTrangThai(String trangThai) {
		this.trangThai = trangThai;
	}
  
	@Override
    public void reset(ActionMapping mapping, HttpServletRequest request) {
    	try {
			request.setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
    }
	
}
