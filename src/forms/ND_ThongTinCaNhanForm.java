/**
 * 
 */
package forms;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import model.bean.NguoiDung;

/**				
 * ND_ThongTinCaNhanForm.java			
 *				
 * Version 1.0				
 *				
 * Date: Aug 13, 2015			
 *				
 * Copyright 				
 *				
 * Modification Logs:				
 * DATE                 AUTHOR          DESCRIPTION				
 * -----------------------------------------------------------------------				
 * Aug 13, 2015        HuyNV          Create				
 */

public class ND_ThongTinCaNhanForm extends ActionForm{
	/**
	 * 
	 */
    private static final long serialVersionUID = 1L;
	private String maND;
	private String tenND;
	private String gioiTinh;
	private String ngaySinh;
	private String soDienThoai;
	private String email;
	private String tenDangNhap;
	private String matKhau;
	private String submit;
	private String matKhauMoi;
	private String matKhauCu;
	private String matKhauXN;
	private String errorMS;
	private String successMS;
	private String anhDaiDien;
	
	
	public String getErrorMS() {
		return errorMS;
	}
	public void setErrorMS(String errorMS) {
		this.errorMS = errorMS;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getMatKhauMoi() {
		return matKhauMoi;
	}
	public void setMatKhauMoi(String matKhauMoi) {
		this.matKhauMoi = matKhauMoi;
	}
	public String getMatKhauCu() {
		return matKhauCu;
	}
	public void setMatKhauCu(String matKhauCu) {
		this.matKhauCu = matKhauCu;
	}
	public String getMatKhauXN() {
		return matKhauXN;
	}
	public void setMatKhauXN(String matKhauXN) {
		this.matKhauXN = matKhauXN;
	}
	public String getSubmit() {
		return submit;
	}
	public void setSubmit(String submit) {
		this.submit = submit;
	}
	public String getNgaySinh() {
		return ngaySinh;
	}
	public void setNgaySinh(String ngaySinh) {
		this.ngaySinh = ngaySinh;
	}
	public String getMaND() {
		return maND;
	}
	public void setMaND(String maND) {
		this.maND = maND;
	}
	public String getTenND() {
		return tenND;
	}
	public void setTenND(String tenND) {
		this.tenND = tenND;
	}
	public String getGioiTinh() {
		return gioiTinh;
	}
	public void setGioiTinh(String gioiTinh) {
		this.gioiTinh = gioiTinh;
	}
	public String getSoDienThoai() {
		return soDienThoai;
	}
	public void setSoDienThoai(String soDienThoai) {
		this.soDienThoai = soDienThoai;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTenDangNhap() {
		return tenDangNhap;
	}
	public void setTenDangNhap(String tenDangNhap) {
		this.tenDangNhap = tenDangNhap;
	}
	public String getMatKhau() {
		return matKhau;
	}
	public void setMatKhau(String matKhau) {
		this.matKhau = matKhau;
	}
	 @Override
	    public void reset(ActionMapping mapping, HttpServletRequest request) {
	    	try {
				request.setCharacterEncoding("UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
	    }

    public void setSuccessMS(String successMS) {
	    this.successMS = successMS;
    }
	public String getSuccessMS() {
		return successMS;
	}
	public String getAnhDaiDien() {
	    return anhDaiDien;
    }
	public void setAnhDaiDien(String anhDaiDien) {
	    this.anhDaiDien = anhDaiDien;
    }
	    
}
