/**
 * 
 */
package forms;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import model.bean.NhanVien;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * Admin_DSNhanVienFrom.java
 * 
 * Version 1.0
 * 
 * Date: Aug 13, 2015
 * 
 * Copyright
 * 
 * Modification Logs: DATE AUTHOR DESCRIPTION
 * ----------------------------------------------------------------------- 13,
 * 2015 HuyNV Create
 */

public class Admin_DSNhanVienForm extends ActionForm {
	private ArrayList<NhanVien> listNV;
	private String searchName;
	private String submit;

	
	
	public String getSubmit() {
		return submit;
	}

	public void setSubmit(String submit) {
		this.submit = submit;
	}

	public String getSearchName() {
		return searchName;
	}

	public void setSearchName(String searchName) {
		this.searchName = searchName;
	}

	public ArrayList<NhanVien> getListNV() {
		return listNV;
	}

	public void setListNV(ArrayList<NhanVien> listNV) {
		this.listNV = listNV;
	}

}
