/**
 * 
 */
package forms;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import model.bean.KhuVuc;

/**
 * ND_ChiTietBDGiaSuForm.java
 *
 * Version 1.0
 *
 * Date: Aug 17, 2015
 *
 * Copyright 
 *
 * Modification Logs:
 * DATE                 AUTHOR          DESCRIPTION
 * -----------------------------------------------------------------------
 * 8:44:02 PM        	HuyNV          Create
 */
public class ND_BaiDangGiaSuForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String submit;
	private String maBD;
    private String loaiBD;
    private String tieuDe;
    private String mon;
    private String lop;
    private String thoiGianDay;
    private String soDienThoai;
    private String moTaThem;
    private String maNguoiDang;
    private String thoiGianDang;
    private String trangThai;
    
    
    private String tenGiaSu;
    private String gioiTinh;
    private String noiCongTac;
    private String ngheNghiep;
    private String ngaySinh;
    private String queQuan;
    private String luongGS;
    
    // arrayList khu vuc;
    private String[] arrTemp;
    private ArrayList<KhuVuc> khuVucList;
    private ArrayList<String> maKhuVucList;
    
    @Override
    public void reset(ActionMapping mapping, HttpServletRequest request) {
    	try {
			request.setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
    }
    
    
	public String[] getArrTemp() {
		return arrTemp;
	}
	public void setArrTemp(String[] arrTemp) {
		this.arrTemp = arrTemp;
	}
	public String getSubmit() {
		return submit;
	}
	public void setSubmit(String submit) {
		this.submit = submit;
	}
	public String getMaBD() {
		return maBD;
	}
	public void setMaBD(String maBD) {
		this.maBD = maBD;
	}
	public String getLoaiBD() {
		return loaiBD;
	}
	public void setLoaiBD(String loaiBD) {
		this.loaiBD = loaiBD;
	}
	public String getTieuDe() {
		return tieuDe;
	}
	public void setTieuDe(String tieuDe) {
		this.tieuDe = tieuDe;
	}
	public String getMon() {
		return mon;
	}
	public void setMon(String mon) {
		this.mon = mon;
	}
	public String getLop() {
		return lop;
	}
	public void setLop(String lop) {
		this.lop = lop;
	}
	public String getThoiGianDay() {
		return thoiGianDay;
	}
	public void setThoiGianDay(String thoiGianDay) {
		this.thoiGianDay = thoiGianDay;
	}
	public String getSoDienThoai() {
		return soDienThoai;
	}
	public void setSoDienThoai(String soDienThoai) {
		this.soDienThoai = soDienThoai;
	}
	public String getMoTaThem() {
		return moTaThem;
	}
	public void setMoTaThem(String moTaThem) {
		this.moTaThem = moTaThem;
	}
	public String getMaNguoiDang() {
		return maNguoiDang;
	}
	public void setMaNguoiDang(String maNguoiDang) {
		this.maNguoiDang = maNguoiDang;
	}
	public String getThoiGianDang() {
		return thoiGianDang;
	}
	public void setThoiGianDang(String thoiGianDang) {
		this.thoiGianDang = thoiGianDang;
	}
	public String getTrangThai() {
		return trangThai;
	}
	public void setTrangThai(String trangThai) {
		this.trangThai = trangThai;
	}
	public String getTenGiaSu() {
		return tenGiaSu;
	}
	public void setTenGiaSu(String tenGiaSu) {
		this.tenGiaSu = tenGiaSu;
	}
	public String getGioiTinh() {
		return gioiTinh;
	}
	public void setGioiTinh(String gioiTinh) {
		this.gioiTinh = gioiTinh;
	}
	public String getNoiCongTac() {
		return noiCongTac;
	}
	public void setNoiCongTac(String noiCongTac) {
		this.noiCongTac = noiCongTac;
	}
	public String getNgheNghiep() {
		return ngheNghiep;
	}
	public void setNgheNghiep(String ngheNghiep) {
		this.ngheNghiep = ngheNghiep;
	}
	public String getNgaySinh() {
		return ngaySinh;
	}
	public void setNgaySinh(String ngaySinh) {
		this.ngaySinh = ngaySinh;
	}
	public String getQueQuan() {
		return queQuan;
	}
	public void setQueQuan(String queQuan) {
		this.queQuan = queQuan;
	}
	public String getLuongGS() {
		return luongGS;
	}
	public void setLuongGS(String luongGS) {
		this.luongGS = luongGS;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public ArrayList<KhuVuc> getKhuVucList() {
		return khuVucList;
	}
	public void setKhuVucList(ArrayList<KhuVuc> khuVucList) {
		this.khuVucList = khuVucList;
	}
	public ArrayList<String> getMaKhuVucList() {
		return maKhuVucList;
	}
	public void setMaKhuVucList(ArrayList<String> maKhuVucList) {
		this.maKhuVucList = maKhuVucList;
	}
    
    

}
