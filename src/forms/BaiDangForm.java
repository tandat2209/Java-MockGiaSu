package forms;

import java.util.ArrayList;

import model.bean.BaiDang;
import model.bean.KhuVuc;

import org.apache.struts.action.ActionForm;

/**
 * @author DatNVT
 *
 */
public class BaiDangForm extends ActionForm {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private String maBD;
    private String loaiBD;
    private BaiDang baiDang;
    private ArrayList<KhuVuc> khuVucList;
    private String xuly;
    public BaiDang getBaiDang() {
        return baiDang;
    }
    public void setBaiDang(BaiDang baiDang) {
        this.baiDang = baiDang;
    }
    public static long getSerialversionuid() {
        return serialVersionUID;
    }
    public String getMaBD() {
        return maBD;
    }
    public void setMaBD(String maBD) {
        this.maBD = maBD;
    }
    public String getLoaiBD() {
        return loaiBD;
    }
    public void setLoaiBD(String loaiBD) {
        this.loaiBD = loaiBD;
    }
    public ArrayList<KhuVuc> getKhuVucList() {
        return khuVucList;
    }
    public void setKhuVucList(ArrayList<KhuVuc> khuVucList) {
        this.khuVucList = khuVucList;
    }
	public String getXuly() {
		return xuly;
	}
	public void setXuly(String xuly) {
		this.xuly = xuly;
	}
    
}
