package forms;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import model.bean.ThongBao;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class NV_DSThongBaoForm extends ActionForm {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private ArrayList<ThongBao> thongBaoList;
    public ArrayList<ThongBao> getThongBaoList() {
        return thongBaoList;
    }
    public void setThongBaoList(ArrayList<ThongBao> thongBaoList) {
        this.thongBaoList = thongBaoList;
    }
    public static long getSerialversionuid() {
        return serialVersionUID;
    }
    @Override
    public void reset(ActionMapping mapping, HttpServletRequest request) {
    	try {
			request.setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
    }
}
