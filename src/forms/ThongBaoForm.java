package forms;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;

import model.bean.ThongBao;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * @author DatNVT
 *
 */
public class ThongBaoForm extends ActionForm {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private String maTB;
    private ThongBao thongBao;
    private String xuly;
    public String getMaTB() {
        return maTB;
    }
    public void setMaTB(String maTB) {
        this.maTB = maTB;
    }
    public ThongBao getThongBao() {
        return thongBao;
    }
    public void setThongBao(ThongBao thongBao) {
        this.thongBao = thongBao;
    }
    public static long getSerialversionuid() {
        return serialVersionUID;
    }
	public String getXuly() {
		return xuly;
	}
	public void setXuly(String xuly) {
		this.xuly = xuly;
	}
    
	@Override
    public void reset(ActionMapping mapping, HttpServletRequest request) {
    	try {
			request.setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
    }

}
