package forms;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import model.bean.NguoiDung;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class DisplayForm extends ActionForm{
	private ArrayList<NguoiDung> userList;

	public ArrayList<NguoiDung> getUserList() {
		return userList;
	}

	public void setUserList(ArrayList<NguoiDung> userList) {
		this.userList = userList;
	}

	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		try {
			request.setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
	
}
