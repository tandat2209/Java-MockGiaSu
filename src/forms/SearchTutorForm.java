package forms;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import model.bean.BaiDang;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class SearchTutorForm extends ActionForm {
	private String input;
	private String career;
	private String hometown;
	private String gender;
	private String minOfWage;
	private String maxOfWage;
	private String[] area;
	private ArrayList<BaiDang> tutorList;

	public ArrayList<BaiDang> getTutorList() {
		return tutorList;
	}

	public void setTutorList(ArrayList<BaiDang> tutorList) {
		this.tutorList = tutorList;
	}

	public String[] getArea() {
		return area;
	}

	public void setArea(String[] area) {
		this.area = area;
	}

	public String getInput() {
		return input;
	}

	public void setInput(String input) {
		this.input = input;
	}

	public String getCareer() {
		return career;
	}

	public void setCareer(String career) {
		this.career = career;
	}

	public String getHometown() {
		return hometown;
	}

	public void setHometown(String hometown) {
		this.hometown = hometown;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMinOfWage() {
		return minOfWage;
	}

	public void setMinOfWage(String minOfWage) {
		this.minOfWage = minOfWage;
	}

	public String getMaxOfWage() {
		return maxOfWage;
	}

	public void setMaxOfWage(String maxOfWage) {
		this.maxOfWage = maxOfWage;
	}
	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		try {
			request.setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
}
