package forms;

import org.apache.struts.action.ActionForm;

public class DetailInfoForm extends ActionForm {
	private String maBD;
	private String loaiBD;
	private String tieuDe;
	private String mon;
	private String lop;
	private String moTaThem;
	private String maNguoiDang;
	private String thoiGianDang;
	private String trangThai;

	public String getMaBD() {
		return maBD;
	}

	public void setMaBD(String maBD) {
		this.maBD = maBD;
	}

	public String getLoaiBD() {
		return loaiBD;
	}

	public void setLoaiBD(String loaiBD) {
		this.loaiBD = loaiBD;
	}

	public String getMon() {
		return mon;
	}

	public void setMon(String mon) {
		this.mon = mon;
	}

	public String getLop() {
		return lop;
	}

	public void setLop(String lop) {
		this.lop = lop;
	}

	public String getMoTaThem() {
		return moTaThem;
	}

	public void setMoTaThem(String moTaThem) {
		this.moTaThem = moTaThem;
	}

	public String getMaNguoiDang() {
		return maNguoiDang;
	}

	public void setMaNguoiDang(String maNguoiDang) {
		this.maNguoiDang = maNguoiDang;
	}

	public String getThoiGianDang() {
		return thoiGianDang;
	}

	public void setThoiGianDang(String thoiGianDang) {
		this.thoiGianDang = thoiGianDang;
	}

	public String getTrangThai() {
		return trangThai;
	}

	public void setTrangThai(String trangThai) {
		this.trangThai = trangThai;
	}

	public String getTieuDe() {
		return tieuDe;
	}

	public void setTieuDe(String tieuDe) {
		this.tieuDe = tieuDe;
	}

}
