package forms;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;


public class Admin_NhanVienForm extends ActionForm {

	private String tenDangNhap;
	private String matKhau;
	private String matKhauXN;
	private String tenNV;
	private String gioiTinh;
	private String ngaySinh;
	private String diaChi;
	private String email;
	private String soDienThoai;
	private String submitSua;
	private String submitThem;

	private String errTenDangNhap;
	private String errMatKhau;
	private String errNgaySinh;
	private String errEmail;
	private String errSoDienThoai;

	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		try {
			request.setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e) {
		}
	}

	
	
	public String getMatKhauXN() {
		return matKhauXN;
	}



	public void setMatKhauXN(String matKhauXN) {
		this.matKhauXN = matKhauXN;
	}



	public String getSubmitThem() {
		return submitThem;
	}

	public void setSubmitThem(String submitThem) {
		this.submitThem = submitThem;
	}

	public String getSubmitSua() {
		return submitSua;
	}

	public void setSubmitSua(String submitSua) {
		this.submitSua = submitSua;
	}

	public String getTenDangNhap() {
		return tenDangNhap;
	}

	public void setTenDangNhap(String tenDangNhap) {
		this.tenDangNhap = tenDangNhap;
	}

	public String getMatKhau() {
		return matKhau;
	}

	public void setMatKhau(String matKhau) {
		this.matKhau = matKhau;
	}

	public String getTenNV() {
		return tenNV;
	}

	public void setTenNV(String tenNV) {
		this.tenNV = tenNV;
	}

	public String getGioiTinh() {
		return gioiTinh;
	}

	public void setGioiTinh(String gioiTinh) {
		this.gioiTinh = gioiTinh;
	}

	public String getNgaySinh() {
		return ngaySinh;
	}

	public void setNgaySinh(String ngaySinh) {
		this.ngaySinh = ngaySinh;
	}

	public String getDiaChi() {
		return diaChi;
	}

	public void setDiaChi(String diaChi) {
		this.diaChi = diaChi;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSoDienThoai() {
		return soDienThoai;
	}

	public void setSoDienThoai(String soDienThoai) {
		this.soDienThoai = soDienThoai;
	}

	public String getErrTenDangNhap() {
		return errTenDangNhap;
	}

	public void setErrTenDangNhap(String errTenDangNhap) {
		this.errTenDangNhap = errTenDangNhap;
	}

	public String getErrMatKhau() {
		return errMatKhau;
	}

	public void setErrMatKhau(String errMatKhau) {
		this.errMatKhau = errMatKhau;
	}

	public String getErrNgaySinh() {
		return errNgaySinh;
	}

	public void setErrNgaySinh(String errNgaySinh) {
		this.errNgaySinh = errNgaySinh;
	}

	public String getErrEmail() {
		return errEmail;
	}

	public void setErrEmail(String errEmail) {
		this.errEmail = errEmail;
	}

	public String getErrSoDienThoai() {
		return errSoDienThoai;
	}

	public void setErrSoDienThoai(String errSoDienThoai) {
		this.errSoDienThoai = errSoDienThoai;
	}

}
