USE [master]
GO
/****** Object:  Database [Mock]    Script Date: 08/15/2015 15:38:34 ******/
CREATE DATABASE [Mock] ON  PRIMARY 
( NAME = N'Mock', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10.MSSQLSERVER\MSSQL\DATA\Mock.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Mock_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10.MSSQLSERVER\MSSQL\DATA\Mock_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Mock] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Mock].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Mock] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [Mock] SET ANSI_NULLS OFF
GO
ALTER DATABASE [Mock] SET ANSI_PADDING OFF
GO
ALTER DATABASE [Mock] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [Mock] SET ARITHABORT OFF
GO
ALTER DATABASE [Mock] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [Mock] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [Mock] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [Mock] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [Mock] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [Mock] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [Mock] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [Mock] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [Mock] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [Mock] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [Mock] SET  DISABLE_BROKER
GO
ALTER DATABASE [Mock] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [Mock] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [Mock] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [Mock] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [Mock] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [Mock] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [Mock] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [Mock] SET  READ_WRITE
GO
ALTER DATABASE [Mock] SET RECOVERY FULL
GO
ALTER DATABASE [Mock] SET  MULTI_USER
GO
ALTER DATABASE [Mock] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [Mock] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'Mock', N'ON'
GO
USE [Mock]
GO
/****** Object:  Table [dbo].[NGUOIDUNG]    Script Date: 08/15/2015 15:38:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NGUOIDUNG](
	[tenDangNhap] [varchar](50) NOT NULL,
	[matKhau] [varchar](50) NULL,
	[hoTen] [nvarchar](50) NULL,
	[gioiTinh] [nvarchar](5) NULL,
	[ngaySinh] [date] NULL,
	[soDienThoai] [varchar](20) NULL,
	[email] [varchar](50) NULL,
	[soLanCamOn] [int] NULL,
	[soLanCanhBao] [int] NULL,
	[soBaiDang] [int] NULL,
	[trangThai] [nvarchar](50) NULL,
 CONSTRAINT [PK_NGUOIDUNG] PRIMARY KEY CLUSTERED 
(
	[tenDangNhap] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[NGUOIDUNG] ([tenDangNhap], [matKhau], [hoTen], [gioiTinh], [ngaySinh], [soDienThoai], [email], [soLanCamOn], [soLanCanhBao], [soBaiDang], [trangThai]) VALUES (N'DatNVT', N'12345678', N'Nguyen Van Tan Dat', N'Nam', CAST(0x801C0B00 AS Date), N'0121212121', N'datnguyen@gmail.com', 0, 0, 0, N'Binh thuong')
INSERT [dbo].[NGUOIDUNG] ([tenDangNhap], [matKhau], [hoTen], [gioiTinh], [ngaySinh], [soDienThoai], [email], [soLanCamOn], [soLanCanhBao], [soBaiDang], [trangThai]) VALUES (N'HuyNV', N'123456', N'Nguyen Viet Huy', N'Nam', CAST(0x441B0B00 AS Date), N'429873568376', N'Huy@gmail.com', 0, 0, 2, N'Binh thuong')
/****** Object:  Table [dbo].[KHUVUC]    Script Date: 08/15/2015 15:38:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KHUVUC](
	[maKV] [int] IDENTITY(1,1) NOT NULL,
	[tenKV] [nvarchar](50) NULL,
 CONSTRAINT [PK_KHUVUC] PRIMARY KEY CLUSTERED 
(
	[maKV] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[KHUVUC] ON
INSERT [dbo].[KHUVUC] ([maKV], [tenKV]) VALUES (1, N'Lien Chieu')
INSERT [dbo].[KHUVUC] ([maKV], [tenKV]) VALUES (2, N'Thanh Khe')
INSERT [dbo].[KHUVUC] ([maKV], [tenKV]) VALUES (3, N'Hai Chau')
INSERT [dbo].[KHUVUC] ([maKV], [tenKV]) VALUES (4, N'Hoa Vang')
INSERT [dbo].[KHUVUC] ([maKV], [tenKV]) VALUES (5, N'Ngu Hanh Son')
INSERT [dbo].[KHUVUC] ([maKV], [tenKV]) VALUES (6, N'Son Tra')
INSERT [dbo].[KHUVUC] ([maKV], [tenKV]) VALUES (7, N'Cam Le')
SET IDENTITY_INSERT [dbo].[KHUVUC] OFF
/****** Object:  Table [dbo].[NHANVIEN]    Script Date: 08/15/2015 15:38:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NHANVIEN](
	[tenDangNhap] [varchar](50) NOT NULL,
	[matKhau] [varchar](50) NULL,
	[tenNV] [nvarchar](50) NULL,
	[gioiTinh] [nvarchar](5) NULL,
	[ngaySinh] [date] NULL,
	[diaChi] [nvarchar](200) NULL,
	[email] [varchar](50) NULL,
	[soDienThoai] [varchar](20) NULL,
	[quyen] [int] NULL,
 CONSTRAINT [PK_NHANVIEN] PRIMARY KEY CLUSTERED 
(
	[tenDangNhap] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[NHANVIEN] ([tenDangNhap], [matKhau], [tenNV], [gioiTinh], [ngaySinh], [diaChi], [email], [soDienThoai], [quyen]) VALUES (N'admin', N'12345678', NULL, NULL, NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[NHANVIEN] ([tenDangNhap], [matKhau], [tenNV], [gioiTinh], [ngaySinh], [diaChi], [email], [soDienThoai], [quyen]) VALUES (N'dagger', N'12345678', N'Nguyen Van Tan Dat', N'Nam', CAST(0x801C0B00 AS Date), N'Da Nang', N'dracudakid@live.com', N'01214521499', 0)
/****** Object:  Trigger [triggernv]    Script Date: 08/15/2015 15:38:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create trigger [dbo].[triggernv] 
on [dbo].[NHANVIEN]
after insert
as update NHANVIEN set quyen=0
	where tenDangNhap=(select tenDangNhap from inserted)
GO
/****** Object:  Table [dbo].[BAIDANG]    Script Date: 08/15/2015 15:38:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BAIDANG](
	[maBD] [int] IDENTITY(1,1) NOT NULL,
	[loaiBD] [nvarchar](50) NULL,
	[tieuDe] [nvarchar](50) NULL,
	[mon] [nvarchar](50) NULL,
	[lop] [nvarchar](50) NULL,
	[thoiGianDay] [nvarchar](50) NULL,
	[soDienThoai] [varchar](50) NULL,
	[moTaThem] [nvarchar](max) NULL,
	[maNguoiDang] [varchar](50) NULL,
	[thoiGianDang] [datetime] NULL,
	[trangThai] [nvarchar](50) NULL,
	[tenGiaSu] [nvarchar](50) NULL,
	[gioiTinh] [nvarchar](10) NULL,
	[noiCongTac] [nvarchar](50) NULL,
	[ngheNghiep] [nvarchar](50) NULL,
	[ngaySinh] [date] NULL,
	[queQuan] [nvarchar](50) NULL,
	[luongGS] [money] NULL,
	[tenPhuHuynh] [nvarchar](50) NULL,
	[diaChi] [nvarchar](50) NULL,
	[soBuoiTrenTuan] [int] NULL,
	[soLuongHocSinh] [int] NULL,
	[luongSD] [money] NULL,
 CONSTRAINT [PK_BAIDANG] PRIMARY KEY CLUSTERED 
(
	[maBD] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[BAIDANG] ON
INSERT [dbo].[BAIDANG] ([maBD], [loaiBD], [tieuDe], [mon], [lop], [thoiGianDay], [soDienThoai], [moTaThem], [maNguoiDang], [thoiGianDang], [trangThai], [tenGiaSu], [gioiTinh], [noiCongTac], [ngheNghiep], [ngaySinh], [queQuan], [luongGS], [tenPhuHuynh], [diaChi], [soBuoiTrenTuan], [soLuongHocSinh], [luongSD]) VALUES (1, N'Gia su', N'Toan 10,11', N'Toan', N'10, 11', N'T2-T4-T6 8pm-11pm', N'0121212111', N'Gia su dep trai vui tinh day', N'HuyNV', CAST(0x0000A4F500B3996D AS DateTime), N'Cho duyet', N'Nguyen Van Huy', N'Nu', N'Bach Khoa Da Nang', N'Sinh vien', CAST(0x801C0B00 AS Date), N'Da Nang', 1000000.0000, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BAIDANG] ([maBD], [loaiBD], [tieuDe], [mon], [lop], [thoiGianDay], [soDienThoai], [moTaThem], [maNguoiDang], [thoiGianDang], [trangThai], [tenGiaSu], [gioiTinh], [noiCongTac], [ngheNghiep], [ngaySinh], [queQuan], [luongGS], [tenPhuHuynh], [diaChi], [soBuoiTrenTuan], [soLuongHocSinh], [luongSD]) VALUES (2, N'Suat day', N'Ly 10', N'Ly', N'10', N'T2-T4-T6 8pm-11pm', NULL, N'Can tim mot gia su nu tinh sexy', N'HuyNV', CAST(0x0000A4F500B43408 AS DateTime), N'Cho duyet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Nguyen Viet Huy', N'195 Nguyen Luong Bang', 2, 2, 100000.0000)
SET IDENTITY_INSERT [dbo].[BAIDANG] OFF
/****** Object:  Trigger [nguoidungTrigger]    Script Date: 08/15/2015 15:38:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[nguoidungTrigger] on [dbo].[NGUOIDUNG]
AFTER INSERT
AS
	UPDATE NGUOIDUNG
	SET soLanCamOn = 0, soLanCanhBao = 0, soBaiDang = 0, trangThai = 'Binh thuong'
	WHERE tenDangNhap = (SELECT tenDangNhap from inserted)
GO
/****** Object:  Table [dbo].[THONGBAO]    Script Date: 08/15/2015 15:38:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[THONGBAO](
	[maTB] [int] IDENTITY(1,1) NOT NULL,
	[maBaiDang] [int] NULL,
	[ngayBao] [datetime] NULL,
	[email] [varchar](50) NULL,
	[soDienThoai] [varchar](50) NULL,
	[noiDung] [varchar](max) NULL,
	[trangThai] [nvarchar](50) NULL,
 CONSTRAINT [PK_THONGBAO] PRIMARY KEY CLUSTERED 
(
	[maTB] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[THONGBAO] ON
INSERT [dbo].[THONGBAO] ([maTB], [maBaiDang], [ngayBao], [email], [soDienThoai], [noiDung], [trangThai]) VALUES (1, 1, CAST(0x0000A4F500B4938A AS DateTime), N'ohyeah@yeha.com', N'0945461124', N'Anh thich em nhu xua ', N'Cho xu ly')
SET IDENTITY_INSERT [dbo].[THONGBAO] OFF
/****** Object:  Trigger [baidangTrigger]    Script Date: 08/15/2015 15:38:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[baidangTrigger] on [dbo].[BAIDANG]
AFTER INSERT
AS
	UPDATE dbo.BAIDANG
	SET thoiGianDang = GETDATE(),
		trangThai = 'Cho duyet'
	WHERE maBD = (SELECT maBD from inserted)
	UPDATE dbo.NGUOIDUNG
	SET soBaiDang = soBaiDang + 1
	WHERE tenDangNhap = (SELECT maNguoiDang from inserted)
GO
/****** Object:  Table [dbo].[BAIDANG_KHUVUC]    Script Date: 08/15/2015 15:38:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BAIDANG_KHUVUC](
	[maBaiDang] [int] NOT NULL,
	[maKhuVuc] [int] NOT NULL,
 CONSTRAINT [PK_BAIDANG_KHUVUC] PRIMARY KEY CLUSTERED 
(
	[maBaiDang] ASC,
	[maKhuVuc] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[BAIDANG_KHUVUC] ([maBaiDang], [maKhuVuc]) VALUES (1, 1)
INSERT [dbo].[BAIDANG_KHUVUC] ([maBaiDang], [maKhuVuc]) VALUES (1, 2)
INSERT [dbo].[BAIDANG_KHUVUC] ([maBaiDang], [maKhuVuc]) VALUES (1, 3)
INSERT [dbo].[BAIDANG_KHUVUC] ([maBaiDang], [maKhuVuc]) VALUES (2, 4)
/****** Object:  Trigger [xoabaidangTrigger]    Script Date: 08/15/2015 15:38:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[xoabaidangTrigger] on [dbo].[BAIDANG]
AFTER DELETE
AS
	UPDATE NGUOIDUNG
	SET soBaiDang = soBaiDang - 1
	WHERE tenDangNhap = (SELECT maNguoiDang from deleted)
GO
/****** Object:  Trigger [thongbaoTrigger]    Script Date: 08/15/2015 15:38:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[thongbaoTrigger] on [dbo].[THONGBAO]
AFTER INSERT
AS 
	UPDATE THONGBAO
	SET ngayBao = GETDATE(), trangThai = 'Cho xu ly'
	WHERE maTB = (SELECT maTB from inserted)
GO
/****** Object:  ForeignKey [FK_BAIDANG_NGUOIDUNG]    Script Date: 08/15/2015 15:38:34 ******/
ALTER TABLE [dbo].[BAIDANG]  WITH CHECK ADD  CONSTRAINT [FK_BAIDANG_NGUOIDUNG] FOREIGN KEY([maNguoiDang])
REFERENCES [dbo].[NGUOIDUNG] ([tenDangNhap])
GO
ALTER TABLE [dbo].[BAIDANG] CHECK CONSTRAINT [FK_BAIDANG_NGUOIDUNG]
GO
/****** Object:  ForeignKey [FK_THONGBAO_BAIDANG]    Script Date: 08/15/2015 15:38:34 ******/
ALTER TABLE [dbo].[THONGBAO]  WITH CHECK ADD  CONSTRAINT [FK_THONGBAO_BAIDANG] FOREIGN KEY([maBaiDang])
REFERENCES [dbo].[BAIDANG] ([maBD])
GO
ALTER TABLE [dbo].[THONGBAO] CHECK CONSTRAINT [FK_THONGBAO_BAIDANG]
GO
/****** Object:  ForeignKey [FK_BAIDANG_KHUVUC_BAIDANG]    Script Date: 08/15/2015 15:38:34 ******/
ALTER TABLE [dbo].[BAIDANG_KHUVUC]  WITH CHECK ADD  CONSTRAINT [FK_BAIDANG_KHUVUC_BAIDANG] FOREIGN KEY([maBaiDang])
REFERENCES [dbo].[BAIDANG] ([maBD])
GO
ALTER TABLE [dbo].[BAIDANG_KHUVUC] CHECK CONSTRAINT [FK_BAIDANG_KHUVUC_BAIDANG]
GO
/****** Object:  ForeignKey [FK_BAIDANG_KHUVUC_KHUVUC]    Script Date: 08/15/2015 15:38:34 ******/
ALTER TABLE [dbo].[BAIDANG_KHUVUC]  WITH CHECK ADD  CONSTRAINT [FK_BAIDANG_KHUVUC_KHUVUC] FOREIGN KEY([maKhuVuc])
REFERENCES [dbo].[KHUVUC] ([maKV])
GO
ALTER TABLE [dbo].[BAIDANG_KHUVUC] CHECK CONSTRAINT [FK_BAIDANG_KHUVUC_KHUVUC]
GO
